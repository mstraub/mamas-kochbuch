# Germteig

[Germteig (Hefeteig)](https://de.wikipedia.org/wiki/Hefeteig) besteht zumindest aus Mehl, Wasser, Salz und Germ (Hefe) als Triebmittel. Üblich ist auch die Beigabe von Zucker, Fett, Milch, und Eiern.

Er kann gebacken ([Germkuchen](../Kuchen%20und%20Torten/Germkuchen%20mit%20Obst%20und%20Streusel), [Kärntner Reindling](../Kuchen%20und%20Torten/Kärntner%20Reindling),..) oder in Wasser(dampf) ([Germknödel](../Nachspeisen/Germknödel),..) gegart werden.

[Video zur Zubereitung von *Resi Oma kocht*](https://www.youtube.com/watch?v=3l9EXHkvqoc)

**Beispielrezept**

> [!CITE] Hauptschulbuch "Gemeinsam Haushalten"

- 25 dag Mehl
- 1/2 Pkg Frischgerm (~2 dag)
- 1 Dotter (oder Ei)
- 1 dag Butter (zimmerwarm)
- 4 dag Zucker
- 1/8 - 3/16 L Milch
- 1/2 TL Salz

1. Mehl und Salz in Schüssel geben
2. In der Mitte eine Grube formen
3. **Dampfl:** In der Grube Germ, 1 TL Zucker, 2 EL *lauwarme* Milch (oder Wasser) vorsichtig verrühren
4. **Gärprobe**: Schüssel zudecken und Dampfl ca. 10 Minuten gehen lassen
5. Restliche Zutaten einkneten bis der Teig seidig glänzt und sich vom Gefäß löst. Die Festigkeit lässt sich durch die Milch kontrollieren. (Tipp: schon 1-2 EL können die Teigkonsistenz stark ändern!)
6. Schüssel wieder zudecken und Teig ca. 20 Minuten gehen lassen
7. Je nach Rezept weiterverarbeiten

**Tipps & Anmerkungen**
- Die Gärprobe mit Dampfl ist optional, aber bei nicht mehr ganz frischem / abgelaufenen Germ zu empfehlen
- Germteig sollte an einem warmen Ort rasten, bei kühlem Zimmer im Winter kann auch der Ofen helfen
- Zudecken des Germteigs wird oft empfohlen (hilft vermutlich um Austrocknen zu vermeiden und Wärme zu halten)
- Butter kann auch geschmolzen und Zucker darin aufgelöst werden (aber auch ohne wirds irgendwann homogen)
- Der Teig kann auch vorbereitet werden und über Nacht im Kühlschrank lagern

**Varianten**
Je mehr Butter und Dotter, desto feiner wird der Teig, vor allem der Butteranteil kann stark variieren, siehe Vergleich der Basiszutaten mit dem Rezept oben:

| Zutaten   | Hauptschulbuch | [Feiner Germteig](https://www.mehlspeiskultur.at/c/feiner-germteig) | [Christstollen](https://www.backenmachtgluecklich.de/rezepte/christstollen.html) |
| --------- | -------------- | ------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| Mehl      | 500 g          | 500 g                                                               | 500 g                                                                            |
| Zucker    | 80 g           | 100 g                                                               | 50 g (+ Marzipan)                                                                |
| Butter    | 20 g           | 100 g                                                               | 220 g                                                                            |
| Milch     | 250 ml         | 250 ml                                                              | 120 ml (+80 ml Rum)                                                              |
| Ei (ganz) | 2              | 1                                                                   | 1                                                                                |
| Dotter    |                | 3                                                                   | 1                                                                                |
