# Kaffee

## French Press
> [!CITE] [Quelle](https://rauwolf-coffee.at/kaffeezubereitung/french-press-kaffee/)

- 65 g / L Wasser = ca. 4-5 gehäufte EL

1. Wasser auf 96°C erhitzen
2. Kaffee mit Wasser aufgießen
3. Umrühren, Deckel drauf und leicht andrücken (so dass kein Pulver oben schwimmen kann)
4. Maximal 4 Minuten ziehen lassen, dann Stempel langsam nach ganz unten drücken
    - Eventuell dazwischen noch (mehrmals) umrühren, das soll den Geschmack verändern
   
## Filterkaffee 

> [!CITE] [Quelle](https://www.coffeecircle.com/de/e/kaffee-dosierung):

- 12-13 g / 200 ml Wasser = ca. 1 gehäufter EL
- 60-65 g / L Wasser = ca. 4-5 gehäufte EL
