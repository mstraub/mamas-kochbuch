# Topfenteig 

## Variante Fellinger

> [!TIP] fester Teig
> geeignet für Mohnnudeln oder gefüllte Topfenknödel

* 4 dag Margarine
* 1 Ei
* 25 dag Topfen
* 10 dag Mehl
* 2½ dag Brösel
* 1½ EL Zucker
* 1 Prise Salz
* 1/4 Pkg Backpulver

Margarine und Ei schaumig rühren, dann Rest der Zutaten dazurühren. Es soll ein eher fester Teig sein.

**Für Birnenknödel**: etwas mehr Mehl verwenden. Sauce aus 1:1 Topfen + Sauerrahm.


## Variante

* 7 dag Butter
* 25 dag Topfen
* 1 Ei
* 15 dag Mehl
* 1 Prise Salz

Butter schaumig rühren, Topfen und Ei beigeben, zum Schluss Mehl und Salz zugeben. Teig rasten lassen. Rolle formen, Scheiben herunterschneiden und die Frucht einwickeln.
