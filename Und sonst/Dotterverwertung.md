# Dotterverwertung

- [Crème brûlée](https://www.lecker.de/creme-brulee-so-geht-das-original-rezept-75096.html)
- Eierlikör
- Eierspeis
- Erdäpfelteig (Das große Sacher Kochbuch, Seite 394)
- Lemon Curd
- [Linzer Kolatschen](https://www.chefkoch.de/rezepte/3421841509743977/Linzer-Kolatschen.html)
- Sauce Hollandaise
- [Spaghetti Carbonara](https://www.lecker.de/klassische-spaghetti-carbonara-73209.html)
- [Tiramisu](../Nachspeisen/Tiramisu.md)
- [Topfenknödel](../Nachspeisen/Topfenknödel.md)
