# Mürbteig

- 300 g glattes Mehl
- 200 g kalte Butter
- 100 g Staubzucker
- 1 Ei
- Prise Salz
- optional: Zitronenschale, Vanille,..

**Zubereitung**
1. Alle Zutaten möglichst flott verkneten (Butter in Würfel schneiden hilft)
2. Kühl stellen und rasten lassen (einige Stunden)

> [!note] pikant: Zucker weglassen

https://www.pati-versand.de/lexikon/muerbeteige/fehlerquellen-bei-muerbeteigen