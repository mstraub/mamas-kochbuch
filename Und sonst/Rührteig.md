# Rührteig

> [!tldr] Rührteig ist eine gerührte, teils auch aufgeschlagene Masse zur Herstellung von Kuchen, die hauptsächlich **Eier, Fett, Zucker, Milch oder Wasser, Mehl und Stärke** enthält.

**Basiszutaten**
- Eier
- Butter
- Zucker
- Mehl
- Backpulver

**Zubereitung**
1. Butter schaumig rühren
2. Zucker dazu, rühren
3. Eier dazu (einzeln), rühren
4. Mehl und Backpulver mischen
5. Mehl (in kleinen Portionen) dazu, rühren
6. Backform gut fetten und mehlen

**Wichtige Hinweise**
- **Zutaten (v.a. Butter & Eier) müssen auf Zimmertemperatur erwärmt werden**, sonst funktioniert das schaumig rühren nicht (oder nur sehr mühsam)
- "Schaumig" gerührte Butter (mit Zucker / Eiern) ist heller als zuvor, und es dauert mehrere Minuten bis man soweit ist
- **Konsistenz:** Rührteig sollte *"schwer reißend"* vom Löffel rinnen/fallen
- Backform mit Fett bestreichen und mit Mehl bestauben damit sich der gebackene Teig gut aus der Form löst

siehe auch diese [längere Ausführung](https://www.backenmachtgluecklich.de/rezepte/grundrezept-ruehrteig-und-ruehrkuchen.html)

Ein leicht zu merkendes Rezept ist der **Eischwerteig**, bei dem das Gewicht der Eier (inkl. Schale) das Gewicht von Butter + Zucker + Mehl bestimmt. (+Backpulver)