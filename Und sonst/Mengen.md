# Mengen

**Angaben**

- EL Esslöffel (gestrichen voll)
- L Liter
- Msp Messerspitze
- Pkg Packung
- TL Teelöffel (gestrichen voll), vermutlich äquivalent mit selten verwendeten KL (Kaffeelöffel)

**Äquivalente**

- 1 Pkg Vanillezucker = 8 g
- 1 Pkg Puddingpulver / Stärke = 40 g (für 0,5 L Milch)
- 1 Schneidsemmel = 50 g

**Markus' Gschirrln**

- grünes Emaillereindl: 18 x 30 cm
- großes silbernes Reindl: 28 x 38 cm

## Abmessen mit Löffeln

Alle Angaben gelten für **gestrichene (nicht gehäufte) Löffel**.

Grundsätzlich scheint zu gelten
- 1 Esslöffel = 1 [tablespoon (UK/US)](https://en.wikipedia.org/wiki/Tablespoon) = 15 ml
- 1 Teelöffel = 1 [teaspoon (UK/US)](https://en.wikipedia.org/wiki/Teaspoon) = 5 ml

> [!TIP] 1 ml Wasser = 1 g Wasser

Auszug von [Chefkoch](https://www.chefkoch.de/magazin/artikel/500,0/Chefkoch/Ratgeber-Gewichte-und-Masseinheiten.html) und Das große Sacher Kochbuch Seite 57 (wobei da manche Werte, z.B. für das Salz, imho nicht nachvollziehbar sind):

|                          | 1 EL | 1 TL |
|--------------------------|------|------|
| Backpulver               | 10 g | 3 g  |
| Butter                   | 10-15 g | 4-5 g  |
| Kaffee gemahlen        |  5 g | 2 g  |
| Kakao                    |  5 g | 2-3 g  |
| Mehl                     | 10 g | 3-4 g  |
| Öl                       | 10 g | 3 g  |
| Salz                     | 15-20 g | 3-5 g  |
| Schlagobers              | 10-13 g | 5 g  |
| Stärke                   | 9-10 g | 3-5 g  |
| Wasser (Milch, Essig,..) | 15 g | 5 g     |
| Zucker                   | 15 g | 5 g  |


Markus' Messungen mit dem größten & tiefsten Esslöffel und dem tiefen Silberteelöffel:

|                           | 1 EL | 1 TL |
| ------------------------- | ---- | ---- |
| Kaffee gemahlen (gehäuft) | 15 g |      |
| Mehl                      | 8 g  | 3 g  |
| Salz                      | 20 g | 7 g  |
| Wasser                    | 16 g | 6 g  |
| Zucker                    | 14 g | 5 g  |


## Dichte: Volumen vs. Gewicht


Rezepte den USA, UK, Kanada,.. verwenden oft 1 [Cups](https://en.wikipedia.org/wiki/Cup_(unit)) als Maßeinheit.
Cups sind je nach Zeitperiode und Region unterschiedlich definiert:

- 250 ml = 1 metrischer Cup (UK, Kanada, Neuseeland, Australien)
- 237 ml = 1 US cup (auch 2023 noch)
- 227 ml = 1 canadian cup (vor der Nutzung des metrischen Cups)
- 284 ml = 1 imperial Cup (alte Rezepte aus UK,..)

Anhaltspunkte (v.a. bei Mehl vom Typ abhängig):

|                | Dichte (g / ml) | 250 ml (1 cup) |
| -------------- | --------------- | -------------- |
| Mehl           | 0,7 (?!)        | 125 g          |
| Kristallzucker |                 | 200 g          |
| Öl             | 0,92            | 230 g          |
| Wasser         | 1               | 250 g          |

>[!TIP] Markus: meine Kaffeetassen mit Goldhenkel fassen exakt 250 ml!

## Backtriebmittel

- 1 Pkg Backpulver = 16 g
- 1 Würfel Germ = 42 g
- 1 Pkg Trockenhefe = 7 g (enstpricht 21 g Germ)

für 50 dag Mehl ([Quelle](http://www.kirchenweb.at/kochrezepte/lexikon/backen/backen.htm)):

- 16 g Backpulver (üblicherweise = 1 Pkg)
- 6 g Natron
- 6 g Pottasche (Kaliumcarbonat)
- 6 g Hirschhornsalz (Ammoniumcarbonat)
- 1/2 Würfel Germ
- 1 Pkg Trockenhefe

> [!TIP] Kalium- und Ammoniumcarbonat gibt's in der Apotheke!
### Pottasche (Kaliumcarbonat)

- Wird traditionell für Lebkuchen verwendet.
- Benötigt Wasser und [eine Säure](https://www.pharmawiki.ch/wiki/index.php?wiki=Kaliumcarbonat) um CO2 freizusetzen.
- Lässt Teig breit werden (geht nicht so sehr in die Höhe auf)

Details ([Quelle](https://www.lebensmittellexikon.de/p0001120.php)): Durch die trockene Hitze im Backofen bildet Pottasche im Teig Kohlendioxid. Die Kohlendioxidbildung wird zusätzlich durch im Teig enthaltene Säure begünstigt. Als Nebenprodukt entsteht Kalilauge. Sie hat die Eigenschaften den Kleber des Weizenmehls zu schwächen. Das hat zur Folge, dass die Teige breit auseinander laufen. Durch die Lauge entwickelt sich der charakteristische Geschmack von Lebkuchen. Die Kombination von Pottasche und Hirschhornsalz verhindert schließlich, dass die Teige beim Backen zu breit auseinander laufen. Denn während die Pottasche für eine breite, flache Porung ausschlaggebend ist, so sorgt das Hirschhornsalz für einen großporigen nach oben gerichteten Trieb im Gebäck.

**Verwendung gemeinsam mit Hirschhornsalz**:
Wenn Pottasche zusammen mit Hirschhornsalz zur Teiglockerung verwendet wird, werden beide Stoffe getrennt voneinander aufgelöst und nacheinander in den Teig eingearbeitet. Wenn man Hirschhornsalz zusammen mit Pottasche auflöst und in den Teig gibt, reagieren beide Stoffe miteinander und verlieren ihre Triebkraft.