# Kochideen

Was wir sonst noch so kochen:

- Borschtsch (z.B. https://birgitd.com/ukrainischer-borschtsch-2/, schmeckt super, Menge für ca. 10 Personen)
- Brennsterz (Das große Sacher Kochbuch, Seite 410)
- Chili con Carne (mit Kakao, Kreuzkümmel, Majoran, Oregano, Lorbeerblätter)
- [Couscoussalat](https://emmikochteinfach.de/couscous-salat-rezept-schnell-und-einfach/) (mit Erdnüssen + Rosinen ergänzt)
- Dillrahmfisolen (Das große Sacher Kochbuch, Seite 420)
- Eingebrannte Linsen / Specklinsen mit Semmelknödeln (Das große Sacher Kochbuch, Seite 444) 
- Erdäpfelpuffer
- Gefüllte Zucchini / Paprika / Tomaten
- Gemüsereindl (verschiedenstes Gemüse grob schneiden, ins Reindl, Olivenöl und Gewürze drüber, rein ins Rohr) .. z.B. mehlige Erdäpfel, Süßkartoffel, Paradeiser, Zwiebel, Knoblauch (ganz!),.. und in den letzten 10 Minuten Feta dazugeben. Salzen erst am Tisch.
- Gemüsesupperl, z.B. [mit Wirsing](https://www.gutekueche.at/wirsingeintopf-mit-gemuese-rezept-38098)
- Grammelknödel mit Kartoffelteig und Sauerkraut
- Gulasch mit Nockerl
- Karfiol (oder [Romanesco](https://mehr-genuss.de/rezept/romanesco-zubereiten-so-gelingt-es-dir-garantiert/)) mit Butterbröseln: kurz kochen, dann rein in Pfanne mit Butter, Gewürzen, Zitrone
- [Kichererbsen-Melanzani-Eintopf](https://klaraslife.com/de/auberginen-kichererbsen-eintopf-und-gewuerz-joghurt) mit Granatapfelkernen und Fladenbrot
- [Kichererbsen-Zucchini-Curry](https://aufgegabelt-foodblog.de/2012/09/12/kichererbsen-zucchini-curry)
  mit [Naan (Brot)](../Brot/Naan.md) ([anderes Rezept](https://aufgegabelt-foodblog.de/2012/03/13/naan-brot-aus-der-pfanne-2)) 
- [Leinölerdäpfel](https://www.servus.com/r/muehlviertler-leinoelerdaepfel)
- [Moussaka](https://www.recipetineats.com/moussaka-greek-eggplant-beef-bake/) (könnte +50% Melanzani vertragen, war arg fleischlastig)
- Mühlviertler Rahmsuppe (aus Geschmack hat Tradition - Bad Leonfeldner Tourismusschule)
- [Palak Paneer](https://carointhekitchen.com/palak-paneer), optional mit selbstgemachtem Paneer
- Spinat mit Spiegelei und Erdäpfeln
- [Schoko-Kokos-Kuchen](https://www.oetker.de/rezepte/r/schoko-kokos-kuchen) - ultrasaftig browniemäßig 
- Sellerieschnitzel mit Petersilienerdäpfel
- [Thai-Gemüsecurry](https://kochkarussell.com/thai-gemuese-curry-kokosmilch/) (Rote Currypaste, Kokosmilch, Gemüse)
