# Strudelteig (gezogen)

## Zutaten

> [!CITE] Das große Sacher Kochbuch, Seite 493, Menge für einen Strudel

* 200 g Mehl (glatt)
* 1 Ei (optional)
* Salz
* 1/16 L lauwarmes Wasser
* 20 g Öl

**Variante ohne Ei** für ca. zwei Strudel

* 300 g glattes Mehl
* 150 g lauwarmes Wasser
* 40 g Öl
* eine Prise Salz

**Variante mit Ei** für 3-4 Strudel, von [Chefkoch](http://www.chefkoch.de/rezepte/drucken/1550611262213964/2309481a/2/Alt-Wiener-Apfelstrudel.html)

* 50 dag Mehl, Type 550
* 2 Eier
* 200 ml Wasser, lauwarm
* 40 ml Öl
* 4 g Salz

## Zubereitung

1. Alle Zutaten in eine Schüssel geben und so lange kneten, bis der Teig glatt ist, seidig glänzt, und nicht klebt (kleben sollte er aber von Anfang an kaum)
2. Den Teig in zwei gleich große Stücke teilen und Kugeln formen.
3. Die Kugeln mit gutem Öl bestreichen, in Frischhaltefolie wickeln, auf einen Teller legen und an einem warmen Platz ca. eine Stunde rasten lassen.

> [!WARNING] Strudelteig mag es nicht, wenn er auskühlt. Wird Strudelteig auf Vorrat im Kühlschrank gelagert, dann muss er vor der Verarbeitung wieder auf Zimmertemperatur gebracht werden.

**Weitere Variationen bei der Zubereitung:**

* Teig auch auf einem Nudelbrett mischen und so lange abarbeiten (schlagen), bis er seidig glänzt
* Teig in kleiner Schüssel in Sonnenblumenöl ca. 30 Minuten baden. Danach ist der Teig richtig schön elastisch. Nach dem Ölbad den Teig in Mehl wenden und auf einem bemehlten Tuch rechteckig ausrollen

> [!TIP] Strudelteig ist dann richtig ausgezogen und perfekt, wenn man das unter den Teig geschobene Rezept lesen kann.