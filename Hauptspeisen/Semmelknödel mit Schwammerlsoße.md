# Semmelknödel mit Schwammerlsoße

> [!CITE] von Mama, sehr einfach aber trotzdem gut

**Zutaten für ca. 5 Personen**

* 35 dag Knödelbrot (Semmelwürfel)
* 1 Ei
* 1 Bund Petersilie
* Salz

> [!NOTE] Erfahrung Markus: mehr Ei geht immer, aber auch mit nur einem Ei schmeckt's gut!

1. Knödelbrot mit einem Schöpfer heißem Wasser übergießen und mit Kochlöffel durchmischen.
2. So lange wiederholen, bis keine trockenen Stücke Knödelbrot mehr vorhanden sind.
3. Ei und Petersilie hinzufügen und erneut durchmischen.
4. Ca. 20 Minuten ruhen lassen.
5. Mit nassen Händen Knödel formen.
6. In Salzwasser ca. 20 Minuten leicht wallend kochen.

> [!TIP] ein Stück Bergkäse in der Mitte der Knödel schmeckt super!

## Schwammerlsauce

### Variante No Nonsense

* Schwammerl
* Zwiebel

Zwiebel rösten, Schwammerl dazu, 5 min köcheln, mit Salz und Pfeffer abschmecken

### Variante Michael

* 500g Schwammerl
* ½ Zwiebel
* 1 EL Mehl
* 1 Becher Schlagobers
* 250 ml kalte Suppe
* Muskat
* Salz, Pfeffer
* Petersilie (zum Schluss hinzugeben)

Schwammerl putzen, kurz anbraten und wieder aus der Pfanne nehmen. Zwiebel klein schneiden und in der Pfanne anbraten. Mit Mehl bestäuben und mit der kalten Suppe ablöschen. Etwas Schlagobers hinzugeben und mit den restlichen Gewürzen abschmecken, die Schwammerl hinzugeben und leicht köcheln lassen.

Laut Chefkoch kann man zum Schluss ein Eigelb mit dem restlichen Schlagobers vermischen und unter die Sauce geben zum Binden.

