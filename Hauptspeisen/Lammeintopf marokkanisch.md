# Lammeintopf (Marokko)

> [!cite] [Quelle](https://cookidoo.de/recipes/recipe/de-DE/r447833), ca. 5 Portionen

- 280 g Couscous
- 20 g Butter
- 2 TL Salz

- 30 g Öl
- 400 g Lammfleisch (Schulter, ohne Knochen, in 2-3 cm großen Stücken)
- 2 Zwiebeln, halbiert, in Ringen
- 1 rote Chilischote, frisch, in Ringen
- 1-2 Knoblauchzehen, in Scheiben
- 200 g Karotten, in Scheiben
- 30 g Datteln, entkernt
- 30 g getrocknete Marillen, entkernt
- 20 g Tomatenmark
- 400 g Tomaten gewürfelt (z.B. aus der Dose)
- 0,5 L Suppe (z.B. Rind)
- 1/2 TL Pfeffer
- 1/2 TL Kreuzkümmel
- 1-2 Prisen Zimt
- 1/2 TL Curry
- 200 g Fisolen, halbiert
- 30 g Mandelstifte
- 1-2 Stängel Marokkanische Minze, abgezupft

**Zubereitung**
1. Zwiebel, Öl, Fleisch anbraten
2. Knoblauch, Karotten dazu, kurz anrösten
3. langsam und lange kochen (1-2 Stunden oder auch länger)
