# Kässpätzle

> [!CITE] von [vorarlberg.travel](https://www.vorarlberg.travel/aktivitaet/kaesspaetzle-rezept)

Für 4 Personen

**Zutaten**

*Spätzle*

- 5 Eier
- 500 g griffiges Mehl (Type 480)
- 1/8 L Wasser
- Prise Salz, Muskatnuss
- 100 g Bergkäse
- 100 g Alpkäse (alternativ Emmentaler)
- 100 g Räßkäse

*Röstzwiebel*

- 1 Gemüsezwiebel, geschält und in Scheiben geschnitten
- 150 g Butter

*Garnieren*

- 1/2 Bund Schnittlauch
- Pfeffer aus der Mühle

**Zubereitung**

1. Eier, Mehl, Salz und Muskatnuss mit etwas lauwarmem Wasser zu einem glatten Teig verkneten. Der Teig sollte von der Konsistenz zwischen flüssig und fest sein.
2. Mit einem Spätzlehobel den Teig in das kochende Salzwasser "spätzla".
3. Sobald die Spätzle an der Wasserober fläche schwimmen, abseihen. Den Käse durchmischen und schichtweise Spätzle und Käse in die gebutterte Form schichten.
4. Die Butter in einer kleinen Pfanne erhitzen. Die Zwiebelringe darin goldbraun braten. Bitte dabei beachten, dass die Zwiebeln enorm nachbräunen. Das heißt: hellbraun aus der Pfanne nehmen, auf einem Küchenpapier abtropfen lassen und mit etwas gehacktem Schnittlauch vor dem Servieren über die Kässpätzle geben. Mit Pfeffer nachwürzen.

