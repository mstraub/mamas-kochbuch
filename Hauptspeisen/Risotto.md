# Risotto

**Zutaten für ca. 4 Personen**

- 1 L Gemüsesuppe (heiß!)
- 1 Zwiebel
- 4 EL Olivenöl
- 300 g Risotto-Reis (z.B. Carnaroli)
- 100 ml Weißwein trocken
- 4 EL Parmesan gerieben
- 100 g Butter
- Pfeffer
- Salz

1. Zwiebel fein schneiden und im Olivenöl andünsten.
2. Sobald die Zwiebel glasig werden, den Reis hinzufügen und ebenfalls 1-2 Minuten andünsten.
3. Den Wein hineingießen und solange köcheln lassen, bis er fast verkocht ist.
4. Das Risotto mit einer Kelle Suppe auffüllen und einkochen lassen. Solange wiederholen bis die komplette Suppe eingekocht ist und die Reiskörner im Inneren al dente sind. Dabei immer wieder rühren.
5. Wenn die letzte Kelle Brühe im Risotto ist, den geriebenen Parmesan und die Butter einrühren, so dass der Reis cremig wird und glänzt.
6. Mit Salz und Pfeffer abschmecken (vermutlich kein Salz nötig)

Optionale Gewürze: Gewürze (z.B. Oregano, Petersilie, Knoblauch,..)

Optionale Zutaten: Gemüse ([1 kg Kürbis](https://www.ichkoche.at/kuerbis-risotto-rezept-7018), [60 dag Sellerie](https://frischgekocht.billa.at/rezept/sellerierisotto-mit-gremolata-BI-29426), Schwammerl, Fleisch,..). Diese Zutaten können einfach mitgekocht werden, auch wenn in einigen Rezepten Risotto und Gemüse separat gekocht werden.


