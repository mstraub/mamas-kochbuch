# Ananas Puten Curry

> [!cite] von Mama

* 60 dag Putenfleisch, gewürfelt
* 1 Dose Ananasstückchen
* 1 Zwiebel
* Curry
* Salz
* Pfeffer
* 25 g Mandelblättchen (optional)

Zwiebel glasig dünsten, Putenfleisch hinzugeben und anbraten. Mit Salz und Curry würzen, anschließend Ananasstückchen beigeben. Mit Ananassaft oder Wasser ablöschen und 1 Suppenwürfel unterrühren. Aufkochen lassen und mit etwas Mehl binden. Mit Reis servieren.

