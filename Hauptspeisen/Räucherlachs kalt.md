# Räucherlachs (kalt)

> Weihnachts- und Silvesterschmaus von Mama

* Lachs
* Grüner Salat
* Zwiebel
* Zitrone

Salatbeet auf Tablett legen, dann Zwiebel (dünne Scheiben) und Lachs (ebenfalls so dünn wie möglich aufgeschnitten) drauflegen. Zitronenscheiben / Zitronensaft dazu.

**Dill-Senf-Sauce**

* 1/2 Becher Rahm
* 1/2 Becher Joghurt
* fein gehackter Dill
* 2cm Senf
* Schuss Whiskey oder Cognac
* Prise Salz

**Honig-Senf Sauce**

* 1/2 Becher Rahm
* 1/2 Becher Joghurt
* 1 EL Honig (oder auch mehr)
* 2cm Senf
* Schuss Whiskey oder Cognac
* **kein (!)** Salz

