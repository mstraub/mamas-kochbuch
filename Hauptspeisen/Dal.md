# Dal

> [!CITE] von [sevencooks.com](https://www.sevencooks.com/de/magazin/der-beste-dal-meines-lebens-UdoMOCOwuImwCgGgKqW4m) 

Für 4 Personen, dazu passt Reis oder [Naan](../Brot/Naan.md)-Brot.

* 2 Zwiebeln
* 2 Knoblauchzehen
* 2 cm Ingwer
* 2 rote Chilischoten
* 2 EL Kokosöl (oder das Fett der Kokosmilch)
* 1 EL Kumin
* 1 TL Zimt
* 1 EL Kurkuma
* 1 TL gemahlener Koriander
* 1 TL schwarze Senfsamen
* 250 g rote Linsen
* 400 g Dosentomaten
* 1 TL Salz
* 1 Zitrone, Saft davon
* 200 ml Wasser
* 300 ml Kokosmilch

**Zubereitung**

1. Zwiebeln schälen und in Würfel schneiden. Knoblauchzehen schälen und fein hacken. Ingwer ebenfalls schälen und hacken und die Chilis kleinschneiden.
2. Kokosöl in einem Topf erhitzen und die geschnittenen Zutaten darin anschwitzen. Dann die Gewürze hinzugeben und mit anrösten.
3. Linsen in den Topf geben und für ca. 1 Minute mit anschwitzen. Mit den Dosentomaten ablöschen. Salz, Zitronensaft, die Hälfte des Wasser und die Kokosmilch hinzugeben und köcheln lassen.
4. Das Dal sollte mindestens 30 Minuten köcheln. Währenddessen regelmäßig umrühren, sodass nichts anbrennt. Nach und nach das restliche Wasser zugeben.

## Simple Variante für faule Tage

für 4 Personen

* 200 g rote Linsen
* 1 L Wasser
* 1 TL Kreuzkümmel
* 1 TL Koriander
* 1/2 TL Kurkuma
