# Tomatensugo

> [!NOTE] nach Markus' Internetrecherche zu möglichst italienischen Sugos

- 1 kg Tomaten (frische Sommertomaten oder gute Dosentomaten)
- 1-2 Zwiebeln
- 1-2 Knoblauchzehen
- 4 EL Olivenöl
- 1/4 Tube Tomatenmark
- 1 EL Zucker
- 1-2 TL Salz
- 2 TL Basilikum (oder frisch!)
- 2 TL Oregano
- Pfeffer (beliebig)
- Weißwein (beliebig)

1. Zwiebel und Knoblauch fein hacken und in Olivenöl anbraten
2. Tomaten in mittelgroße Stücke schneiden und dazugeben
3. Tomatenmark, Zucker, Salz dazugeben
4. Lange leicht köcheln (zwischen einer und sieben Stunden - das scheint das Geheimnis für guten Geschmack zu sein)
5. Wenige Minuten vorm Schluss Basilikum und Oregano beigeben und mit Olivenöl und Salz abschmecken

> [!TIP] nach Belieben können auch Karotten oder Stangensellerie mitspielen

