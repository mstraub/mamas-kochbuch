# Steckerlfisch

> [!CITE] Markus' und Ullis Erfahrungen

**Zutaten**

- Fische, ganz und ausgenommen, z.B. Forelle, Saibling,..
	- 300 - 400 g ist eine gute Portion pro Person
- Steckerl: mindestens 80 cm lange angespitzte Holzspieße.
- Zitronensaft
- Salz

Dazu passen gut [Steckerlbrot](../Brot/Steckerlbrot.md) und Erdäpfelsalat

**Vorbereitungen**

1. **Steckerl** mehrere Stunden lang einwassern (damit sie nicht verkohlen)
2. **Länglicher Sandwall** (ca. 50 cm hoch) für die Steckerl neben der Feuerstelle aufschütten
3. **Fische aufspießen** ([empfehlenswertes Video zum Spießen](https://www.youtube.com/watch?v=jsOfWBmMsQo))
	1. Fische gut mit Wasser reinigen und trocken tupfen
	2. Fische Innen und außen mit Zitronensaft beträufeln, salzen, und alles zusammen verreiben
	3. Steckerl mit Öl einreiben (damit es sich nachher vom Fisch löst)
	4. Steckerl in Maul einführen
	5. Auf einer Seite entlang des Rückgrats durch die Fleischmitte spießen (bis kurz vor Ende der Bauchhöhle)
	6. Auf der anderen Seite ebenfalls durch die Fleischmitte spießen
	7. Genau die Mitte der Schwanzflosse durchstechen (nur die Spitze des Steckerls soll rausschauen)
4. **Glut** neben dem Sandwall vorbereiten

 > [!TIP] Feuer direkt in der Feuerstelle machen und bis zur Glut runterbrennen.
 > Denn: glühende Kohlen auf kalter Feuerstelle verlieren zu schnell an Hitze.
 > Nach Bedarf kann auch während dem Grillen der Fische Kohle nachgelegt werden.

**Grillen**

1. Steckerl so in Sandwall stecken, dass die Fische mit Kopf und Rücken nach unten schräg über der Glut hängen
	- Den Abstand zur Glut so wählen, dass man die Hand ca. 3 Sekunden lang hinhalten kann
2. Zuerst den Rücken grillen bis er knusprig ist (ca. 20 Minuten)
3. Dann umdrehen und den (offenen) Bauch grillen (nochmal ca. 20 Minuten)
4. Fertige Fische (mit dem Steckerl) in Backpapier und außen noch Zeitung eingewickelt servieren 

> [!NOTE] Keine Marinade?
> Einige [Rezepte](https://www.bayerische-spezialitaeten.net/schmankerl/steckerlfisch.php) marinieren die Fische zuvor, wir fanden das nicht notwendig, sie schmecken auch pur sehr gut.