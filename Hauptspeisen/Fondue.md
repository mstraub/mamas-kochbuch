# Fondue

## Chinesische Suppe

> [!CITE] von Mama

* 1 L Rindssuppe
* 2 EL Weißwein
* 3TL Sojasauce

## Fondue Zutaten

Einkaufsliste (für 2 Personen)

* 2 Baguette
* 30-40 dag Fleisch
* 15 dag Champignon
* Karfiol (auch zum ins Fondue hängen)
* Maiskölbchen
* Silberzwiebel
* Gurkerl
* Oliven
* Brennpaste / Spiritus / ..

## Saucen

### Grüne Pfeffersauce

> [!CITE] von Mama

* 4 EL Topfen
* 2 EL Rahm
* 1/2 TL Salz
* 1 TL Zitronensaft
* 2 EL grüne Pfefferkörner

### Apfelsauce

> [!CITE] von Mama

* 4 EL Rahm
* 1 EL scharfen Senf
* 2 EL gehackte Zwiebel
* 1 Apfel
* Salz
* 1 hart gekochtes Ei

### Curry-Bananensauce

> [!CITE] von Mama

* 4 EL Creme fraîche
* ½ Banane
* 2 EL geriebenen Emmentaler
* 1 TL Curry
* 5 dag Mandelblättchen
* 1 EL Zitronensaft
* 5 gehackte grüne Pfefferkörner
* Salz, Pfeffer
* 1 Prise Zucker
* (evtl. 1 TL Mango oder Marillenchutney)

### Scharfe Honigsauce

> [!CITE] von Michael

* 3 EL Honig
* 30 dag Tomatenketchup
* 3 EL Essig
* 2 Zehen Knoblauch
* 1 Messerspitze [Black Mamba](http://www.hotsauce.com/Black-Mamba-Hot-Sauce-p/1553.htm) (oder eine andere scharfe Sauce)
* 1 EL Petersilie
* 1 EL Schnittlauch
* 1 TL Curry
* Salz, Pfeffer

### Berliner Fondue- und Grillsauce

> [!CITE] von Michael

* 10 dag Mayonnaise
* 30 dag Jogurt (Original mit 40 dag Mayonnaise → sehr geil)
* 1 EL geriebener Kren
* 1 Prise Zucker
* 1 Prise Salz
* 3 TL Schlagobers
* 2 TL Currypulver
* 4 EL Ketchup
* 3 TL Kräuter (Dill, Schnittlauch, Petersilie, ...)

