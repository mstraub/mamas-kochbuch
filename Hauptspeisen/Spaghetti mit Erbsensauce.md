# Spaghetti mit Erbsensauce

**Zutaten für 4 Personen**

* 10 dag Schlagobers
* 300 ml klare Suppe
* Salz, Pfeffer (wenn vorhanden weißer Pfeffer und Zitronenpfeffer)
* 60 dag Erbsen
* 150 g Kasseler-Aufschnitt / Schinken
* Muskatnuss
* Spaghetti

Schlagobers, Suppe, Salz und Pfeffer aufkochen und die Erbsen darin zugedeckt ca. 10 min köcheln lassen. Einige Erbsen herausnehmen und den Rest pürieren. Bei milder Hitze etwas einkochen lassen und anschließend den Aufschnitt beigeben. Die Sauce abschmecken (optional mit Worcester Sauce) und mit den Spaghetti servieren. Das Ganze kann mit Minze verziert und/oder mit Parmesan bestreut werden.

