# Erdäpfelgulasch

> [!CITE] Das große Sacher Kochbuch, Seite 409

* 1 kg Erdäpfel
* 40 g Fett (z.B. Butter)
* 40 g Speck
* 250 g Zwiebeln
* 20 g Paprika gemahlen
* Kümmel, Majoran, Knoblauch, Salz, Essig
* Optional: Paprika (frisch), Paradeiser, geräucherte Wurst

1. Kleinwürfelig geschnittenen Speck in heißem Fett leicht glasig rösten.
2. Gehackte Zwiebeln darin goldgelb rösten, paprizieren und mit etwas Essig & Wasser ablöschen.
3. Gehackte Gewürze und würfelig (oder blättrig) geschnittene rohe Erdäpfel beigeben.
4. Zugedeckt weich dünsten.

> [!TIP] Noch feiner:
> In Streifen geschnittene Paprikaschoten und würfelig geschnittene Paradeiser mitdünsten und geschnittene Räucherwurst als Einlage am Schluss beigeben und kurz erhitzen.

> [!TIP] geräuchertes Paprikapulver verwenden! (guter Speckersatz)