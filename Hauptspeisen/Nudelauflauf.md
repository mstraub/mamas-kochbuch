# Nudelauflauf

> [!CITE] Markus' flotter Auflauf fürs grüne Reindl

- 300 g Nudeln
- 700 g Gemüse (z.B. Brokkoli und Tomaten)
- 200 g Schinken (oder Speck)
- 250 ml Rahm
- 2-3 Eier
- 250 g Käse (würzig)

1. Gemüse und Schinken klein schneiden
2. Backrohr auf 180°C vorheizen
3. Nudeln kochen (Gemüse gleich im selben Topf mitkochen)
4. Rahm und Eier vermischen, mit Salz und Pfeffer stark würzen
5. Käse reiben und zum Rahm-Ei-Gemisch geben
6. Reindl einölen
7. Nudeln, Schinken und Gemüse gut vermischt ins Reindl füllen
8. Rahm-Ei-Käse-Gemisch drübergießen
9. Ca. 20 Minuten überbacken

> [!TIP] fein geschnittener Zwiebel macht sich im Rahm-Ei-Gemisch auch gut