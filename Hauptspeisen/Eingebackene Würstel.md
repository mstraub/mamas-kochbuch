# Eingebackene Würstel

für ca. 13-15 eingebackene Würstel

* 1 kg glattes Mehl
* 1 Würfel Hefe
* 2 EL Zucker
* 250 ml warmes Wasser
* 250 ml Milch
* 3 EL Öl
* 2 TL Salz
* Frankfurter
* Käse in Streifen

zu Teig verarbeiten, gehen lassen.

**Variation**: nur 800 g normales + 200 g Vollkornmehl, schmeckt fast noch besser. Außerdem noch Käsestreifen auf die Frankfurter legen und mit Teig einwickeln!

bei 220°C ca. 15 min fertig backen

