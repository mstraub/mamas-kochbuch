# Pastinakenpancake

> [!CITE] Andi Poschmaiers Babynahrungsklassiker, immer am Spielplatz dabei :)

- 300 g Pastinaken
- Schuss Milch
- 200-250 g Mehl
- 1/2 Pkg Backpulver
- 3 Eier
- Schuss Öl
- Petersilie

1. Pastinaken grob schneiden, in gesalzenem Wasser kochen
2. Mit einem Schuss Milch pürieren
3. Pastinaken-Creme mit restlichen Zutaten vermischen (Festigkeit mit optionaler Zugabe von mehr Milch bzw. Öl anpassen)
4. In Butter oder Öl bei schwacher Hitze backen (ca. 2 EL große Patzerl)