# Pizzateig

Rezept für ca. 3 Backbleche Pizza, aber nachdem der Teig aufgegangen ist, kann man ihn super einfrieren - einfach dann auftauen lassen und verarbeiten!

* 1 kg Mehl
* 2 Pkg. Trockenhefe (oder 1 Würfel Germ)
* 4 EL Olivenöl
* 1 EL feiner Zucker
* 1 EL Salz
* 650 ml lauwarmes Wasser

alle Zutaten zu einem Teig verarbeiten und mindestens 1h gehen lassen.

Ofen auf höchster Stufe vorheizen (250°C), den Teig dünn ausrollen und belegen. Die Pizza auf mittlerer Stufe ca. 10-12 min fertig backen (je nach Temperatur des Ofens).

## Gute Tomatensauce

* 1 Dose passierte Tomaten (= vermutlich 400g)
* 5 EL Tomatenmark
* 1 Prise Basilikum
* 2 Stk Knoblauchzehen
* 2 EL Olivenöl
* 1 Prise Oregano
* 3 EL Parmesan
* je 1 Prise Pfeffer + Salz
* 1 Prise Thymian
* 0.75 Stk Zwiebel

Die passierten Tomaten in ein hohes Gefäß füllen. Hierzu die geschälten Knoblauchzehen pressen.

Die Zwiebeln schälen und in kleine Würfel schneiden. Zusammen mit dem Tomatenmark zu den Tomaten geben. Das Olivenöl dazugießen und alles mit dem Pürierstab vermengen und pürieren bis eine sämige Konsistenz entsteht.

Unter die Tomatensauce den Parmesan mischen.

Je nach Geschmack mit Thymian, Oregano, Basilikum, Salz und Pfeffer abschmecken.

