# Ripperl mit warmem Krautsalat

* 1 Haufen Ripperl
* 1 Kopf Weißkraut
* 1 Knolle Koblauch
* Speckwürfel
* ganzer und gemahlener Kümmel
* edelsüßer Paprika
* Salz

**Ripperl**

Knoblauch pressen, salzen und mit gemahlenem Kümmel und Paprika vermischen, Ripperl damit einreiben.

Eingeriebene Ripperl in Auflaufform mit 2 cm Wasser im Rohr bei 200°C oder mehr backen.

> Tipp: Bei Bedarf Wasser dazu. Wasser muss immer Boden bedecken!

Nach 15-20 Minuten Ripperl umdrehen und nochmal 15-20 Minuten backen.

**Krautsalat**

Kraut fein schneiden, mit Salzwasser und ganzem Kümmel kurz aufkochen, ausschalten, mit geschlossenem Deckel ziehen lassen bis das Kraut weich ist (ca 10 Minuten). Dann abseihen, mit gebratenen Speckwürfeln, Essig und Salz abschmecken.

> Tipp: Kraut nicht zu früh aufkochen sonst wird es kalt bis die Ripperl fertig sind


