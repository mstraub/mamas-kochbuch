# Grießknödel mit Kohlrabi

> [!CITE] Klassiker von Mama

**Grießknödel (\~5 Personen)**

* 80 dag Grieß
* 3 Schöpfer Suppe (aus einem Suppenwürfel)
* 6 dag Fett (flüssig, am besten aus Speck ausgelassen)
* Speckwürfel
* Salz

1. Flüssiges Fett mit Grieß vermischen
2. Suppe dazugeben und gut durchkneten
3. Knödel im Salzwasser kochen

**Kohlrabi**

* 3 große Kohlrabi
* 1 TL Zucker
* 1-2 Becher Rahm

1. Zucker mit etwas Öl karamellisieren
2. würfelig geschnittenen (oder geriebenen) Kohlrabi dazu
3. mit etwas Wasser ablöschen, etwas salzen
4. dünsten, am Ende mit Mehl binden und mit Rahm servieren
