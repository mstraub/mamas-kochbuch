# Brathendl

* ein ganzes Hendl
* Knödelbrot
* Ei
* Rosmarin
* Salz

> [!NOTE] Markus' Erfahrung: für ein 1,3 kg Hendl waren 300 g Knödelbrot mehr als genug Füllung (drei extra Knödel!)

1. Knödelbrot mit Petersilie, Salz, Ei vermischen, dann heißes Wasser dazugeben bis die Konsistenz passt. Rasten lassen.
2. Hendl in Bratform legen, innen und außen salzen und mit gehacktem Rosmarin einreiben.
3. Hendl mit Knödelteig stopfen, mit Zahnstochern o.ä. Haut zumachen
4. Hals und Magen neben Hendl in Form legen, Leber in Pfanne braten (erst *nach* dem Braten salzen).
5. Hendl auf den Bauch drehen, Boden mit Wasser bedecken (und zwischendurch immer wieder nachgießen, so dass er immer bedeckt bleibt), bei 200°C Umluft für ca. 30 Minuten braten.
6. Danach umdrehen und nochmals ca. 30 Minuten braten. (bei großen Hendln kann es insgesamt auch 1,5 h dauern)

> [!TIP] Als Beilage eignen sich Erdäpfel, die man einfach mit in die Bratform legt

