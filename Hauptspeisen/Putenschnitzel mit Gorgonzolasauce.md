# Putenschnitzel mit Gorgonzolasauce

* 60 dag Putenschnitzel
* 10 dag Gorgonzola
* 2 Zwiebel
* 250 ml klare Suppe
* 2 Becher Frischkäse
* Schnittlauch

Zwiebel klein schneiden und in etwas Öl glasig dünsten. Mit der klaren Suppe ablöschen und darin Frischkäse sowie Gorgonzola auflösen, bis sich eine cremige Masse bildet. Anschließend den Schnittlauch unterrühren. Die Putenschnitzel in einer Auflaufform verteilen und die Sauce darüber geben und im Ofen bei 180° für ca. 30 bis 40 Minuten braten lassen.

Dazu passt z.B. Reis oder Nudeln.


