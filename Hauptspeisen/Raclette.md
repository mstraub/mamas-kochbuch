# Raclette

Einkaufsliste (für 2 Personen):

* 50 dag Fleisch
* 1 Pkg. Shrimps
* 2 Pkg. Raclettekäse (Hofer)
* 15 dag Champignon
* 1 Paprika
* 10 kleine Tomaten (zum drauflegen)
* 10 Zehen Knoblauch (zum drauflegen)
* 6 Kartoffel
* Oliven
* Salat
* Saucen wie bei Fondue

