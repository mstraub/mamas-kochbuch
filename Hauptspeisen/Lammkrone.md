# Lammkrone

* 2 Stk. Lammkronen
* 3 Stk. Knoblauchzehen
* 2 Zweige Thymian
* 1 Zucchini
* 1 Paprikaschote (rot und gelb)
* 2 Stangen Sellerie
* 1 Stk. Karotte
* 1 Stk. Fenchel
* 125 ml Kalbsfond
* Dijonsenf
* Meersalz
* Pfeffer
* 4 EL Butter
* Olivenöl
* Erbsenschoten

Das Lammfleisch mit Dijonsenf bestreichen und mit Salz und Pfeffer ordentlich würzen. Die Zucchini in dünne Scheiben schneiden. Die Paprikaschoten entstielen, entkernen und in kleine Würfel schneiden. Den geschälten Sellerie in etwa 2 cm große Stücke, die geputzte Karotte in Scheiben schneiden und den Fenchel vierteln. In einer Pfanne etwas Olivenöl erhitzen und die Lammkrone von beiden Seiten scharf anbraten. Mit der Fleischseite nach oben in das auf etwa 170-180°C vorgeheizte Backrohr stellen und je nach gewünschtem Garungsgrad (für medium am besten etwa 10-12 Minuten) braten. Das Lamm herausnehmen, in Alufolie wickeln und einige Minuten reasten lassen. In der Zwischenzeit die Zucchini, Karotten, Paprika und Sellerie nacheinander in wenig Öl anbraten, mit dem Fond aufgießen und den Fenchel dazugeben. Jeweils einen Zweig der Kräuter fein hacken, zugeben und alles etwa 10 Minuten zart köcheln lasse. Mit Salz und Pfeffer abschmecken. Nun 1 (!) Esslöffel Butter in einer Pfanne schmelzen lassen. Die restlichen Kräuter fein hacken, gemeinsam mit dem in Streifen geschnittenen Knoblauch zugeben und das Lamm einlegen. Nochmals rundum kurz nachbraten. Die Lammkronen aus der Pfanne heben und jeweils halbieren. Die restliche Butter unter das Gemüse rühren und das fertige Gemüse auf Tellern anrichten. Je eine Lammkronenhälfte darauf drapieren und mit den Kräutern, den in Butter geschwenkten Erbsenschoten sowie Tomaten garnieren.

