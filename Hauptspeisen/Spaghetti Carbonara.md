# Spaghetti Carbonara

> [!CITE] von [lecker.de](https://www.lecker.de/klassische-spaghetti-carbonara-73209.html), echt italienisch (nicht die österreichische Variante mit Schlagobers und Schinken) 

**Zutaten für 4 Personen**

* 100 g Pecorino (Stück)
* 100 g Pancetta (italienischer Bauchspeck)
* 2 Knoblauchzehen
* 500 g Spaghetti
* 2 EL Olivenöl
* 1 Ei (Gr. M)
* 3 Eigelb (Gr. M)
* Salz, grober Pfeffer

1. 3 L Salzwasser (1 TL Salz pro Liter) aufkochen.
2. Käse fein reiben und Pancetta in kleine Würfel schneiden.
3. Knoblauch schälen und fein hacken.
4. Nudeln ins kochende Salzwasser geben und nach Packungsanweisung garen.
5. Pancetta in Öl anbraten, Knoblauch kurz mitbraten.
6. Nudeln abgießen, dabei ca. 4 EL Kochwasser auffangen.
7. Nudeln in die Pfanne zum Pancetta geben und alles gut mischen.
8. Ei, Eigelb, Kochwasser und die Hälfte Käse verquirlen, mit Pfeffer würzen.
9. Nudelmix in eine vorgewärmte Schüssel umfüllen, Ei-Käse-Masse zugießen und schnell mit den Nudeln mischen.
10. Mit Rest Käse und Pfeffer bestreuen.
