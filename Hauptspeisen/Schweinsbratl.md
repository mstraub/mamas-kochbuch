# Schweinsbratl

> [!CITE] frei nach [Werner Gruber](https://www.youtube.com/watch?v=oaRyYuUbttI) ([bzw. dieser Variante](https://www.ilseblogt.at/der-perfekte-schweinsbraten-nach-werner-gruber-aus/)) mit Markus' Anmerkungen.

**Zutaten**
* ca. 1,5 kg Schweinsschulter, mit Schwarte und schönem Fettrand
* 7 EL Salz
* 1 EL Koriander, zerstoßen
* 2 EL Kümmel
* 6 Zehen Knoblauch, zerdrückt (war schon sehr intensiv, evtl. reduzieren)
* 15-25 dag Butter
* Optional: Erdäpfel

**Zubereitung - Zusammenfassung**

1. 24-48 h Marinade einziehen lassen
2. 45 min braten (dann umdrehen und einschneiden)
3. 60 min braten (dann Kruste kontrollieren)
4. max. 15 min stark braten (Kruste bilden)
5. 15 min rasten (vor dem Anschneiden)

> [!TIP] **Faustregel zur Bratdauer:** ~1h für 1kg Braten.
> Erfahrungen Markus: Mein 2,3 kg Braten war nach 3h mit 180° (und 15 Minuten Grillen) perfekt - knuspriges Schwartl und innen saftig.

**Zubereitung im Detail:**

1. Das Fleisch mit ~~sechs~~ drei EL Salz, dem Koriander, dem Kümmel und dem Knoblauch einreiben. In einen Kunststoffbeutel geben und verschließen. Einen Tag rasten lassen.
2. Fleisch in eine große Kasserolle legen:
    - Salzige Flüssigkeit wegleeren, Gewürze und Knoblauch am Braten lassen
    - Schwarte nach unten
    - Ca. 1/2 L heißes Wasser in die Kasserolle geben (bzw. bis ca 2-3 cm Flüssigkeit steht) 
    - Auf das Fleisch etwas (bis zu 25 dag) Butter legen
3. Optional: geschälte Erdäpfel dazulegen (bekommen eine wunderbare braune Kruste)
4. Braten 45 Minuten bei 180°C ins Backrohr geben.
5. Braten rausfischen, die jetzt weichgekochte Schwarte einschneiden und mit 1 EL Salz würzen (hilft für Knusprigkeit). 
6. Braten wieder in die Kasserolle legen, diesmal ist aber die Schwarte oben. Vielleicht noch etwas Wasser dazugeben, aber unter gar keinen Umständen den Braten mit Wasser übergießen.
7. Nun das Fleisch für rund eine Stunde im Backrohr lassen.
8. Danach noch einmal für 15 Minuten bei maximaler Leistung des Backrohrs braten. In dieser Phase sollte man Vorsicht walten lassen. Es hängt vom Backrohr ab, bis eine wunderbare Kruste entsteht. Bei manchen Backrohren erreicht man das gewünschte Ergebnis schon nach sieben Minuten, bei manchen muss man fast eine halbe Stunde warten, bis man ein brauchbares Ergebnis hat. Natürlich sollte man dies bezogen auf die Gesamtbratdauer berücksichtigen. Es hilft auch, das Backrohr einen Spalt zu öffnen.
9. Danach den Braten rund 15 Minuten rasten lassen - nur so bleibt der Saft im Braten.
10. Mit Knödeln (z.B. Semmelknödel, Mehlknödel), Erdäpfeln und Krautsalat servieren.


## Warmer Krautsalat mit Speck auf Mühlviertler Art

> [!CITE] von [schlattbauerngut.at](http://schlattbauerngut.at/warmer-krautsalat-muhlviertler-art/), schmeckt wie von Waldinger Oma

Für 4-6 Personen, Zubereitungszeit ca. 40-45 min

* 1 Kopf Weißkraut (mittelgroß)
* 3 EL Sonnenblumenöl
* 3 EL Apfelessig
* 1 EL Zucker
* 1/2 TL Kümmel (ganz)
* roher gewürfelter Speck (laut Rezept 50-80 g, kann aber weit mehr sein, z.B. 200g in Streifen)
* ca. 400 ml Wasser
* Salz, Pfeffer

1. Weißkraut vierteln und Strunk herausschneiden, anschließend mit Maschine in feine Streifen hobeln.
2. In Kochtopf Wasser mit Kümmel aufkochen, Weißkraut beigeben und solange kochen bis das Kraut bissfest ist. Achtung: Wasser nicht wegschütten.
3. Sollte nach dem Kochvorgang noch zu viel Flüssigkeit vorhanden sein, etwas davon ausleeren. Das Kraut muss aber in einer relativ flüssigen Marinade sein.
4. Öl, Essig, Zucker, Salz und Pfeffer dazugeben und gut vermischen.
5. Speck in der Pfanne etwas anrösten und unter den Krautsalat mischen.

> [!TIP] Am Vortag machen, schmeckt aufgewärmt noch besser

## Mühlviertler Mehlknödel

> [!cite] Vom Essen auf dem Lande, Seite 261, wie von Waldinger Oma
> auch genannt: Staubadö Knoan, Staubige Knödel

- 500 g glattes Weizenmehl (alternativ auch mit Roggenmehl)
- einige EL flüssiges Fett (Öl oder Schmalz)
- Salz
- kochendes Wasser

1. Mehl mit Fett und Salz gut vermischen
2. *Wenig* kochendes Wasser dazumischen, so dass ein trockener krümeliger Teig entsteht
3. Ohne den Teig rasten zu lassen schnell Knödel formen und in Salzwasser langsam kochen
