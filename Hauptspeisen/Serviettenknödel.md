# Serviettenknödel

> [!CITE] von Mama

* 25 dag Knödelbrot
* 3 Eier
* 1/2 L Milch
* 2 Zwiebel
* 1 EL Mehl

Knödelbrot mit Eiern und Milch übergießen, rasten lassen. Einen Knödel formen, z.B. in ein einem Geschirrtuch einwickeln und in kochendes (!) Salzwasser legen, 15-20 min kochen.

## Variante

> [!CITE] Das große Sacher Kochbuch, Seite 392

* 5 leicht entrindete gewürfelte Semmeln (gleiche Menge Knödelbrot)
* 3/16 L Milch (bei Bedarf etwas mehr)
* 3 Dotter, 3 Klar
* 6 dag Butter
* Salz, Muskat, Petersilie, Butter für die Serviette

Semmeln mit flüssiger Butter gut mischen, Dotter, Milch, Salz, Muskat, Petersilie verquirlen darübergießen, gut mischen, verdichten und 1/2 Stunde rasten lassen. Schnee von 3 Klar unter die Semmelmasse ziehen. Baumwollserviette (Geschirrtuch) mit Butter bestreichen,(dort wo die Teigrolle anliegt) Knödelmasse fest zusammendrücken und zu einer Rolle formen 7 -8 cm Durchmesser, auf die bebutterte Serviette legen und einrollen. Die Enden zusammenbinden - in Salzwasser ca 35 min kochen. Rolle vor dem Öffnen etwas rasten lassen, dann in dicke Scheiben schneiden.
