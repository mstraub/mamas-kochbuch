# Schinkenfleckerl (gebacken)

> [!cite] von Ulli (Fellinger)

- 500 g Fleckerl (Nudeln)
- 500 g Rollschinken (geräuchert)
- 250 g Butter
- 4 Eier
- ca. 150 g Rahm
- Salz, Pfeffer

**Zubereitung**
1. Fleckerl kochen
2. Eier trennen und Schnee schlagen
3. Abtrieb: Butter schaumig rühren, nach und nach Dotter beigeben, salzen und pfeffern
4. Abgekühlte Nudeln zum Abtrieb mischen
5. Rahm und Hälfte des Schnees untermischen
6. Reindl einfetten, Masse einfüllen, restlichen Schnee draufstreichen
7. ca. 40 Minuten bei 175°C Ober/Unterhitze backen

![](Schinkenfleckerl.jpg)