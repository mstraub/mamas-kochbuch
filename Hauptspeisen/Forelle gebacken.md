# Forelle gebacken

> [!CITE] von Mama

* Ganze Forelle(n), pro Person ein Fisch mit ~300g passt gut
* Mandelblättchen
* Butter
* Zitronensaft

1. Forellen außen und innen salzen und mit Öl beträufeln
2. Außen leicht mit Bröseln panieren
3. Forellen nebeneinander in Reindl legen
4. Bei 180-190°C Umluft backen, bis die Augen weiß sind
5. Umdrehen, auf anderer Seite backen, bis die Augen fast weiß sind (evtl. mit Gabel testen, ob sich das Fleisch von den Gräten löst)
6. Ungefähr 5 Minuten vor Ende Mandelblättchen auf Forellen legen
7. Beim Servieren Forellen mit geschmolzener (aufgeschäumter aber nicht brauner) Butter beträufeln, evtl. auch mit Zitronensaft

> [!TIP] Beilagen: Petersilienerdäpfel und Salat

[Andere Variante](http://www.mamas-rezepte.de/rezept_Forellen_mit_Mandeln-5-1424.html)