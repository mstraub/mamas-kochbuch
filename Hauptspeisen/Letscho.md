# Letscho (Lecsó)

> [!cite] ungarisch


- 500 g Spitzpaprika
- 500 g Tomaten
- 3 Zwiebel
- Salz, Pfeffer

**Zubereitung**
1. Zwiebel in Öl glasig anschwitzen
2. Paprika und Tomaten (würfelig) dazu
3. Köcheln lassen (ca. 15 Minuten) und würzen

Optional:
- Paprikapulver / -paste zum Würzen
- Fleisch / Wurst / Speck als Einlage
- zu Beginn Reis dazugeben und mitkochen
- Am Ende Ei entweder untermischen oder a la Shakshuka als ganzes garen