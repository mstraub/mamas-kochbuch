# Krautfleckerl

> [!CITE] Quelle: [Tobias Müller, derstandard.at](https://www.derstandard.at/story/2000142333412/die-suche-nach-den-perfekten-krautfleckerln)
> Alternativ: Das große Sacher Kochbuch Seite 399

> [!NOTE] Anmerkungen Markus:
> - Ich habs mit 100g Speck gekocht, sehr gut
> - Menge: 1 kg Kraut + 400 g (trockene) Nudeln passt für 4 hungrige Erwachsene

**Zutaten**

Für das Kraut

- 1 kleines Spitzkraut, egal ob frisch oder gelagert
- Salz
- 2 EL Zucker
- 1-2 EL gutes Schmalz
- 2 kleine Zwiebel, in Ringe geschnitten
- 1 Lavendelblüte, gemahlen
- 1 TL Kümmel, zerstoßen
- 1 TL Fenchelsamen, zerstoßen
- 1 ordentlicher Schuss Essig
- Frisch gemahlener schwarzer Pfeffer
- 1 Schuss Suppe, Gans oder Rind oder Huhn oder was da ist

Für die Fleckerl

- 120g Weizenmehl Universal
- 4 Eigelb
- 15ml Milch oder Öl

**Zubereitung**

1. Am Abend vor dem Essen (oder mindestens eine Stunde davor) zwei Drittel des Krauts schneiden, ein Drittel auf einer groben Reibe reiben. Gut salzen und stehen lassen.
2. Aus allen Zutaten für die Fleckerln einen Teig kneten. Achtung: er wird sehr trocken sein! In Folie gewickelt mindestens eine Stunde im Kühlschrank rasten lassen. Dann in einer Pastamaschine bis auf Stufe sechs ausrollen und in grobe Quadrate schneiden.
3. In einem kleinen Topf über mittelhoher Hitze den Zucker (Feinkristall) schmelzen und karamellisieren lassen. Falls der Zucker an manchen Stellen im Topf schneller schmilzt als an anderen, ein wenig frischen Zucker drüberstreuen. Wenn er über den ganzen Topfboden kleine Bläschen wirft und die gewünschte Farbe erreicht hat, rasch mit Wasser ablöschen, das Karamell auflösen und kurz einkochen lassen, bis ein dicker Sirup entsteht. In ein Schüssel gießen und für später zur Seite stellen.
4. In einer großen Pfanne das Schmalz schmelzen und die Hitze auf mittel zurückschalten. Die Zwiebel darin weich und braun werden lassen. Kein Grund zur Eile, das darf durchaus etwas dauern. Das Kraut und Gewürze zugeben und kurz mitbraten, dann mit einem ordentlichen Schuss Essig ablöschen. Den Deckel auflegen und auf niedriger Hitze dünsten, bis das Kraut ganz weich ist, etwa 20 Minuten.
5. Währenddessen die Fleckerln (sehr) bissfest garen, abgießen und kalt abschrecken.
6. Wenn das Kraut weich ist, den Karamellsirup einrühren und mit Salz und nochmals Essig abschmecken. Mächtig pfeffern. Die kalten Fleckerln zugeben, mit einem Schuss Suppe aufgießen und noch ein paar Minuten unter häufigem Rühren fertig garen. Mit Pfeffermühle servieren und genießen.

## Variante
>[!CITE] Quelle ??

* 300 g Fleckerl
* 500 g Weißkraut
* 120 g Speck (mit kräftigem Geschmack)
* 2 EL Kristallzucker
* 1 Stück Zwiebel
* Öl (für die Pfanne)
* 1 kräftiger Schuss Süßwein
* Pfeffer + Salz
* Gemüsesuppe (zum Aufgießen)
* Kümmel
* Petersilie (zum Garnieren)

1. Zwiebel schälen und fein hacken. Die äußeren Blätter und den Strunk vom Krautkopf entfernen und diesen in kleine Stücke schneiden.
2. Von einem kräftigen Speck die Schwarte wegschneiden und in Würfel schneiden.
3. In einer großen Pfanne wenig Öl erhitzen und den Speck darin anbraten. Die Zwiebeln hinzugeben und glasig dünsten. Einen Löffel Zucker darüberstreuen und alles kurz karamellisieren lassen.
4. Kraut hinzugeben, kurz anrösten und mit Süßwein ablöschen. Mit Salz, Pfeffer und Kümmel würzen. Suppe hinzugeben.
5. Kraut ca. 20 Minuten auf kleiner Flamme köcheln lassen, sodass es weich ist, aber trotzdem noch ein wenig Biss hat.
6. Fleckerl kochen so dass sie gleichzeitig mit dem Kraut fertig sind.
7. Vor dem Servieren die Fleckerl zu dem Kraut hinzugeben und in der Pfanne nochmals kurz erhitzen.
8. Die Krautfleckerl mit Speck mit Petersilie garnieren und servieren.
