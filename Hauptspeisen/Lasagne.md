# Lasagne

> [!cite] von Mama

für ca. 4 Personen

**Ragout Bolognese**

* 50 dag Faschiertes
* 2 Dosen geschälte, gewürfelte Tomaten
* 1 Zwiebel
* 2 Knoblauchzehen
* 1 EL Tomatenmark
* Sellerie, Karotten
* Oregano, Majoran, div. Gewürze (Lorbeerblätter)
* Petersilie
* Olivenöl

1. In einem Topf das Olivenöl erhitzen, die gehackten Zwiebeln anrösten, dann das Faschierte darin rundherum anbraten.
2. Knoblauchscheiben und Tomatenmark dazu rühren und mitbraten.
3. Mit den Dosentomaten aufgießen, Gemüse dazu, salzen, pfeffern, mit Oregano, Majoran und div. Gewürzen abschmecken.
4. Das Ragout mindestens eine halbe Stunde lang bei geöffnetem Topf einkochen lassen.

**Béchamelsauce**

* 3/4 L Milch
* 4 dag Butter
* 4 dag Mehl
* Salz, Pfeffer, Muskatnuss

1. Butter in einem kleinen Topf schmelzen und das Mehl mit dem Schneebesen dazuarbeiten.
2. Die Milch nun langsam dazugießen und die Sauce glatt rühren.
3. Die Sauce auf kleiner Flamme köcheln lassen (Gourments meinen: fast eine halbe Stunde lang damit sie den Mehlgeschmack verliert)
4. Mit Salz und Pfeffer sowie etwas Muskatnuss abschmecken. Optional kann auch etwas Zitronensaft hinzugegeben werden.

> [!tip] Wer zu langsam gerührt hat und Klümpchen in der Sauce findet, kann die Sauce durch ein feines Haarsieb passieren und dann weiterkochen lassen.

**Zubereitung**

* 35 dag Lasagneplatten
* Käse gerieben

1. In einer feuerfesten Form etwas Ragout Bolognese verteilen, eine Schicht Lasagnenudeln darauf legen
2. Anschließend so oft bis die Form voll ist: Bechamel, Ragout und Lasagneblätter drauflegen
3. Die letzte Schicht sollte die Bechamelsauce bilden
4. Dick mit geriebenem Käse bestreuen und die Lasagne bei 180°C im Ofen überbacken, bis die Kruste goldbraun ist

> [!note]
> - Geriebener Käse zwischen den Schichten schmeckt auch sehr gut
> - Die Lasagne kann man auch gut einen Tag vorher vorbereiten und im Kühlschrank ziehen lassen

> [!tip] Fleichlose Alternativen
> 1. Spinat + Lachs statt Bolognese
> 2. Faschiertes in Bolognese weglassen, stattdessen Süßkartoffelscheiben in die Lasagne schichten
> 3. Bolognese mit Sojagranulat:
> statt Faschiertem 18 dag Sojagranulat mit 150 ml Suppe einweichen,
> kurz mit den Zwiebeln anrösten, dann mit 3/8 L Rotwein ablöschen.

