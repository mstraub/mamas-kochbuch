# Bärlauch-Ravioli mit Zitronenbutter

> [!CITE] von [kuechengoetter.de](https://www.kuechengoetter.de/rezepte/baerlauch-ravioli-mit-zitronenbutter-1727)

**Zutaten**

*Nudelteig*

* 300 g Mehl
* 3 Eier
* 2 Eigelbe
* Salz
* Mehl zum Ausrollen

*Füllung*

* 2 EL Olivenöl
* 100 g Bärlauch
* 250 g Ricotta
* 50 g geriebener Parmesan
* 2 EL Semmelbrösel
* Pfeffer

*Zitronenbutter*

* 100 g Butter
* 1-2 EL Zitronensaft

**Zubereitung**

*Nudelteig*

1. Mehl auf die Arbeitsfläche häufen, eine Mulde eindrücken.
2. 3 Eier, 1 TL Salz und Öl hinzufügen.
3. Alles mit den Händen zu einem geschmeidigen Teig verkneten, bei Bedarf etwas Wasser zugießen.
4. Zur Kugel formen und abgedeckt 30 Min. ruhen lassen.

*Füllung*

1. Für die Füllung Bärlauch waschen, trockentupfen und die Stängel abzwicken.
2. 2 Blätter beiseite legen, Rest fein hacken.
3. Mit Ricotta, Eigelben, Parmesan und Bröseln vermengen. Salzen und pfeffern.

*Ravioli*

1. Teig in vier Portionen teilen.
2. Das erste Viertel auf der bemehlten Fläche oder mit der Nudelmaschine dünn ausrollen.
3. Darauf im Abstand von 5 cm Häufchen von der Füllung setzen.
4. Die Teigfläche dazwischen dünn mit Wasser bestreichen.
5. Das zweite Viertel dünn ausrollen und auf den gefüllten Teig legen.
6. Teig rund um die Füllungen leicht andrücken.
7. Mit einem Teigrädchen Ravioli ausschneiden (oder Keksausstecher verwenden).
8. Übrigen Teig genauso füllen.
9. Teigtaschen in kochendem Salzwasser 4-5 Min. garen, abgießen und abtropfen lassen.
10. Butter zerlassen, Zitronensaft einrühren, erhitzen, salzen und pfeffern.
11. Übrigen Bärlauch in feine Streifen schneiden, einstreuen.
12. Zitronenbutter über die Ravioli gießen.

> [!TIP] geht gut zum Einfrieren!
> bei unserem nicht gerade hauchdünn ausgerolltem Teig gefroren ins wallende Wasser und dann 10 Minuten ziehen lassen / wallen

