# Quiche Lorraine

Mischung aus [Teig von hier](https://www.kuechengoetter.de/rezepte/lieblings-quicheteig-cafe-solo-20016)
und [Füllung von hier](https://www.franzoesischkochen.de/quiche-lorraine/#)

**Teig**

- 125 g kalte Butter
- 125 g Topfen (40%)
- 200 g Mehl
- 1/2 TL Salz

1. Butter klein würfeln, alle Zutaten flott zusammenkneten.
2. Teig in Folie wickeln und 15-30 Minuten in den Kühlschrank legen.
3. Springform befetten und bemehlen
4. Teig dünn ausrollen, einlegen, mit Gabel einstechen
5. Teig bei 180°C Umluft vorbacken (ca. 10 Minuten, goldgelb)

**Füllung**

- 4 große Zwiebeln
- 300 g Schinkenspeck
- 250 g griechisches Joghurt
- 250 g Crème fraîche
- 2 Eier
- Muskatnuss, Salz und Pfeffer.

1. Speck und Zwiebel klein schneiden und in einer Pfanne anbraten (glasig)
2. Mit den restlichen Zutaten vermischen und würzen
3. Auf den vorgebackenen Boden geben
4. Bei 180°C Umluft backen (30-45 Minuten)

> [!TIP] Erfahrung Markus: je nach Speckart ist kein Salz nötig.
> Mit klassischen österreichischem Speck ist es schon fast zu salzig.
