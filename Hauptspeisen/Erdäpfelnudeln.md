# Erdäpfelnudeln

> [!CITE] [Adaptiertes](http://www.ichkoche.at/muehlviertler-erdaepfelnudeln-rezept-10581) [Rezept](http://www.ichkoche.at/erdaepfelnudeln-wutzlnudeln-oder-schupfnudeln-rezept-2009), schmeckt wie von Waldinger Oma!

Menge für 4 Personen, für Markus' grünes Reindl zu viel (nächste Mal im silbernen)

**Zutaten**

Erdäpfelteig
* 1 kg mehlige Erdäpfel
* 200 g Universalmehl
* 100 g Erdäpfelmehl (Stärke)
* 2 Dotter
* 1 EL Salz

Fürs Reindl
* Schweineschmalz (oder Butter(schmalz))

Ei-Rahm-Guss
* ¼ L Sauerrahm
* 1 Ei
* Salz, Pfeffer, Muskat

Dazu
* Speckwürfel
* ½ - 1 kg Sauerkraut

**Zubereitung**

1. Erdäpfel kochen und heiß durch die Erdäpfelpresse drücken.
2. Mit Mehl und Dotter vermengen und fingerdicke Nudeln formen.
3. Schmalz im Reindl schmelzen.
4. Reindl schräg stellen, die Nudeln dann zuerst kurz in das untere Fett tunken, drehen und oben schön dicht auflegen. Das verhindert, dass sich die Nudeln gleich mit zu viel Fett ansaugen und dass generell nicht zu viel Fett im Reindl bleibt.
5. Im Rohr bei 190-200°C bei Ober- und Unterhitze ca. 15-20 Minuten backen, bis die Nudeln schön knusprig sind.
6. Danach Sauerrahm mit Ei (und übrigem Eiklar) vermengen, mit Salz, Pfeffer und Muskat würzen.
7. Die Masse über die Nudeln geben, nochmal in das Rohr geben, und bei etwas weniger Hitze stocken lassen. (Bei 140°C ungefähr 10 Minuten).
8. Speckwürfel in Pfanne anbraten, Nudeln mit Speck bestreuen und mit Sauerkraut servieren.

