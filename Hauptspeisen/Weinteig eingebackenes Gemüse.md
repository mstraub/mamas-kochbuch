# Weinteig (eingebackenes Gemüse)

**Zutaten für 3 Personen**

* 1/8l Weißwein
* 12 dag Mehl
* 1 EL Öl
* Salz
* Schnee von 2 Eiern

Wein mit Mehl, Salz und Öl mischen, dann Schnee unterheben. Gemüse vordünsten/kochen (z.B. Karfiol 10 min in Salzwasser kochen), dann in Teig tunken und in der Pfanne in Öl ausbacken.
