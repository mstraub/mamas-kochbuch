# Geschäumte Maronisuppe

ca. für 6 Personen

50 dag gekochte geschälte Maroni, 3 Schalotten oder 1 Zwiebel, 2 EL
Olivenöl,1 Liter Rindsuppe,125 ml Schlagobers, 1 EL Sauerrahm,
Salz,Pfeffer, Zitronensaft, 6 gehobelte Maroni als Suppeneinlage

Schalotten schälen.klein schneiden und in Öl anschwitzen. Maroni
untermischen und kurz mitgaren. Suppe zugießen und die Maroni darin
weichkochen. Obers zugießen, Rahm einrühren und die Suppe fein pürieren.
Mit Salz, Pfeffer und Zitronensaft abschmecken.

mit geschlagenem Obers und gehobelten Maroni garniert servieren.

