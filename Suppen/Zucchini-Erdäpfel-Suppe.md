# Zucchini-Erdäpfel-Suppe

- 750 ml Gemüsesuppe
- 50 dag Zucchini
- 20 dag Erdäpfel
- 1 Zwiebel
- 2 Knoblauchzehen
- optional: Rahm / Schlagobers

**Zubereitung**

1. Zwiebel und Knoblauch fein schneiden und in Öl glasig anbraten
2. Grob geschnittenes Gemüse kurz mit anbraten, dann mit Suppe ablöschen
3. ca. 20 Minuten köcheln, dann grob pürieren

> [!NOTE] Markus: trivial, aber sehr gemüsig und schmeckt auch Fridolin :)
