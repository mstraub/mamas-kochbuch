# Rote-Rübensuppe mit Krennockerl

> [!CITE] von Mama

ca. für 4 Personen

- 20 dag Rote Rüben
- 1 kleine Zwiebel
- 750 ml Rindsuppe
- 250 ml Schlagobers (evtl. weniger, je nach Geschmack)
- 1 EL Essig
- 1 Lorbeerblatt, Kümmel, Salz&Pfeffer

Rüben kochen (ca. 30 Minuten), reiben. Zwiebel rösten geriebene Rüben
und Wasser dazu, Essig, Lorbeerblatt, Suppenwürfel und Kümmel dazu und
kochen. Am Schluss mixen und Schlagobers zugeben.

> [!TIP] Markus: mehr Rüben sind auch gut, z.B. 40 dag!

## Krennockerl

- 1 Ei
- 6 dag Butter
- 6 dag Mehl
- frisch geriebener Kren
- Salz, Pfeffer

Schnee schlagen, Dotter mit bereits abgetriebener Butter vermengen, Mehl
dazu, dann ca. Hälfte von Schnee unterheben und mixen, zum Schluss
restl. Schnee nur unterheben → eher fester Teig. In kochendes Salzwasser
legen, 10 min köcheln und dann ziehen lassen.

