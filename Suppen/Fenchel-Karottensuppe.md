# Fenchel-Karottensuppe

- 2 große Karotten (in Scheiben)
- 1 Fenchelknolle (in Streifen)
- 1 Erdapfel (kleine Würfel)

Alles in 1 EL Olivenöl anschwitzen, mit 750 ml Gemüsebrühe weich
dünsten.

Mit Kurkuma, Kräutersalz, Apfelessig und gehacktem Fenchelkraut
abschmecken.

