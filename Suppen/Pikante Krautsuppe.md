# Pikante Krautsuppe

½ fein gewürfelte Zwiebel und 2 fein gehackte Knoblauchzehen in etwas Öl
anschwitzen.

150 g Weißkraut,1 geschälte Karotte und eine kleine Lauchstange in
Streifen schneiden,

2 kleine festkochende Erdäpfel schälen und würfeln.

Alles zu Zwiebel und Knoblauch geben und kurz anschwitzen.

400 ml Gemüsebrühe zugeben und ca 15 min köcheln, bis das Gemüse weich
ist.

Mit Salz, Kümmel, Pfeffer und Schnittlauch verfeinern.


