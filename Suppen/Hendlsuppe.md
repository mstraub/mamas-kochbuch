# Hendlsuppe

> [!CITE] Adaptiert von [hier](https://deeskueche.de/huehnersuppe/)  und [hier](https://www.lapura.at/huehnersuppe/)

> [!INFO] Wissenschaftlich erwiesen: hilft bei Erkältung!
> *Rennard, Barbara O., et al. "Chicken soup inhibits neutrophil chemotaxis in vitro." Chest 118.4 (2000)*


**Zutaten**

- 1 Suppenhuhn (~1,5 kg)
- 1 Zwiebel
- Suppengrün (Menge nach Belieben), z.B.
  - Karotten
  - Sellerie
  - Peterwurzel
  - Lauch
  - gelbe Rüben
- 1 Hand voll Erbsen (optional)
- Gewürze
  - 1 Bund Petersilie
  - 3 Nelken
  - 4 Pfefferkörner
  - 2 Lorbeerblätter
  - 5 Wacholderbeeren
  - Pfeffer und Salz

**Zubereitung**

1. Hendl waschen, mit grob geschnittenem Gemüse und den Gewürzen in Topf geben
2. Mit kaltem Wasser (~2 L auffüllen)
3. Langsam erhitzen und für etwa 2 Stunden leicht köcheln lassen
4. Suppe abseihen, Hendl ausboaln (e.g. Knochen und Knorpel weg), alles wieder zusammengeben

**Tipps**

- eine Tomate mitkochen für mehr Umami-Gesschmack
- am besten ein echtes *Suppenhuhn* verwenden. Im Unterschied zu Masthühnern sind Suppenhühner Legehühner, die bereits etwas älter sind und um die 2 kg wiegen. Ihr Fleisch hat einen hohen Fettanteil, der allen Hühnersuppen Rezepten den intensiven Geschmack gibt

