# Marillenknödel

> [!CITE] von [derstandard.at](http://derstandard.at/2000060706070/Rezept-Marillenknoedel-mit-Nussbroesel)

**Knödel**

* 9 - 12 ganze Marillen
* 250 g Topfen (20 % Fett)
* 30 g Butter
* ca. 130 g griffiges Mehl
* 1 Ei
* Prise Salz

**Brösel**

* 50 g Butter
* 80 g Semmelbrösel
* 2-3 EL geriebene Walnüsse
* 2-3 EL Kristallzucker

Aus Topfen, Butter, Mehl, Salz und Ei mit dem Knethaken des Handmixers einen Teig kneten.

Den Teig mindestens eine halbe Stunde bei Zimmertemperatur zugedeckt rasten lassen.

Wenn der Teig länger im Voraus gemacht wird, muss er zum Rasten in den Kühlschrank.

Butter in einer Pfanne schmelzen. Die Brösel einrühren und leicht rösten. Die geriebenen Walnüsse einrühren und zum Schluss noch den Kristallzucker dazugeben. Unter ständigem Rühren hell anrösten. Vom Feuer nehmen und zur Seite stellen.

Achtung, wenn die Pfanne noch sehr heiß ist, können die Brösel mit der Resthitze verbrennen.

Den Teig auf die bemehlte Arbeitsfläche legen. Je nach Größe der Marillen in neun bis zwölf Stücke teilen.

Ein Teigstück nach dem anderen flachdrücken und jeweils eine Marille darauflegen. Den Teig sanft über die Marille ziehen, bis die Frucht zur Gänze umschlossen ist. Mit bemehlten Händen zu einem Knödel rollen.

In einem weiten Topf Wasser kochen, eine Prise Salz dazugeben und die Knödel darin leicht wallend kochen. Je nach Knödelgröße dauert das etwa 12 bis 15 Minuten.

Die Knödel mit einem Schaumlöffel aus dem Wasser heben und vorsichtig in den Nussbröseln rollen, bis sie rundherum angebröselt sind.

