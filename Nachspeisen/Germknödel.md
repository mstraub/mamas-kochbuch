# Germknödel

[Germteig](../Und%20sonst/Germteig.md) zubereiten, in Stücke Teilen, mit nassen oder bemehlten Händen flach drücken, einen großzügigen Teelöffel Powidl rein, zu Knödel schließen. Oder [einem](http://valentinas-kochbuch.de/rezept-von-sarah-wiener-germknoedel-mit-mohn-und-zwetschgenmus/) [Rezept](http://www.ichkoche.at/germknoedel-rezept-1822) folgen.

50 dag Mehl reichen für 3-4 Personen

Man kann die Knödel schwimmend im Wasser kochen, dadurch werden sie aber unten etwas weich und wässrig. Besser: mit einem Dampfeinsatz (oder genügend Besteck am Boden des Topfs und Alufolie drüber) garen.

Dazu dann zerlassene Butter und gemahlenen Mohn.
