# Gebackene Zwetschkenknödel

> [!CITE] von Mama

Teig siehe [Erdäpfelnudeln](../Hauptspeisen/Erdäpfelnudeln.md), Menge für 3x5 Knödel in Markus' grünem Reindl

1. Mit dem selben Teig Zwetschkenknödel formen und ins Reindl schlichten
2. Bei 180° C backen bis die Knödel an der Oberseite eine goldbraune Kruste bekommen
3. Mit doppelter Menge Sauerrahm-Ei-Gemisch (in diesem Fall mit Zimt + Zucker) übergießen und bei verringerter Hitze fertig backen.
