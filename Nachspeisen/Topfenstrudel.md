# Topfenstrudel

> [!CITE] von [steirische-spezialitaeten.at](https://www.steirische-spezialitaeten.at/rezepte/topfenstrudel-flaumiger-fuellung.html), unglaublich fluffig und geil

**Teig (ca. zwei Strudel)**, alternativ siehe [Strudelteig](../Und%20sonst/Strudelteig.md)

* 250 g Mehl
* ⅛ L lauwarmes Wasser
* 2 EL Schmalz
* ½ KL Salz
* Schmalz zum Bestreichen

**Fülle**

* 500 g Topfen
* 50 g Butter zimmerwarm
* 100 g Zucker
* Vanillezucker
* 5 Eier
* Prise Salz
* 50 g Grieß
* ¼ L Saft (Marille, Pfirsich oder was gerade zuhause ist)
* evt. Schuss Rum (für Kinder einfach weglassen)
* evt. Rosinen
* versprudeltes Ei zum Bestreichen

Für den Strudelteig aus Mehl, Salz, Wasser und Schmalz einen geschmeidigen, mittelfesten Teig kneten. Entweder mit dem Mixer oder in der Küchenmaschine bis der Strudelteig schön glatt ist.

Den Strudelteig mit Schmalz zu einer Kugel schleifen und über die Nacht kühl stellen. Durch die lange Rastzeit lässt sich der Teig am nächsten Tag besonders leicht ausziehen und verarbeiten. Tipp: Plötzlicher Topfenstrudel-Gusto und keinen Strudelteig am Vortag vorbereitet? In der Not reicht auch eine Stunde Rast, aber eine Stunde sollte es mindestens sein.

Für die Topfenfülle die Eier in Dotter und Eiklar trennen. Die zimmerwarme Butter mit 2/3 vom Zucker schaumig rühren und die Eidotter zum Schluss hin unterrühren. Topfen mit Grieß, Saft und eventuell etwas Rum glatt rühren und zur Masse hinzugeben. Wer möchte, mischt hier auch noch Rosinen unter die Topfenfülle. Man kann auch die Rosinen in Rum einweichen und dazugeben.

Die Eiklar mit einer Prise Salz und dem restlichen Zucker zu Eischnee schlagen und zum Schluss unterheben. Die Topfenfülle abschmecken und zur Seite stellen.

Ein Geschirr- oder Strudeltuch auf die Arbeitsfläche legen und mit etwas Mehl bestäuben. Den Strudelteig halbieren und einen Teil davon auf dem Strudeltuch legen. Zuerst etwas flach drücken und weiter zu einem Rechteck ausziehen. Nicht zu stark am Teig ziehen, dass Löcher entstehen!

Tipp: Eine bebilderte Schritt-für-Schritt Anleitung zum „Strudelteig ausziehen„, findet ihr bei unserem tollen Apfelstrudel Rezept!

Nun geht es ans Topfenstrudel Füllen: Dabei vom ausgezogenen Teigrand rund 5 bis 10 Zentimeter freilassen. Dann die Hälfte der vorbereiteten Topfenfülle auf den Strudelteig geben. Den Rand einschlagen und mit Hilfe vom Geschirr-/Strudeltuch den Topfenstrudel einrollen und in eine befettete Form geben. Mit dem zweiten Strudel gleich vorgehen.

Zum Schluss den Topfenstrudel mit versprudeltem Ei bestreichen und für rund eine ½ bis ¾ Stunde bei 200° in den Backofen schieben.

