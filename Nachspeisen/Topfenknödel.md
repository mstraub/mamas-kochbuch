# Topfenknödel

> [!CITE] von Mama

**Zutaten (für ca. 4-5 Personen als leichten Hauptgang)**

* 50 dag Topfen
* 10 dag Butter
* 3 Dotter
* 1 Ei
* 15 dag Mehl (griffig)
* Salz, 2 TL Vanillezucker, ½ Zitrone

1. Butter mit Dotter schaumig rühren (Mixer)
2. Ei dazugeben
3. Mit Mehl und Topfen vermengen (falls Teig noch zu weich: Brösel dazu)
4. Teig 10 min ziehen lassen
5. Mit Löffel herausstechen, mit der Hand (mit Mehl!) vorsichtig formen
6. 20 min leicht köcheln

**Honig-Sauce**

1 EL Honig, 1 EL Butter, 2 EL Wasser vermischen - Knödel mit Zimt bestauben

**Frucht-Sauce**

Erdbeeren, Waldbeeren,..

