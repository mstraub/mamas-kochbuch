# Kaiserschmarrn

## Variante Tobias Müller

>[!CITE] [Langer Artikel im Standard](https://www.derstandard.at/story/3000000185235/die-suche-nach-dem-perfekten-kaiserschmarrn)

für 1-2 Personen

**Zutaten**

- 60 g glattes Mehl (oder 70)
- 120 ml Sauerrahm ODER 100 ml Milch und 20 g geschmolzene Butter
- 2 Eier, getrennt
- 20 g Zucker
- Rosinen nach Geschmack
- etwas Rum
- Zwetschkenröster

**Zubereitung**

1. Die Rosinen in einer kleinen Schüssel mit Rum bedecken und mindestens 30 Minuten ziehen lassen.
2. Das Backrohr auf 220 Grad vorheizen.
3. Die Eier trennen. Eiweiß und Zucker mischen und steif schlagen.
4. Mehl, Eigelb und Sauerrahm bzw. Milch und Butter möglichst klumpenfrei verrühren, dann den Eischnee unterheben. Rum der Rosinen abgießen.
5. Eine Gusseisen- oder Schmiedeeisenpfanne schön heiß werden lassen und etwas Butter darin schmelzen. Die Masse hineingießen, Rosinen gleichmäßig darauf verteilen und auf mittlerer Hitze backen, bis sie unten schön Farbe genommen hat. Mit dem Schmarrnschauferl oder einem ähnlichen Instrument wenden, ins Backrohr geben und backen, bis der Schmarren durchgegart ist, etwa weitere fünf bis zehn Minuten.
6. Herausnehmen, in die gewünschte Stückgröße reißen. Bei Lust, Laune und Zeit mit etwas mehr Butter und extra Zucker die gerissenen Stücke in der Pfanne nochmals scharf anbraten. Sofort mit Zwetschkenröster servieren.

> [!NOTE] Markus: bester Kaiserschmarren ever!
> ..auch ohne Backrohr mangels passender Pfanne
> (ich schätze die Rohrvariante behält mehr Fluffigkeit?) 

## Variante Mama (?)

* 30 dag Mehl
* Milch
* 4 Eier
* Prise Salz
* Rosinen

1. Eiklar mit kaltem Wasser (½ Eischale) aufschlagen
2. Dotter mit etwas Milch und Mehl sowie einer Prise Salz verrühren, so dass sich ein zähflüssiger Teig ergibt
3. Schnee unterheben und in heißer Pfanne (Butterschmalz!) anbraten
4. Rosinen drüberstreuen und in Teig drücken, dann Deckel drauf
5. 1x umdrehen - fertig

## Variante "Omelettenteig" 

> [!CITE] Hauptschulbuch "Gemeinsam Haushalten"

für 2 Personen

* 12 dag Mehl
* 3/8 L Milch
* 4 Eier
* 1 Prise Salz

Optionale Zutaten:

* Rosinen (am besten ein paar Stunden vorher in Wasser oder Rum eingelegt)
* Zitronenzesten

1. Eier trennen und steifen Schnee schlagen.
2. Mehl, Milch, Dotter und Salz glatt rühren, Schnee unterheben.
3. Pfanne erhitzen, 1 KL Öl reingeben und verteilen (überschüssiges Öl abgießen).
4. Teig mit Schöpfer eingießen, mit Schwenken gleichmäßig verteilen.
5. Von beiden Seiten (mit geschlossenem Deckel) backen.
6. In mundgerechte Stücke zerreißen.

> [!TIP] Markus: mit weniger Eiern (z.B. 2) schmeckt es mir besser (und weniger nach Eierspeis)

Der Teig kann sowohl süß als auch pikant verwendet werden:
Süß mit Rosinen, Vanillezucker,.. als Kaiserschmarrn.
Pikant mit Käse, Paprika, Tomaten, Zucchinistückchen (wie Mamas Omelette) - mit viel Paprika genießen.

