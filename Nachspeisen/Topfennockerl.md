# Topfennockerl

* 1 TL Butter
* 25 dag Topfen
* 1 Ei
* 1 EL glattes Mehl
* 1 EL Grieß
* 1 EL Brösel

Zubereitung wie [Topfenknödel](Nachspeisen/Topfenknödel).

[bissl anderes Rezept](http://www.cucina-verde.at/altwiener-topfennockerl-rezept)

