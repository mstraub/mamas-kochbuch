# Tiramisu

> [!cite] Originalrezept von [Quelle und mehr Geschichte auf Wikipedia](https://de.wikipedia.org/wiki/Tiramisu)
> TODO: ausprobieren :) (vermutlich waren italienische Löffelbiskuits weit größer als die handelsüblichen Biskotten in Österreich?)

- 6 Eigelb  
- 150 g weißer Zucker  
- 500 g Mascarpone  
- 0,5 L Kaffee (ungesüßt) 
- ~30 Löffelbiskuits  
- Bitteres Kakaopulver zum Garnieren

## Variante mit ganzen Eiern

**Zutaten** (perfekt für Markus' grünes Reindl)

* 4 Eier
* 14 dag Zucker
* 50 dag Mascarpone
* Biskotten (ca. 60-70)
* 250 ml Kaffee
* 6 cl Amaretto
* Kakao

> [!WARNING] Erfahrung Markus: 6 cl Amaretto ist zu viel
> schmeckt schon einigermaßen hervor

**Zubereitung**

1. Dotter mit Zucker schaumig rühren und Mascarpone sowie die Hälfte des Amaretto unterheben.
2. Eiweiß zu Schnee schlagen und vorsichtig unterheben.
3. Kaffee mit restlichem Amaretto vermischen und darin die Biskotten einseitig tränken.
4. Anschließend Auflaufform mit Biskotten auslegen, ⅓ Creme, Biskotten, ⅓ Creme, Biskotten, ⅓ Creme.
5. Mindestens 12 Stunden kühl stellen und vor dem Servieren mit Kakao (optional Zimt beimengen) bestreuen.

> [!note] "Original italienische" Variante von [Galbani](https://www.galbani.de/kase-liebhaber/echte-italienische-tiramisu/) ist sehr ähnlich, aber: 6 Eier, nur 75 g Zucker, 2 EL Marsala statt Amaretto

## Variante nur Dotter

* 4 Dotter
* 6 dag Zucker
* 4 dag Staubzucker
* 50 dag Mascarpone
* Biskotten
* 4 cl Rum
* 250 ml Kaffee
* Kakao

Dotter mit Zucker schaumig rühren und Mascarpone unterheben. Kaffee mit Rum und Staubzucker vermischen und darin die Biskotten einseitig tränken. Anschließend Auflaufform mit Biskotten auslegen, ⅓ Creme, Biskotten, ⅓ Creme, Biskotten, ⅓ Creme. Ca. 3 - 4 Stunden kühl stellen und vor dem servieren mit Kakao bestreuen.

## Variante ohne Eier

* 1 kleiner Becher Mascarpone
* 1 Becher Dany+Sahne Vanille
* 3 dag Staubzucker
* 1 ½ Schachteln Biskotten
* ¼l Kaffee mit 2 Kaffeelöffel Zucker
* 2 Kaffeelöffel Rum
* Kakao

Den Becher Mascarpone, Dany+Sahne und Staubzucker verrühren. Kaffee, Zucker und Rum vermischen und Biskotten darin tränken und in Auflaufform legen. Anschließend ⅓ der Creme, wieder Biskotten, ⅓ der Creme, Biskotten und zum Schluss erneut ⅓ Creme auftragen. Das Ganze 2 bis 3 Stunden kühl stellen und vor dem Servieren mit Kakao bestreuen.
