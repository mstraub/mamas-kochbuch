# Grießschmarrn

> [!CITE] von Paschinger Oma

* 2 EL Butter
* 1 Tasse Grieß (normale Kaffeetasse reicht für 2 Personen)
* 1 Tasse Milch

1. Butter in Pfanne zergehen lassen
2. Grieß gut mit Butter vermengen, auch eine Prise Salz kann nicht schaden
3. Milch dazugießen, kurz umrühren und dann Deckel drauf und ausdünsten lassen
4. Zwischendurch Schmarrn auch wenden - ist am besten wenn er etwas knusprig braun angebraten ist.
5. In kleine Stücke zupfen / reißen
6. Servieren mit Apfelmus, Sauerkirschen, oder anderen Kompotten

>[!TIP] Tipps
>- Noch besser wird er, wenn man die kleinen Stücke mit noch mehr Butter weiterbrät
>- Zum Grieß noch z.B. Zitronenzeste, Kardamom, Zimt, 1-2 EL Zucker geben