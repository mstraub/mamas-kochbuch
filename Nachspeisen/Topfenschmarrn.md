# Topfenschmarrn

für ca. 3-4 Personen

- 50 dag Topfen
- 18 dag Mehl
- 1/4 L Milch
- 6 Eier
- 10 dag Zucker
- 6 dag Rosinen
- Vanillezucker

Zubereitung:

1. großes Reindl fetten / stauben
2. Eier trennen und Schnee schlagen
3. alle anderen Zutaten zu Teig verrühren, dann Schnee unterheben
4. in Reindl füllen, 45 min bei 180°C im Rohr backen
5. Zerrissen wie Kaiserschmarren mit Kompott servieren

