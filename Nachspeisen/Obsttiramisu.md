# Obsttiramisu

> [!CITE] Grundrezept von Katharina Veverka, Menge für Markus' grünes Reindl

- 500 g Schlagobers
- 500 g Sauerrahm
- 1,5 Pkg Biskotten (60)
- 500 - 800 g Obst (Himbeeren, Brombeeren, Erdbeeren,..)

1. Schlagobers und Rahm verrühren (Schlagobers nicht schlagen)
2. Eine Lage Biskotten, dann Schlagobers-Rahm mit Früchten aufschichten
  - Biskotten dabei etwas tränken (z.B. in Obstsaft)
3. Wiederholen bis das Reindl voll ist
4. Einige Stunden kühlstellen (zum Durchziehen)

> [!TIP] bei gefrorenem Obst
> Früchte halb auftauen lassen und Biskotten *nicht* tränken

**Varianten / Anmerkungen**
- Statt Schlagobers + Sauerrahm habe ich auch schon mal Mascarpone + Jogurt verwendet, aber das Original schmeckt besser
- Ja, es ist wirklich kein Zucker drin, durch die Biskotten wird es süß genug
