# Palatschinken

für ca. 6 Palatschinken

* 1/4l Milch
* 1 Ei
* 12 dag glattes Mehl
* Prise Salz

**Zubereitung**
1. Mehl nach und nach mit Milch vermengen (so dass zuerst ein dicker Teig entsteht der dann immer mehr verdünnt wird um Powerl vorzubeugen)
2. Dann Ei und Salz untermischen
3. Konsistenz mit Milch abstimmen: Teig soll Löffelrücken überziehen können
4. In Pfanne(n) mit Öl oder Butter ausbacken

