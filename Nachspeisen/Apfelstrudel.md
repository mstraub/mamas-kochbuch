# Apfelstrudel

> [!CITE] adaptiert von Markus, Menge für zwei Strudel

**Teig**

siehe [Strudelteig (Variante ohne Ei)](../Und%20sonst/Strudelteig.md), ergibt ca. 500 g Teig

**Füllung**

- 1,2 kg Äpfel (ohne Abfall gerechnet!)
- 100 g Rosinen (= eine Hand voll pro Strudel)
- Saft einer Zitrone
- 80 g Semmelbrösel
- 40 g Butter
- Zimt
- optional Zucker (ich lasse ihn gern komplett weg)

**Zubereitung**

1. Teig vorbereiten
2. Äpfel fein blättrig schneiden (nicht schälen!), mit Zitronensaft vermischen
3. Butter in Pfanne schmelzen, Brösel goldbraun rösten
4. Teig auf bemehlter Arbeitsfläche (oder bemehltem Tuch) auswalken (dabei immer wieder wenden und bemehlen)
5. Teig händisch weiter möglichst rechteckig ausziehen
6. Außer bei einem Rand mit ca. 10 cm Breite Brösel, Äpfel, Rosinen verteilen und mit Zimt würzen
7. Seitenränder umschlagen, Strudel zum freien Rand hin einrollen, vorsichtig aufs Blech (mit Backpapier) rollen, so dass die Teignaht unten liegt
8. Bei 200°C für ca. 30 Minuten backen, dabei 2-3 Mal mit Butter bestreichen

> TODO: Brösel am Rand waren teilweise trocken.. in machen Rezepten wird empfohlen alles gleich zu mischen, nächstes mal testen!

## Variante

> [!CITE] ?? .. ist schon länger im Kochbuch eingetragen :)

**Teig**: siehe [Strudelteig](../Und%20sonst/Strudelteig.md)

**Füllung** (ca. 3 Strudel)

* 20 dag Semmelbrösel
* 10 dag Butter
* 28 dag Zucker
* 2 dag Zimt
* 34 dag Rosinen
* 2 dag Zitronensaft
* 220 dag Äpfel, saure, z.B. Boskoop oder Granny Smith, entkernt und in Scheiben geschnitten
* 2 Schuss Rum
* Butter, flüssig, zum Bestreichen
* Puderzucker, zum Bestäuben

Butter in der Pfanne erhitzen, die Semmelbrösel beimengen und goldbraun rösten. Zimt und Zucker vermischen.

Butterbrösel, Zimtzucker, Rosinen, Zitronensaft und Äpfel mit einem Schuss Rum gut vermengen.

Teig mit dem Handrücken hauchdünn ausziehen und mit flüssiger Butter bestreichen.

Den ausgezogenen Teig mit der Fülle bestreuen und mithilfe des Tuches vorsichtig einrollen.

Den Strudel auf ein mit Butter bestrichenes Backblech rollen und bei ca. 190°C goldbraun backen. Nach dem Backen den Strudel sofort mit heißer Butter bestreichen.

Vor dem ersten Anschneiden den Strudel ca. 30 min. ruhen lassen und mit Puderzucker bestäuben.
