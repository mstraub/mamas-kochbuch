# Topfenknödel mit Früchten

> [!CITE] von Ulli (Fellinger)

- Teig eignet sich gut für Mohnnudeln oder gefüllte Topfenknödel
- Der Teig ist eher fest aber trotzdem klebrig: mit mehliger Arbeitsfläche und Händen verarbeiten

**Zutaten**

* 4 dag Margarine
* 1 Ei
* 25 dag Topfen
* 10 dag Mehl
* 2½ dag Brösel
* 1½ EL Zucker
* 1 Prise Salz
* 1/4 Pkg Backpulver
* Butter (geschmolzen, zum Servieren)

> [!NOTE] **für Birnenknödel** etwas mehr Mehl verwenden

**Zubereitung**

1. Margarine und Ei schaumig rühren
2. Rest der Zutaten dazurühren (es soll ein eher fester Teig sein), rasten lassen
3. Mit Mehl auf Händen und Arbeitsfläche Knödeln formen (optional mit Früchten)
4. Knödel im gesalzenen Wasser leicht wallend kochen
5. Mit geschmolzener Butter und Topfen-Sauce servieren


**Topfen-Sauce**

Topfen und Sauerrahm 1:1 mischen.


## Variante

* 7 dag Butter
* 25 dag Topfen
* 1 Ei
* 15 dag Mehl
* 1 Prise Salz

Butter schaumig rühren, Topfen und Ei beigeben, zum Schluss Mehl und Salz zugeben. Teig rasten lassen. Rolle formen, Scheiben herunterschneiden und die Frucht einwickeln.
