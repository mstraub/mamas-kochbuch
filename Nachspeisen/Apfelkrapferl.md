# Apfelkrapferl

> [!CITE] Paschinger Oma

* Äpfel
* Mehl

Äpfel grob reiben, mit etwas Mehl vermischen so dass das Resultat eine teigige Konsistenz hat. In der Pfanne zu flachen Laberln formen, in etwas Öl knusprig rausprasseln (1x wenden). Mit Zimt und Zucker genießen.

