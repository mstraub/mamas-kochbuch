# Walnuss-Marzipan-Bratapfel

**Bratäpfel**

* 4 Äpfel
* 80 g grob gehackte Walnüsse
* 4 stück Löffelbiskuits
* 80 g Marzipan
* 80 g Apfelsaft
* Zimt
* Preiselbeermarmelade

**Sauce**

* 40 g Vanillepulver
* 80 g Apfelsaft
* 20 g Dotter
* 100 g Schlagobers
* 300 g Milch
* 40 g Zucker
* Orangenschale

Äpfel abspülen und aushöhlen. Walnüsse, Löffelbiskuit, Marzipan, Apfelsaft und Zimt abkneten. Äpfel damit füllen und obenauf einen Löffel Preiselbeermarmelade geben. Eine Auflaufform mit Butter ausstreichen und die Äpfel hineinsetzen. Im vorgeheizten Backofen bei 200° 15 bis 20 Minuten backen

Puddingpulver mit Apfelsaft, Dtter und Schlagobers kalt anrühren. Milch mit Kristallzucker sowie Orangenschale aufkochen und das Puddingpulvergemisch einrühren. Unter ständigem Rühren nochmals kurz aufkochen und dann etwas abkühlen lassen.

Bratäpfel mit Staubzucker bestäuben und mit Minzblätter dekorieren. Mit Beeren und der Sauce anrichten.
