# Weintraubenstrudel

> [!CITE] adaptiert von [Altwiener Weintraubenstrudel](https://www.gutekueche.at/alt-wiener-weintraubenstrudel-rezept-2659), Menge für zwei Strudel

**Teig**

siehe [Strudelteig (Variante ohne Ei)](../Und%20sonst/Strudelteig.md), ergibt ca. 500 g Teig

**Füllung**

- 1,2 kg Weintrauben
- 4 Eier
- 60 g Zucker
- 120 g Mehl

**Zubereitung**

1. Teig vorbereiten
2. Weintrauben waschen und abrebeln
3. Eier trennen und Schnee schlagen
4. Dotter mit Zucker schaumig rühren, Mehl dazumixen
5. Eischaum unterheben
6. Teig auf bemehlter Arbeitsfläche (oder bemehltem Tuch) auswalken (dabei immer wieder wenden und bemehlen)
7. Teig händisch weiter möglichst rechteckig ausziehen
8. Ei-Zucker-Mehl-Schaum auf Strudelteig (bis auf ca. 10 cm Rand) streichen, Weintrauben draufstreuen
9. Seitenränder umschlagen, Strudel zum freien Rand hin einrollen, vorsichtig aufs Blech (mit Backpapier) rollen, so dass die Teignaht unten liegt
10. Bei 200°C für ca. 30 Minuten backen, dabei 2-3 Mal mit Butter bestreichen

> TODO: mit unseren Ripatella Trauben wars ganz gut, aber: nächstes Mal ein bissl weniger Trauben und mehr Zucker (zumindest wenn die Trauben nicht vollreif sind)