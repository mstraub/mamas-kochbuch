# Apfelscheiterhaufen

> [!cite] [Quelle](https://www.steirische-spezialitaeten.at/rezepte/scheiterhaufen.html)

> [!tip] Brot bevor es ganz hart wird schon in Scheiben schneiden

**Zutaten**
- 400 g Weißbrotreste (Semmeln, Striezel,...)
- 400 g Äpfel
- 500-600 ml Milch (je nachdem wie trocken das Brot ist)
- 3 Eier
- 2 EL Zucker
- 2 EL Butter
- 1 Prise Salz
- Zimt
- handvoll Rosinen

**Zubereitung**
1. Die Weißbrotreste in gut 1cm dicke Scheiben schneiden und gleichmäßig auf ein Backblech legen.
2. Milch mit Eier, Zucker und einer Prise Salz verquirlen. Über die Brotreste gießen.
3. Die Äpfel schälen, das Kerngehäuse entfernen und in dünne Scheiben schneiden. Mit etwas Zimt und den Rosinen vermischen.
4. Die Brotscheiben einmal umdrehen, so dass sie von allen Seiten gut mit der Eiermilch getränkt werden.
5. Das Backrohr auf 180° Ober/Unterhitze vorheizen.
6. Eine Auflaufform mit Butter einfetten und mit der Hälfte der getränkten Brotscheiben auslegen. Darauf die Äpfel verteilen und mit der zweiten Hälfte an Brotscheiben bedecken.
7. Restliche Eiermilch über den Auflauf gießen und mit Butterflöckchen abschließen. Den Scheiterhaufen rund 40 Minuten im vorgeheizten Ofen backen.
8. Beim Servieren mit Staubzucker bestreuen.

## Experimente mit Schwarzbrot

Experiment ist geglückt mit obigem Rezept und ausschließlich Schwarzbrot mit
- Brot in max 1cm² große Stücke brechen
- Brot lange (mehr als 30 Minuten) in der Eimilch einweichen
- Etwas mehr Zucker und mehr Äpfel als oben angegeben