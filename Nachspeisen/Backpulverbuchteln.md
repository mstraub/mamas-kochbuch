# Backpulverbuchteln

> [!CITE] von Mama

* 30 dag Mehl
* 15 dag Zucker
* 10 dag Fett (& mehr für die Pfanne)
* 2 Eier (versprudelt)
* 1/8 L Milch
* 1 Pkg Vanillezucker
* 1/2 Pkg Backpulver
* 1 Prise Salz

Teig verrühren, Fett in Reindl zergehen lassen, Buchteln (mit Löffel) formen, in Reindl schlichten und backen. Mit Kompott oder Vanillesauce essen.

> [!WARNING] Markus: wird ein sehr patziger Teig!
> Patzen mit Löffeln im geschmolzenen Fett platzieren funktioniert aber gut,
> mit etwas Spielerei hab ich auch Powidl einbauen können.
