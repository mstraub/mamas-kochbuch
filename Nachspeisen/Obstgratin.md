# Obstgratin

* 10 dag Butter
* 10 dag Staubzucker
* 1 TL Vanillezucker
* Amaretto
* 4 Eier
* 25 dag Topfen
* 1 TL Mehl
* Obst (Marillen, Zwetschken, Kirschen, ...)

Butter mit Staubzucker schaumig rühren, Vanillezucker, Amaretto und Eidotter gut unterrühren. Topfen mit der Masse gut verrühren, Mehl dazugeben und den Schnee unterheben. In Form füllen und mit Obst belegen und bei 180° für ca. 12 Minuten backen.

