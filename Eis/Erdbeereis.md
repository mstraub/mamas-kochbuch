# Erdbeereis
- 450 g Erdbeeren
- 130 g Zucker
- 1 ½ EL Zitronensaft
- 1 Becher Schlagobers
- 75 ml Milch

Erdbeeren mit Zucker und Zitronensaft zu einer cremigen Masse mixen.
Anschließend vorsichtig Milch und Schlagobers untermischen. In die
Eismaschine leeren und nach ca. einer halben Stunde das fertige Eis
naschen.
