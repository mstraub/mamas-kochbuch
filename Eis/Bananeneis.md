# Bananeneis
- 450 g Bananen
- 100 g Zucker
- 1 TL Vanillezucker
- Saft einer ½ Zitrone
- 1 Becher Schlagobers
- 75 ml Milch

Bananen mit Zucker und Zitronensaft zu einer cremigen Masse mixen.
Anschließend vorsichtig Milch und Schlagobers untermischen. In die
Eismaschine leeren und nach ca. einer halben Stunde das fertige Eis
naschen.

