# Topfentorte, gebacken

> [!CITE] von Mama

**Mürbteig**

* 18 dag Mehl
* 8 dag Butter
* 7 dag Zucker
* 2 Dotter
* 2 TL Backpulver
* 2 EL Milch

Mürbteig kneten und in Springform geben.

Bei weniger als 180°C Umluft maximal goldgelb backen.

**Topfenmasse**

* 75 dag trockener Topfen
* 20 dag Zucker
* ½ L Milch
* 2 Päckchen Vanillepudding (ca 2\*37g)
* 5 dag Rosinen
* 3 Eiklar (Schnee)
* 1 Dotter (zum Bestreichen)

Aus Milch und Puddingpulver Pudding kochen, dann sofort Topfen einrühren und nochmals aufkochen lassen. Zucker, Rosinen und steifen Schnee unterheben.

> Achtung: alles vorbereiten, Pudding wird sehr fest und brennt leicht an!

Sobald Tortenboden goldgelb, Topfenmasse draufleeren / -streichen. Eidotter mit 1TL Wasser mischen und über rohe Topfenmasse streichen. Dann mit \~100°C ca 1:15h weiterbacken.

