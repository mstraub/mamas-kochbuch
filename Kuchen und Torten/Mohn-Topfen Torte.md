# Mohn-Topfen Torte

**Teig**

* 150g Butter
* 45g Staubzucker
* 1 ½ Pkg Vanillezucker
* 2 TL Zimt
* 1 Prise Salz
* 6 Eier
* 135g Zucker
* 225g Mohn
* 125g geriebene Haselnüsse

**Creme**

* 500g Topfen
* 150g Staubzucker
* Saft von einer ½ Zitrone
* 1 ½ Pkg Vanillezucker
* Zitronenschale
* 1 Prise Salz
* ½ L Schlagobers
* 8 Blatt Gelatine

**Belag**

* 300g Himbeeren
* 50g Staubzucker
* 3 Blatt Gelatine

**Optional zum Verzieren**

* 16 Himbeeren

Butter, Zucker, Vanillezucker, Zimt und Salz gut schaumig rühren. Unter fortwährendem Rühren die Dotter nach und nach dazugeben. Eiklar und Zucker zu steifem Schnee schlagen. ⅓ des Eischnees unter die Buttermasse rühren. Den restlichen Eischnee dazugeben. Mohn und Haselnüsse vermischen und vorsichtig unterheben. Die Masse in eine am Boden gut befettete Springform (28 cm Durchmesser) füllen und in die untere Hälfte des vorgeheizten Backrohres schieben. Bei 160° Ober- Unterhitze ca. 40 Minuten backen. Den erkalteten Tortenboden aus der Form schneiden, mit dem gereinigten Springformrand wieder umstellen und schließen.

Zutaten für die Topfenmasse miteinander verrühren und die Gelatine nach Vorschrift auf der Packung zubereiten und unter die Masse geben. Die Topfenmasse anschließend gleichmäßig auf dem Tortenboden verteilen und glattstreichen. 2 bis 3 Stunden kaltstellen.

Für den Belag die Himbeeren mit Zucker pürieren und leicht erwärmen. Die Gelatine nach Vorschrift auf der Packung zubereiten. Zum Himbeerpüree geben und gut verrühren. Das Ganze auf die Creme geben und glattstreichen. Sobald der Belag zu gelieren beginnt mit den Himbeeren verzieren. Den Springformrand öffnen und vorsichtig entfernen.

