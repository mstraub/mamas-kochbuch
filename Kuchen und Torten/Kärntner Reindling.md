# Kärntner Reindling

> [!CITE] Das große Sacher Kochbuch, Seite 529

**Germteig** (Menge für einen kleinen Guglhupf)

* 250 g Mehl
* 60 g Zucker
* 20 g Germ
* 2-4 Dotter
* 1/8 L Milch (ca)
* 80 g Butter
* Salz, Vanille, Zitronenschale,..

**Füllung**

* flüssige Butter
* viel Zimt
* noch mehr Rosinen
* Nüsse
* evtl. Kakao

[Germteig](../Und%20sonst/Germteig.md) zubereiten, dünn ausrollen, dann mit flüssiger Butter und den anderen Zutaten belegen, rollen (wie einen Strudel), dann in eine Rein legen.

160 Grad Ober- und Unterhitze für ca. 60 Minuten ins Backrohr (laut Rezept, eher 30 Minuten bei mir)

> [!NOTE] Markus: 400 g Rosinen und 100 g Nüsse für Teig aus 500 g Mehl war üppig aber nicht zu viel

> [!TIP] Form mit Fett und Feinkristallzucker (statt Mehl) ausstauben ergibt eine schöne zuckrig-karamellige Oberfläche (in dem Fall gleich nach dem Backen stürzen!)

## Variante

> [!CITE] von [karenten.at](https://www.kaernten.at/aktivitaeten/sommer/kultur-kulinarik/kaerntner-reindling-rezept-ostern-in-kaernten)

**Germteig**

- 0,5 L Milch
- 20 g Kristallzucker
- 1 Pkg. Vanillezucker
- 6 Edotter
- 1 Ei
- 1/16 L Rum
- 250 g Butter
- 1 kg Mehl (+etwas mehr für die Arbeitsfläche)
- 1 TL Salz
- 2 Pkg. Trockenhefe

**Füllung**

- 200 g flüssige Butter(+ etwas mehr für die Form)
- 25 g Zimtpulver
- 400 g Kristallzucker
- Rosinen (je nach Geschmack)

1. Milch, Zucker, Vanillezucker, Eidotter, Ei und Rum in einem Topf erwärmen (nicht kochen) und mit einem Schneebesen aufschlagen, bis sich alle Zutaten gleichmäßig vermengt haben.
2. In einem separaten Topf Butter langsam erwärmen und der Milchmasse beimengen. (nicht heiß)
3. Mehl, Salz und Trockenhefe mischen. Mit der Butter-Zucker–Ei Mischung solange kneten, bis der Teig eine seidige Konsistenz hat.
4. Eine Schüssel mit Mehl bestreuen und den Teig darin, mit einem Küchentuch bedeckt an einem warmen Ort für ca. 1 Stunde gehen lassen. In der Zwischenzeit Zucker und Zimt vermischen. Die Form großzügig mit Butter bestreichen und mit etwas Zucker-Zimt Mischung gleichmäßig bestreuen.
5. Nachdem der Teig das erste Mal aufgegangen ist, nochmals gut durchkneten und ca. 0,5 cm dick, auf einer mit Mehl bestäubten Arbeitsfläche, auswalken. Den Teig mit flüssiger Butter bestreichen und mit der Zucker-Zimt-Mischung sowie den Rosinen gleichmäßig bestreuen. Fest zusammenrollen, in einen Kreis formen (Schnecke) und in die vorbereitete Form legen. Für eine weitere Stunde mit einem Geschirrtuch bedeckt an einem warmen Ort gehen lassen.
6. 3-4 Male an der Oberfläche einstechen. Backofen auf 180° Ober-Unterhitze vorheizen. Den Reindling zuerst für 10 Minuten bei leicht geöffnetem Backofen (Kochlöffel hineinstecken) und für weitere ca. 50 Minuten bei geschlossenem Rohr backen. In der Form auskühlen lassen, stürzen und selbst essen oder verschenken.
