# Joghurttorte

**Boden**

* 15 dag Zucker
* 3 Eiklar
* 15 dag geriebene Mandeln

**Fülle**

* 50 dag Früchte
* 2 Becher Joghurt
* 15 dag Zucker
* ½ Zitrone
* 1 Pkg. Vanillezucker
* ¼ L Schlagobers
* 8 Blätter Gelatine
* 3 EL Rum oder Cointreau

Zutaten für Boden zusammenrühren und bei 180° Umluft für ca. 45 Minuten backen.

Joghurt, Zucker, Zitronensaft und Vanillezucker verrühren. Schlagobers schlagen und dann unterrühren. Anschließend aufgeweichte Gelatine tropfenweise unterrühren und die Fülle in die mit Backpapier ausgelegte Form gießen (anfangs sehr flüssig!). Über Nacht im Kühlschrank kalt werden lassen.

