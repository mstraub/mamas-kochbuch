# Nussguglhupf

* 20 dag Butter
* 25 dag Staubzucker
* 18 dag ger. Nüsse
* 12 dag Mehl
* 10 dag kleingehackte Schokolade
* 5 Dotter / Eiklar
* 1 Pkg. Vanillezucker
* 1 Pkg. Backpulver
* 1 Msp. Zimt
* Zitronenschale

Die Butter schaumig rühren und nach und nach Zucker, Vanillezucker, Dotter, Zitronenschale, Zimt, Nüsse und das mit Backpulver gemischte und gesiebte Mehl hinzugeben. Das Eiklar zu steifem Schnee schlagen. Den Schnee und die Schokolade unter den Teig heben und in eine gut gefettete und gebröselte Gugelhupfform füllen. Im Backrohr auf der unteren Schiene bei ca. 170° 50 bis 60 Minuten backen. Nach dem Backen stürzen und gut auskühlen lassen!

Nach Belieben verzieren (Staubzucker, Schokoladeglasur, ...)

