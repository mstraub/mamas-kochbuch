# Lambada Schnitten

**Biskuit**

* 15 dag Zucker
* 8 dag Mehl
* 8 dag Maizena
* 1 TL Backpulver
* 3 Eier
* 3 EL Wasser
* 1 Pkg. Vanillezucker

**Belag**

* ½ L Cappy
* 1 Pkg. Vanillepudding
* ¼ L Schlagobers
* 3 Blatt Gelatine
* Biskotten
* Schokoladeglasur

Eiweiß mit kaltem Wasser steif schlagen. Zucker und Vanillezucker kurz darunterschlagen. Die verrührten Dotter zugeben, Maizena, Mehl und Backpulver sieben und unter die Eimasse heben. Die Teigmasse auf ein befettetes, bemehltes Blech geben und backen.

Cappy mit Vanillepudding kochen. Den abgekühlten Pudding über das Biskuit streichen. Schlagobers steif schlagen, 3 Blatt Gelatine einweichen und zum Schlagobers geben. Anschließend über das Biskuit streichen Biskotten in Cappy tauchen und über das Schlagobers legen. Mit Schokoglasur überziehen.

