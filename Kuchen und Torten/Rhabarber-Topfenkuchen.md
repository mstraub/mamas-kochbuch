# Rhabarber-Topfenkuchen

**Mürbteig**

* 25 dag (glattes) Mehl
* 1/2 Pkg Backpulver
* 1 Pkg Vanillezucker
* 15 dag Butter
* 7 dag Staubzucker
* 1 Dotter

**Fülle**

* 1/2 kg Topfen
* 12 dag Staubzucker
* 1 Vanillezucker
* 2 Eier
* 1/2 Zitronensaft

**Rhabarber**

* 1/2 kg Rhabarber
* 20 dag Zucker
* 1 Pkg Vanillezucker
* Wasser

> [!TIP] Markus: für das "Rhabarberkompott" reicht imho auch weniger Zucker

1. Für den Mürbteig alle Zutaten durchkneten und ½ Stunde im Kühlschrank rasten lassen.
2. Für die Fülle alles bis auf die Eiweiß verrühren, anschließend Schnee unterheben.
3. Rhabarber (zurechtgeschnitten) kurz im Zuckerwasser aufkochen, dann sofort ohne Deckel vom Herd stellen (sonst zerfällt er).
4. Teig dünn auf ein Blech aufdrücken, Topfen aufstreichen, Rhabarber auflegen
5. ca. 30 Minuten bei 170-180°C Umluft backen (aufpassen, dabeibleiben).

