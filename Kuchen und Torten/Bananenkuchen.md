# Bananenkuchen

> [!cite] von Mama, siehe [Rührteig](../Und%20sonst/Rührteig.md)

* 45 dag Mehl
* 25 dag Butter
* 25 dag Zucker
* 10 dag Nüsse
* 5 dag Rosinen
* 2 Bananen
* 1/8l Milch
* 4 Eier
* 1 Pkg Vanillezucker
* 1 Pkg Backpulver
* Rum

1. Rosinen in Rum ca. 10 Minuten köcheln lassen bis sie prall & saftig sind
2. Butter schaumig rühren, Zucker und Ei abwechselnd hinzugeben
3. Banane mit Gabel zerquetschen und mit Rosinen einrühren
4. Mehl mit Nüssen, Backpulver und Vanillezucker vermischen und einrühren, bei Bedarf Milch dazu
5. Bei 200°C Ober- und Unterhitze backen (20-30 Minuten)

**Glasur**

- 20 dag Staubzucker mit dem Saft von zwei Zitronen vermischen
- Glasur nach dem Backen auftragen
