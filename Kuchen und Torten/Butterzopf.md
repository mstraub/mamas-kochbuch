# Butterzopf

* ca. 80 dag Dinkelmehl
* 10-20 dag griffiges Mehl
* 2 Eier
* ½ Butter (=vermutlich 125g?)
* 1 Würfel Germ (42 g)
* 0,5 L Milch (ev. mehr)

Butter zergehen lassen und mit Milch aufwärmen. Mehl, Zucker und Germ dazugeben. 1x gehen lassen, Butterzopf formen und noch einmal gehen lassen. Mit Milch bestreichen und mit Hagelzucker verzieren. Bei 175° bis 180° ca. 30 Min backen. Ergibt 2 Striezel.

> Frage Markus: 1/2 Butter = 125g?

