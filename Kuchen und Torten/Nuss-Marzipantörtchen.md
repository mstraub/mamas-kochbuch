# Nuss-Marzipantörtchen

**Zutaten für etwa 25 Stück**

**Teig**

* 25 dag Weizenmehl
* 4 dag Margarine
* ⅛ L Milch
* ½ Pkg. Germ
* 5 dag Zucker
* etwas Salz
* 3 EL Wasser

**Füllung**

* 5 dag Marzipanrohmasse
* 5 dag Zucker
* 10 dag gemahlene Nüsse
* 1 Eiweiß
* 3 EL Wasser
* 3 Tropfen Bittermandelöl

Einen [Germteig](../Und%20sonst/Germteig.md) (warme Milch mit etwas Mehl, Zucker und Hefe gehen lassen - dann mit übrigen Zutaten mischen) zubereiten und an einem warmen Ort stehen lassen bis er doppelt so hoch ist. Für die Fülle die Zutaten mit einem Mixer zu einer geschmeidigen Masse verrühren.

Den Teig zu einem Rechteck (ca 50x35) ausrollen. die fülle auf die Teigplatte streichen und von der Längsseite her aufrollen. Die Rolle mit einem scharfen Messer in 25 Stücke schnei den - diese in die Förmchen legen und nochmals gehen lassen (bis sie doppelt so hoch sind. 15 - 20 min backen (je nach Ofen ca 180° bei Heißluft)

