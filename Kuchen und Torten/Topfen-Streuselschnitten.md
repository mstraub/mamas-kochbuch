# Topfen-Streuselschnitten

> [!CITE] von Mama

**Streusel**

* 35 dag Mehl
* 15 dag Zucker
* 15 dag Butter
* 3 dag Kokosflocken
* 2 Dotter
* ½ Pkg. Backpulver
* 1 Pkg. Vanillezucker
* Saft einer Zitrone

**Fülle**

* 1 kg Topfen
* 25 dag Zucker
* 15 dag Butter
* 5 EL Grieß
* 5 Eier
* Saft von 2 Zitronen
* 2 Pkg. Vanillezucker
* Rum

1. Zutaten für Streusel miteinander abbröseln, ⅔ der Menge auf das Blech drücken.
2. Für die Fülle Eier trennen, Eiklar zu Schnee schlagen.
3. Die übrigen Zutaten gut verrühren, Schnee unterheben und auf den Teig verteilen.
4. Die restlichen Brösel obenauf streuen und bei 160° Umluft für ca. 50 Minuten in den **kalten** Backofen schieben.

> Ähnlich: https://www.ichkoche.at/topfen-streusel-torte-rezept-189198/1

> Fruchtige Variante Markus: bei Fülle statt 5 Eier nur 3 Eier mit den 2 Eiklar vom Teig, statt 250g Topfen 250g frische Marillen, und nochmal 5 EL Grieß (oder evtl. noch mehr!)

