# Zucchinikuchen

> [!CITE] von Mama

**Zutaten**

* ¼ L Öl
* 30 dag Staubzucker
* 3 Eier
* 40 dag Zucchini
* 35 dag Mehl
* 10 dag geriebene Haselnüsse
- 1-3 TL Zimt
- 1 Pkg Vanillezucker
- ~~1 TL Natron~~ (hab ich weggelassen)
- 1 Pkg Backpulver

**Zubereitung**

1. Zucchini fein reiben
2. Alle Zutaten verrühren (kein Schnee!)
	1. Zuerst Öl und Zucker
	2. Eier dazu  
	3. Zucchini und Nüsse dazu  
	4. Trockene Zutaten (Mehl,..) dazu
3. Blech fetten und mit Mehl stauben
4. Bei 180° ca. 20-30 Minuten backen
5. Schokolade- oder Rumglasur


## Variante

> [!CITE] haben wir mal die Originalmengen von Mama adaptiert?

**Teig**

* 25 dag Butter (Originalrezept: ¼ L Öl)
* 40 dag Rohrzucker
* 5 Eier
* 50 dag Mehl
* 3 TL Backpulver
* 1 Msp. Zimt
* 20 dag geriebene Haselnüsse
* 50 dag Zucchini

**Schokoladeglasur**

* ⅛ L Schlagobers
* 10 dag Butter
* 25 dag Schokolade

1. Butter mit Zucker schaumig rühren
2. Eier einrühren
3. Mehl, Backpulver, Zimt, Haselnüsse und Zucchini (geschält und fein gerieben) unterrühren
4. Bei 180° ca. 30 Minuten backen
5. Mit Marmelade bestreichen und die Schokoladenglasur darüber leeren
