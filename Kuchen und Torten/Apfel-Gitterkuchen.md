# Apfel-Gitterkuchen

> [!CITE] von Mama

**Teig**

* 60 dag Mehl
* 30 dag Butter
* 26 dag Zucker
* 1 Löffel Rahm oder Milch (optional, macht Teig weicher)
* 3 Dotter
* 1 Ei
* 1/2 Pkg Backpulver
* Zitronenschale (optional)

**Fülle**

* 1-2 kg Äpfel
* Zitronensaft
* Rum
* Rosinen
* Zimt

**Zubereitung**

1. Rosinen entweder über Nacht in Rum einlegen oder im Topf sanft köcheln bis sie schön prall sind.
2. Mehl, Butter, Zucker und Backpulver (optional Zitronenschale) zusammen in einer Schüssel durchkneten. Butter vorher in kleine Stücke schneiden hilft.
3. Zuletzt Dotter, Ei und Rahm bzw. Milch hinzugeben.
4. Teig auswalken, 2/3 als Boden, 1/3 als Gitter verwenden.
5. Boden auf gefettetes bemehltes Blech legen.
6. Äpfel entkernen (nicht schälen), reiben, mit Zitronensaft, Rum, Rosinen, Zimt abschmecken und am Boden verteilen
7. Gitterstreifen schneiden und über die Äpfel legen.
8. Hochmotivierte bestreichen das Gitter noch mit Eigelb für eine dunkelgelbe Farbe.
9. Backen bei Umluft und 180°C, ca 30 Minuten.

> [!TIP] Markus: 1,5 kg reichen für eine saftig dicke Apfelschicht
> Bis 2 kg geht auf großen Blechen.
> Das Originalrezept hatte glaub ich < 1 kg Äpfel.
