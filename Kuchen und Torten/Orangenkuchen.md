# Orangenkuchen

> [!CITE] Markus' Arbeitskollege Gerald, er hat's aus einem Griechenlandurlaub von seiner Gastgeberin - also original griechisch (?).

* 2 Bio Orangen
* 1 Bio Zitrone
* ½ Glas Olivenöl
* ½ Glas Orangensaft
* 1 ½ Gläser Zucker
* 2 Gläser Mehl
* 4 Eier
* 1 Pkg. Backpulver
* ½ Glas Jogurt / Milch

Orangen- und Zitronenschale abraspeln. Alle Zutaten miteinander verrühren. Alles in eine Kuchenform füllen und im Backofen bei 180° ca. 35-40 Minuten backen.


> Tipp: Teig könnte zu flüssig werden, falls das passiert einfach noch Mehl dazu. Auch Mandeln machen sich noch gut!

> Frage Michi: Ich nehme mal an, dass nur die Schale gebraucht wird, da ja auch 1/2 Glas Orangensaft erwähnt wird, aber kein Zitronensaft?

> Antwort Markus: Hm das ist eine gute Frage - ich hab ihn einmal probiert und dann war er nicht so gut wie von Gerald. Aber vielleicht liegt's ja genau daran. Hast du es schon ausprobiert? .. denn 2 Orange ergeben ja eh ca das halbe Glas Saft vom Gefühl her

> Beim nächsten Backversuch vielleicht lieber Chrisis Tipp backen: https://www.christinascucina.com/sicilian-orange-cake-using-entire-orange-peel-juice-pulp/

