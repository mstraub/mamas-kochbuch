# Osterlamm mit Eierlikör

**Teig** (für 2x Hasen (je 0,5 l) und 2x Lämmer (je 0,7 l).

* 30 dag Mehl
* 30 dag Butter
* 30 dag Zucker
* 3 TL Backpulver
* 300 ml Eierlikör
* 4 Eier
* 1 Dotter

**Glasur (optional)**

* 15 dag Staubzucker
* 2-3 EL Zitronensaft oder heißes Wasser
* Kokosflocken

1. Butter schaumig rühren
2. Zucker, Eier und Dotter beigeben
3. Mehl, Backpulver und zum Schluss den Eierlikör unterrühren
4. Formen (Osterlamm, Hase, etc.) einfetten und mit Mehl stauben
5. Im vorgeheizten Backrohr bei 175° ca. 45 Minuten backen
6. Ca. 5 Minuten abkühlen lassen, aus der Form nehmen, Boden abschneiden und aufstellen
7. Entweder nur mit Puderzucker bestauben oder mit der Glasur bestreichen und mit Kokosflocken bestreuen. Kann ebenfalls nach Belieben verziert werden (z.B. Nelken für Augen, Band um den Hals). Hase am Besten mit Schokoglasur verzieren.

