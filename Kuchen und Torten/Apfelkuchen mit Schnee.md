# Apfelkuchen mit Schnee
> [!CITE] von Weigl-Oma

**Zutaten Teig**
- 20 dag Mehl
- 20 dag Fett
- 14 dag Nüsse
- 17 dag Zucker
- 3 Dotter
- Backpulver

**Weitere Zutaten**
- 60 dag Äpfel (grob gerieben)
- 3 Eiklar
- 10 dag Zucker

**Zubereitung**
1. Alle Zutaten des Teigs zu einem Knetteig verarbeiten
2. Teig in eine Lasagnepfanne geben
3. Darüber die geriebenen Äpfel verteilen
4. Bei ca 180° für 30-35 Minuten backen
5. Schnee schlagen, über dem Kuchen verteilen
6. Ca. 10-15 Minuten fertigbacken

![|500](Apfelkuchen%20mit%20Schnee.jpg)
