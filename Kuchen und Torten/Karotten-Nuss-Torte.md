# Karotten-Nuss-Torte

* 25 dag Haselnüsse
* 25 dag Karotten
* 4 Eier
* 20 dag Zucker
* 10 dag Mehl
* ½ Pkg. Backpulver
* Saft einer halben Zitrone
* etwas Rum
* 1 Prise Nelkenpulver
* Schokoladeglasur

Haselnüsse und Karotten fein reiben und vermischen, Eiklar zu Schnee schlagen, ⅓ der Zuckermenge löffelweise einschlagen. Dotter mit restlichem Zucker dickschaumig rühren, Zitronensaft, Rum, Nelkenpulver, Haselnussgemisch und das Mehl-Backpulver-Gemisch mit der Schnellstufe des Mixers sehr kurz unterziehen. Bei 160° ca. 1 Stunde in das kalte Rohr stellen.

