# Pork Fruitcake

> [!CITE] Five Roses Cookbook (Kanada, 1915) - siehe [Youtube](https://www.youtube.com/watch?v=l29uvfVAxDM)

> [!NOTE] ein (alter) kanadischer Cup = 227 ml

**Zutaten**

| Originalrezept | Übersetzung |
|----------------|-------------|
| 1 pound solid fat pork, ground fine (we used pork belly) | 450 g fein faschiertes fettes Schweinefleisch (z.B. Bauchfleisch) |
| 1 Imperial pint boiling water | 570 ml kochendes Wasser |
| 2 tsp baking soda | 2 TL Natron (Natriumhydrogencarbonat) |
| 2 cups sugar | 2 Cups Zucker |
| 1 cup molasses | 1 Cup Melasse (Marke z.B. Black Strap) |
| 1 pound currants | 450 g Korinthen |
| 1 pound raisins | 450 g Rosinen |
| ½ pound candied peel | 225 g kandierte Schalen (Aranzini, Zitronat,..) |
| 1 cup chopped nuts | 1 Cup grob gehackte Nüsse |
| ½ glass brandy (rum, or whisky) | 3/4 Cup Brandy (ungefähr) |
| 1 tsp cloves | 1 TL Nelken |
| 1 tsp ginger | 1 TL Ingwer |
| 2 tsp cinnamon | 2 TL Zimt |
| 1 grated nutmeg | 1 geriebene Muskatnuss |
| 4 cups flour | 4 Cups Mehl |

**Zubereitung**

1. Faschiertes Fleisch mit kochendem Wasser übergießen
2. Mit den anderen Zutaten vermischen (Mehl als letztes dazugeben)
3. Reindl mit Backpapier auslegen und (mindestens) 2 Stunden bei 180°C backen

> [!TIP] Erfahrungen Markus
>
> - Melasse und Korinthen gibt's im Reformhaus
> - 1,5h Backzeit bei 180°C Ober-Unterhitze hat gereicht

![](./Pork%20Fruitcake.jpg)