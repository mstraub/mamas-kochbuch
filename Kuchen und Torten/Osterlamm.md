# Osterlamm

* 18,5 dag Mehl
* 10 dag Butter
* 8 dag Zucker
* 3 - 5 EL Milch
* 1 TL Backpulver
* 1 Pkg. Vanillezucker
* 1 Prise Salz
* 1 Msp Zimt
* 1 Ei
* 1 Dotter

Butter, Zimt, Zucker und Salz schaumig rühren. Danach das Ei sowie den Dotter, zum Schluss Mehl und Backpulver sieben und unterrühren. Anschließend Milch hinzufügen bis der Teig schwer reißend vom Löffel fällt. Die Form (Osterlamm, Hase, etc.) einfetten und mit Mehl stauben. Im Backrohr bei 180° ca. 25-30 Minuten backen. Das Lamm anschließend 15 Minuten ruhen lassen, den Boden abschneiden, aufstellen und mit Puderzucker und/oder anderen Dingen verzieren (z.B. Nelken für Augen).

> Der Teig ist eher trocken.

