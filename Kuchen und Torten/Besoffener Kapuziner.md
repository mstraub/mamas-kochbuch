# Besoffener Kapuziner

* 20 dag Staubzucker
* 14 dag Mehl
* 3 Eier
* 1 Vanillezucker
* 10 dag geriebene Haselnüsse
* ½ Pkg. Backpulver
* 7 dag Öl
* ⅛ L starker Kaffee

Steifen Schnee schlagen, dann die übrigen Zutaten vorsichtig darunterheben, in befettete und mit Bröseln ausgestaube Form füllen und bei mittlerer Hitze (ca. 160°) backen und auskühlen lassen. Mit heißem Glühwein übergießen und mit Schlagobers verzieren.

