# Eierlikörguglhupf

Für eine gängige Gugelhupfform benötigt man:

- 5 Eier (zimmerwarn)
- 250 g Butter (zimmerwarm)
- 250 g Staubzucker
- 1 Pkg Vanillezucker
- 250 g griffiges Weizenmehl
- 1 Pkg Backpulver
- 200 g geriebene Nüsse
- 250 ml Eierlikör

Butter und Mehl für die Form, Staubzucker zum Bestäuben.

1. Die Eier trennen.
2. Die weiche Butter gemeinsam mit dem Staub- und dem Vanillezucker schaumig schlagen, nach und nach die Dotter dazurühren.
3. Die Gugelhupfform mit Butter befetten und mit Mehl bestäuben.
4. Das Eiklar mit einer Prise Salz zu einem festen Schnee schlagen.
5. Die Nüsse fein reiben.
6. In die Buttermasse nun abwechselnd das mit Backpulver versiebte Mehl, die Nüsse und den Eierlikör einrühren.
7. Zum Schluss den Eischnee unterheben.
8. Die Masse in die eingefettete und bemehlte Gugelhupfform füllen.
9. Bei 175 Grad mit Ober-/Unterhitze ca. 50 Minuten backen.
10. Aus der Form stürzen und noch warm mit Staubzucker bestreuen.

