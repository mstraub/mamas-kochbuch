# Topinambur-Mohn-Schoko-Guglhupf

Sehr saftiger, [Adapation von diesem Rezept](http://www.ichkoche.at/topinambur-guglhupf-rezept-221568) (auch sehr gut)

- 5 Eier
- 250 g brauner Zucker
- 200 g Mohn (gerieben)
- 200 g Topinambur
- 200 g Karotten
- 150 g Schokolade
- 250 g Topfen
- 100 g Mehl
- 1 Pkg Backpulver

1. Eier trennen, Dotter mit Zucker und Prise Salz schaumig rühren
2. Karotten und Topinambur reiben, abwechselnd mit Mohn und Topfen unterrühren
3. Mehl mit Backpulver vermischen und unterrühren
4. Schokolade grob schneiden und dazumischen
5. Schnee schlagen und vorsichtig unterheben
6. Teig in gebutterte + bemehlte Form füllen, bei 180°C Umluft ~30-40 Minuten backen


