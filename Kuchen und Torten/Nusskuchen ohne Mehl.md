# Nusskuchen ohne Mehl

> [!CITE] adaptiert von diesem [Netzfund](https://www.bakingbarbarine.at/saftiger-nusskuchen-ohne-mehl), sehr saftig

**Zutaten** (für 24cm Springform)

* 150 g dunkle Schokolade
* 250 g Butter
* 5 Eier
* 180 g Braunzucker
* 250 g gemahlene Nüsse
* 50 g gemahlener Mohn
* 2 EL Rum
* 1 Prise Zimt

**Zubereitung**

1. Schokolade und Butter vorsichtig schmelzen lassen.
2. Backrohr auf 170 °C Heißluft vorheizen.
3. Eier mit dem Braunzucker sehr schaumig schlagen. Rum, Zimt und die zerlassene Schoko-Butter einrühren. Gemahlene Nüsse und Mohn dazugeben und kurz verrühren.
4. Die Springform mit Butter befetten und mit Mehl bestäuben und die Masse einfüllen.
5. Den Kuchen ca. 40-45 Minuten (Stäbchenprobe) auf der mittleren Schiene backen.

Etwas auskühlen lassen, mit Staubzucker bestäuben, z.B. mit Birnenkompott, Kugel Vanilleeis, Apfelkompott, Zwetschkenröster, servieren

**Varianten**

- 60 g Zucker durch 60 g Mehl ersetzen gab auch ein gutes Resultat (noch immer recht süß)