# Schoko-Kirschkuchen

**Teig**

* 12,5 dag Butter
* 15 dag Zucker
* 25 dag Mehl
* 1 TL Backpulver
* 2 EL Vanillezucker
* 4 Eier
* 13 dag Nüsse (gemahlen)
* 20 dag Bitterschokolade (in Stückchen)
* 80 dag Kirschen

**Streusel**

* 15 dag Butter
* 20 dag Zucker
* 25 dag Mehl

Butter schaumig rühren und anschließend mit (Vanille-) Zucker vermengen. Eier einzeln dazu rühren und dann Mehl und Backpulver unterrühren. Die gemahlenen Nüsse sowie die Schokolade wird zum Schluss untergehoben (nicht mehr mixen!). Für den Streusel alle Zutaten vermengen und abbröseln.

Der Teig wird anschließend in die Form gegeben, die (gewaschenen und entkernten) Kirschen oben auf den Teig gelegt und dann mit Streuseln bedeckt.

Im vorgeheizten Backrohr bei 170° Umluft für ca. 1h backen.

