# Apfelmustorte

**Biskuit**

* 3 Eier
* 3 EL Wasser
* 12 dag Zucker
* 12 dag Mehl
* ½ KL Backpulver

**Fülle**

* 1 Becher Schlagobers
* 1 Becher Joghurt
* ca. ¼ L Apfelmus
* 1-2 EL Zucker
* 4 EL Rum
* Saft von einer Zitrone
* 4 Blatt Gelatine
* 2 EL Marillenmarmelade
* 1-2 Schachteln Biskotten

Den ausgekühlten Biskuitteig mit Marillenmarmelade bestreichen. Gelatine in kaltem Wasser einweichen, ausdrücken und im warmen Apfelmus auflösen, unterkühlen lassen. Schlagobers schlagen und mit Joghurt, Zucker, Rum, Apfelmus sowie Zitronensaft vermischen (weiche Masse). Eher säuerlich würzen. Biskotten und Fülle abwechselnd auf dem Tortenboden verteilen. Mind. 3 - 4 Stunden im Kühlschrank erstarren lassen.

