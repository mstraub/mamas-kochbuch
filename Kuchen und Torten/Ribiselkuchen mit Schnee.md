# Ribiselkuchen mit Schnee

## Variante Mama: knusprig

**Teig**

* 30 dag Mehl
* 15 dag Zucker
* 5 dag Butter
* 3 EL Wasser
* 3 Dotter
* 1 Pkg Vanillezucker
* 1/2 Pkg Backpulver
* 1/16 L Milch

1. Butter langsam zerlassen
2. Dotter, Zucker und Wasser schaumig rühren
3. Flüssige Butter, Mehl (mit Backpulver und Vanillezucker) und Milch einmengen
4. Auf Blech aufstreichen
5. Teig bei 170°C goldgelb backen

> [!WARNING] Masse ist wenig und klebrig
> Am besten mit nassen Händen oder einer nassen Teigspachtel verteilen

**Belag**

* 30 dag Ribisel (ich würd mehr nehmen, 45 dag z.B.)
* 3 Eiweiß
* 20 dag Kristallzucker

6. Festen Schnee schlagen und dann löffelweise Zucker untermixen
7. Ribisel in Schnee unterheben
8. Belag draufgeben und bei 150°C weiterbacken bis der Schnee aufgeht und goldig ist
9. bei 100°C weitertrocknen bis Schnee knusprig ist

> [!WARNING] dabeibleiben, kann ca. 30 Minuten dauern bis er fertig ist

## Variante Fellinger: saftig

**Mürbteig**

- 36 dag Mehl
- 24 dag Butter
- 2 Dotter
- 10 dag Zucker
- 1 Prise Salz
- Zitronenabgeriebenes

1. Zutaten zu einem Mürbteig verkneten und 1/2 Stunde kalt stellen
2. Dünn auswalken, auf Blech geben, mit einer Gabel mehrmals einstechen
3. Bei mittlerer Hitze vorbacken

**Belag**

- 60 dag Ribisel
- 20 dag Zucker
- 6 Eiklar

4. Ribisel mit 2/3 des Zuckers mischen und auf überkühltem Kuchenboden verteilen
5. Eiklar mit restlichem Zucker zu einem sehr steifen Schnee schlagen und auf den Ribiseln verteilen
6. ca. 15 Minuten überbacken
