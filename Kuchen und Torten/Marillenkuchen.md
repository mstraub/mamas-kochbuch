# Marillenkuchen

* 45 dag Mehl
* 25 dag Zucker
* 25 dag Joghurt
* 12,5 dag Butter
* 1 Pkg Vanillezucker
* 1 Pkg Backpulver
* Zitronensaft
* ½ kg Marillen
* (1 EL Kakao)

Butter schaumig rühren, anschließend Zucker, Vanillezucker und Joghurt unterrühren. Zum Schluss Mehl und Backpulver beigeben. Auf gefettetes und bebröseltes Blech geben, mit Marillen belegen und Zitronensaft optional beträufeln. Bei 180° ca. 45 Minuten backen.

