# Guglhupf

> [!CITE] von Mama Weigl

* 50 g Kochschokolade
* 7 Eier
* 150 g Butter (zimmerwarm)
* 50 g Staubzucker
* 1 Pkg. Vanillezucker
* 1 kleine Prise Salz
* 200 g Kristallzucker
* 150 g Mehl
* 50 g Mandelstifte

**Zubereitung**

 1. Eine Springform (Durchmesser 24 cm) mit Butter ausstreichen und mit Mehl ausstreuen.
 2. Kochschokolade in kleine Stücke hacken und über Wasserdampf schmelzen.
 3. Die Eier in Dotter und Klar trennen.
 4. Weiche Butter mit Staubzucker, Vanillezucker und Salz gut cremig rühren.
 5. Dotter nach und nach untermischen und schaumig rühren.
 6. Eiklar mit Kristallzucker zu cremigem Schnee schlagen.
 7. Ein Drittel des Schnees in die Dottermasse rühren, restlichen Schnee und das Mehl abwechselnd unterheben.
 8. Ein Drittel der Masse vorsichtig mit der geschmolzenen Schokolade verrühren.
 9. Die Mandeln unter die helle Masse heben.
10. Helle und dunkle Masse abwechselnd in die Springform füllen.
11. Kuchen im vorgeheizten Rohr (180 Grad) ca. 1 Stunde backen.
12. Aus dem Rohr nehmen, stürzen, auskühlen lassen und zuckern.

