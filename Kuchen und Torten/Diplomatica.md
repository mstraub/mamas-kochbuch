# Diplomatica

* 2 Marmor oder Sandkuchen

**Zum Tränken**

* 5 EL Rum
* 5 EL Wasser
* 400 ml Espresso
* 5 TL Zucker

**Schokoladefüllung**

* 4 Eier
* 1 TL Zucker
* 17 dag Halbbitterschokolade

Kastenform mit Lebensmittelfolie auslegen. Kuchen in dünne Scheiben schneiden (ca. 1 cm). In einer Schüssel Rum, Espresso, Zucker und Wasser vermischen. Kuchenscheiben damit tränken und den Boden sowie die Seiten der Form auslegen. Eigelb mit Zucker schaumig rühren und die geschmolzene Schokolade in kleinen Mengen hinzugeben. Anschließend Schnee unterheben und die fertige Schokolademasse auf den getränkten Kuchen in der Form füllen. Zum Schluss mit getränkten Kuchenscheiben bedecken. Mindestens 1 Tag im Kühlschrank ziehen lassen! Anschließend auf eine Platte stürzen und nach Belieben verzieren (Schlagobers und Schokoladeraspeln oder gemahlene Nüsse)

