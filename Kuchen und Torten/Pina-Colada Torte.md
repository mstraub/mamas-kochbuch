# Pina-Colada Torte

**Biskuit**

* 15 dag Zucker
* 8 dag Mehl
* 8 dag Maizena
* 1 TL Backpulver
* 3 Eier
* 3 EL Wasser
* 1 Pkg. Vanillezucker

**Fülle**

* 20 dag weiße Schokolade
* 1 Dose Ananasringe
* 10 dag Schlagobers
* 20 dag Kokosette
* 2 EL Kokoslikör
* 2 Lagen Biskuitboden
* 5 EL weißer Rum
* 75 dag Topfen
* 3 EL Zitronensaft
* 2 Pkg. Gelatine
* 75 g Zucker
* 50 g Kokosraspeln
* 12 Raffaello

Eiweiß mit kaltem Wasser steif schlagen. Zucker und Vanillezucker kurz darunterschlagen. Die verrührten Dotter zugeben, Maizena, Mehl und Backpulver sieben und unter die Eimasse heben. Die Teigmasse in die befettete, bemehlte Tortenform geben und backen (Heißluft, 175 bis 180° ca. 30 min. im vorgeheizten Backofen). Sobald sich das Biskuit vom Rand löst, ist es fertig. Auskühlen lassen und in der Mitte auseinanderschneiden (2 Böden).

Schokolade in kleine Stücke schneiden. Ananasringe abtropfen lassen und den Saft auffangen. Schlagobers zum Kochen bringen. Den Topf vom Herd nehmen, Schokolade darin schmelzen lassen. Kokosette und Kokoslikör hinzugeben. Den ersten Biskuitboden in Tortenform lassen und mit Kokosmasse bestreichen, danach mit dem zweiten Biskuitboden abdecken. Zwei Ananasringe in insgesamt zwölf Stücke schneiden. Die Hälfte der restlichen Ananas klein schneiden und auf dem Biskuit verteilen. Mit Rum beträufeln. Die verbliebenen Ananasringe fein würfeln. Den abgetropften Topfen cremig rühren, Zitronensaft und fein gewürfelte Ananas untermischen. Gelatine mit ¼ L Ananassaft und Zucker unter Rühren zum Kochen bringen. Sofort unter den Topfen ziehen. Die Creme auf den oberen Boden streichen. Die Torte 3 Stunden kühl stellen, danach rundum mit den Kokosraspeln bestreuen. Mit den 12 Ananasstücken und den Raffaellos garnieren.


