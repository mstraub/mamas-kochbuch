# 08/15 Obstkuchen

## Variante Mama

* 3 Eier
* 15 dag Butter
* 15 dag Zucker
* 35 dag Mehl
* 1 Pkg Vanillezucker
* 1 Pkg Backpulver
* Milch nach Bedarf

> [!TIP] Mit Milch kann die Konsistenz des Teigs von locker/flüssig bis gerade noch streichbar variiert werden

1. Eier trennen und Schnee schlagen
2. Butter schaumig rühren, danach Zucker/Vanillezucker, Mehl/Backpulver, Dotter und Milch hinzufügen
3. Zum Schluss Schnee unterheben
4. Mit Obst nach Wahl belegen
5. Bei 180° ca. 20-25 Minuten backen


## Andere Variante

* 4 Eier
* 16 dag Butter
* 20 dag Zucker
* 40 dag Mehl
* 2 TL Vanillezucker
* ½ Pkg Backpulver
* 200 ml Milch

Zubereitung wie oben (aber 45 Minuten backen -> **TODO was stimmt?**)

## Beläge

Je nach Belieben mit Streusel oder Guss bedecken, oder Teig variieren, z.B. einen Teil des Mehls durch Nüsse ersetzen.

### Streusel

* 10 dag Zucker
* 10 dag Mehl
* 10 dag Fett

Eventuell jeweils auf 8 dag reduzieren. Die drei Zutaten miteinander verkneten.

### Guss

* 250 g Rahm
* 2-3 Eier

erst 15 Minuten vor Backende draufgeben
