# Karottenkuchen

* 16 dag Karotten
* 16 dag Nüsse
* 4 Eier
* 2 EL Brösel
* 10 dag Staubzucker

**Zubereitung:**
1. Dotter schaumig rühren
2. Geriebene Karotten und Nüsse untermengen
3. Brösel und Staubzucker dazugeben
4. Schnee unterheben
5. Bei 170° ca. 40 Minuten backen.


## Variante mit Mehl

> [!cite] von Xandi, gut "kuchig", süß (aber nicht klebrig)

- 4 Eier
- 40 dag Zucker (2 Cups)
- 170 g Öl (3/4 Cups)
- 300 g fein geriebene Karotten (3 Cups)
- 250 g Mehl (2 Cups)
- 100 g geriebene Haselnüsse
- 1 TL Zimt
- 1/2 Pkg Backpulver
- 1 Msp Natron

**Zubereitung:**
1. Eier trennen, Dotter schaumig rühren, Schnee schlagen
2. Zucker und Öl zum Dotter 
3. Trockene Zutaten vermischen (Mehl, Nüsse,...)
4. Alles zusammenmischen, Schnee unterheben
5. Bei 170°C ca. 40 Minuten backen
