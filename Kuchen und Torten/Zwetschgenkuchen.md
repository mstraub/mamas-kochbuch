# Zwetschgenkuchen

**Mürbteig**

* 150 g Mehl
* 1 Pkg Vanille-Puddingpulver
* 120 g Butter
* 60 g Zucker
* 1 Ei
* 1 Msp. Backpulver
* 1 Schuss Rum
* ½ kg Zwetschgen

**Streusel**

* 100 g Mehl
* 75 g Butter
* 50 g Zucker
* 2 Pkg. Vanillezucker
* 1 TL Zimt

Einen schönen Mürbteig aus den Zutaten kneten und in eine Springform pressen, Zwetschgen darauf verteilen und mit Streusel bedecken. Bei 180°C Umluft ca. 45 min backen. (Kann sein, dass es sehr flüssig aussieht, aber er wird dann schon recht, sonst zur Not ein paar Brösel unter den Zwetschgen verteilen)
