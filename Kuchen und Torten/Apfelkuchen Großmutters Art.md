# Apfelkuchen "Großmutters Art"

* 35 dag Mehl
* 25 dag Butter
* 25 dag Zucker
* 5 Eier
* 1 Pkg. Vanillezucker
* 1 Pkg. Backpulver
* 1 kg Äpfel (blättrig geschnitten)

Äpfel schälen und in Scheiben schneiden. Butter schaumig rühren, Eier, Zucker und Vanillezucker gut einrühren. Mehl mit Backpulver mischen und unterrühren. Die Apfelscheiben unter den Teig heben und diese Mischung auf ein gefettetes Backblech streichen und bei 180° Umluft ca. 30 Minuten backen.

**Optional:** Auf dem heißen Kuchen eine Zuckerglasur verteilen (25 dag Staubzucker mit Wasser oder Zitronensaft vermischen).

