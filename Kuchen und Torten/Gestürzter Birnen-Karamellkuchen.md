# Gestürzter Birnen-Karamellkuchen

**Karamellcreme**

* 250 g Zucker
* 200 ml Schlagobers
* 100 g Butter (kalt, gewürfelt)
* 1/2 TL Salz

Zucker in Pfanne erhitzen bis er karamellisiert. Schlagobers erwärmen, zum Karamell gießen und köcheln lassen. Butter und Salz einrühren.

**Teig**

* 120 g Butter (weich)
* 190 g Zucker
* 2 Eier
* 200 g Mehl (glatt)
* 2 TL Backpulver
* 1 TL Zimt
* 1 Prise Salz
* 175 ml Milch

Butter, Zucker, Eier vermengen. Mehl, Backpulver, Zimt und Salz mischen und abwechselnd mit der Milch zum Teig geben.

**Obst**

* 3 Birnen

**Zubereitung**

Ofen auf 200°C Ober- und Unterhitze vorheizen. Springform mit Backpapier auslegen und buttern. Birnen in Spalten schneiden und in Springform legen. Karamell zubereiten, drübergießen, kurz abkühlen lassen. Teig zubereiten und in die Form füllen. Ca. 40 Minuten backen, auskühlen lassen, und stürzen.

> [!TIP] Markus: Nüsse im Teig machen sich gut!

