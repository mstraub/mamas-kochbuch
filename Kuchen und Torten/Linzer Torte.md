# Linzer Torte

## Variante Mama

**Teig**

* 50 dag Mehl
* 25 dag Butter
* 2 EL Backpulver
* 1 KL Nelken- und Zimtpulver
* 2 EL Rum
* Prise Salz
* 12 dag geriebene Haselnüsse
* 6 dag Kochschokolade
* 20 dag Honig
* 2 Eier
* 1 Dotter

**Fülle**

* Ribiselmarmelade
* 1 Eiklar
* 3 EL Mandelblättchen

Für den Teig alle Zutaten gut verkneten. Tortenboden mit Backpapier auslegen und ⅔ der Teigmasse in die Tortenform drücken. Dick mit Marmelade bestreichen. Anschließend aus dem letzten ⅓ Teig ein Gitter legen. Mit Eiklar bestreichen und mit Mandelblättchen bestreuen. Im vorgeheizten Backrohr bei 170° ca. 40 Minuten backen.

## Variante Frau Fenzel

**Mürbteig**

* 10 dag Zucker
* 10 dag Mehl
* 10 dag ger. Nüsse
* 10 dag Biskuitbrösel (z.B. Biskotten)
* 10 dag Butter
* 1 Ei
* 1 Messerspitze Backpulver
* Zimt
* 1 EL Rum
* Zitronenschale
* 2 gest. Nelken

**Fülle**

* Ribiselmarmelade

Rasch zu einem Mürbteig kneten. Die Hälfte in eine Tortenform drücken. Ribiselmarmelade draufstreichen Vom restlichen Teig Gitter darüber. Mit Ei bestreichen und Mandeln oder Nüssen bestreuen. Ca. 30 Minuten bei mittlerer Hitze (160°) backen.
