# Apfelschlangerl

> gedeckter Topfen-Apfelkuchen

* 250 g Mehl
* 200 g Butter
* 250 g Topfen
* Salz
* 900 g Äpfel
* Zitronensaft
* Zimt

1. Mehl mit kalter zerschnittener Butter verkneten
2. Mit Topfen und Salz verkneten (etwas klebrig)
3. Im Kühlschrank 60 Minuten rasten lassen
4. Äpfel schneiden / hacheln / reiben und mit Zitrone und Zimt vermischen
5. Teig halbieren, auf bemehlter Fläche auswalken
6. Teig - Äpfel - Teig aufs Blech
7. Bei 180°C ca 30 Minuten backen

## Variante mit Essig

**Teig**

- 150 g glattes Mehl
- 110 g Butter
- 2 EL Wasser
- 1 EL Essig
- 1 Dotter
- 1 Ei (zum Bestreichen)

**Fülle**

- 500 g Apfelspalten
- 30 g Rosinen
- 50-70 g Zucker
- Zimt
- Vanillezucker

**Zubereitung**

1. Mehl mit Butter, Wasser, Essig, sowie Dotter gut vermengen und zu einem Teig verarbeiten.
2. Teig kühl rasten lassen
3. Teig Rechteckig ausrollen
4. Fülle zubereiten (alles vermengen), in der Mitte auftragen und den Teig zusammenschlagen
5. Mit verquirltem Ei bestreichen und mit Gabel Muster eindrücken
6. Auf mit Butter bestrichenem Blech im vorgeheizten Rohr bei 165°C 35-40 Minuten backen


