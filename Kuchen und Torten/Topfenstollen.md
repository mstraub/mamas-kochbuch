# Topfenstollen

* 500 g Mehl
* 125 g Butter
* 125 g Zucker
* 250 g Topfen
* 150 g Rumrosinen
* 100 g Zitronat
* 125 g Haselnüsse
* 2 Eier
* 1 Pkg. Vanillezucker
* 1 Pkg. Backpulver
* 1 Prise Salz

1. Zitronat grob hacken.
2. Mehl, Backpulver, Eier, Zucker, Vanillezucker, Topfen, Salz und Butter zu glattem Teig kneten. 
3. Rosinen, Zitronat und Haselnüsse untermengen.
4. Teig 40 Minuten ruhen lassen, dann auf bemehlter Arbeitsfläche zu einem Stollen formen.
5. Auf ein mit Backpapier belegtes Blech legen und im vorgeheizten Backrohr bei 200° Umluft ca. 60-70 Minuten backen.

**Optional**

* 125 g Butter
* 200 g Staubzucker

6. Den warmen Stollen mit geschmolzener Butter bestreichen und mit Staubzucker bestreuen.
7. Diesen Vorgang 3x wiederholen.
