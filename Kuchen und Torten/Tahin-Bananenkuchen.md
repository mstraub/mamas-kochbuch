# Tahin-Bananenkuchen

> [!CITE] von Begüm + Thomas, vegan

- 2-3 reife Bananen
- 200 g Tahin
- 80 g Zucker (oder Alternativen wie Dattelmus)
- 1 Pkg Backpulver
- 4 TL (gehäuft) Hafermehl
- optional: Schokoladestücke

1. Mix Tahini, banana, sugar (or replacement) in a food processor
2. add flour and baking powder and mix for 30 seconds more
3. fill the dough into muffin papers or some tray
4. You can add slices of 1/3 banana, or chocolate or more tahin on top. (I generally add banana slices between dough)
5. Baking in a preheated oven at 180° for 20 min. Check with toothpick
6. Afiyet olsun! :)

