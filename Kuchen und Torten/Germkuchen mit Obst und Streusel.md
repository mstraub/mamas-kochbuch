# Germkuchen mit Obst und Streusel

**Germteig**

* 500 g Mehl
* 65 g Butter
* 65 g Zucker
* 1 Ei
* 1/4 L Milch
* 1/2 Würfel Germ (21 g)

**Streusel**

* 150 g Mehl
* 100 g Zucker
* 80 g Butter
* Zimt

[Germteig](../Und%20sonst/Germteig.md) zubereiten (Vorteig oder Dampfl), am Blech verteilen, dicht mit Obst (z.B. Zwetschgen) belegen, dann Streusel darüber.

