# Naan

> [!CITE] von [tasteoftravel.at](https://www.tasteoftravel.at/naan/)  

für ca. 4 Brote (zwei Personen), passt z.B. zu [Dal](../Hauptspeisen/Dal.md)

**Zutaten**

* 160 g Mehl, glatt od. universal, plus Mehl zum Arbeiten
* ½ TL (Weinstein-)Backpulver
* ¼ TL Natron
* ½ TL Zucker (2 g)
* ¾ TL Salz (3,5 g)
* 50 ml Milch
* 3 EL Butter (27 g)
* 65 g Naturjoghurt
* Optional: zerlassene Butter zum Einstreichen, Knoblauchhälften

**Zubereitung**

 1. Mehl, Backpulver, Natron, Zucker und Salz in einer Schüssel mischen.
 2. Butter und Milch gemeinsam in einem Topf erwärmen, bis die Butter vollständig zerlassen ist. Kurz abkühlen lassen.
 3. Joghurt in eine große Rührschüssel geben und die warme Milch-Butter-Mischung langsam einrühren.
 4. Die trockenen Zutaten nach und nach mit einem Löffel in die flüssigen rühren. Wenn sich die Bestandteile verbinden zuerst in der Schüssel, dann auf einer Arbeitsfläche zu einem geschmeidigen Teig kneten. Wenn der Teig noch klebrig ist, noch ein wenig Mehl hinzufügen.
 5. Mindestens ½ Stunde unter der umgedrehten Schüssel oder mit einem Geschirrtuch bedeckt rasten lassen - je länger, desto weicher wird der Teig. Er kann auch über Nacht im Kühlschrank rasten; dann ½ Stunde vor der Verarbeitung temperieren lassen.
 6. Den Teig in 4 Portionen (à knapp 80 g) teilen und rund 2 mm dünn auf einer bemehlter Fläche auf 22 cm Länge und 16 cm Breite oval ausrollen. Teig dabei immer wieder wenden und bemehlen.
 7. Eine beschichtete oder gusseiserne Pfanne auf dem Herd erhitzen – sie soll gut vorgeheizt sein.
 8. Die Teigfladen zwischen den Händen schwenken um überschüssiges Mehl zu entfernen und ohne Öl in der Pfanne bei mittlerer bis hoher Hitze backen. Wenn die Teigoberseite Blasen wirft und die Unterseite Farbe annimmt, wenden. Die zweite Seite ebenso backen.
 9. Mehlrückstände in der Pfanne mit einem Küchenpapier entfernen und die restlichen Naan backen. Die fertigen Naan in ein Geschirrtuch einschlagen – so bleiben sie länger warm und weich.
10. Die Brote noch heiß essen - entweder pur oder mit flüssiger Butter bestrichen (Butter-Naan) – dadurch werden sie noch weicher. Für Knoblauch-Naan die Brote mit Butter und den Schnittflächen der Knoblauchhälften einstreichen.

