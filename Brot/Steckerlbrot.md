# Steckerlbrot

> [!CITE] von [soschmecktnoe.at](https://www.soschmecktnoe.at/rezept-steckerlbrot)

- 500 g glattes Mehl
- 1 Würfel Germ
- 1 TL Salz
- 4 EL Öl
- 1 TL Zucker
- ca. 250 ml lauwarmes Wasser
- Kräuter und Gewürze nach Belieben (Rosmarin, Schwarzkümmel,..)

1. [Germteig](../Und%20sonst/Germteig.md) zubereiten
2. Dünn, überlappend, gleichmäßig um ein Steckerl wickeln
3. Am Lagerfeuer vorsichtig grillen