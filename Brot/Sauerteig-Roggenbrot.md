# Sauerteig-Roggenbrot

> [!CITE] Adaptiert von [backenmitchristina.at](https://www.backenmitchristina.at/rezepte/wuerziges-roggenbrot-mit-sauerteig/), wo es auch eine gute Erklärung auch zum [Sauerteigansatz](https://www.backenmitchristina.at/rezepte/sauerteig/) gibt.

für 2 Brote à ca. 850 g

## Vorteig

- 200 g Roggenmehl
- 200 g Wasser
- Sauerteigansatz aus dem Kühlschrank


Den gesamten Sauerteigansatz aus dem Kühlschrank nehmen und mit 200 g Roggenmehl und 200 g Wasser vermischen.

Anschließend **6-8 Stunden** zugedeckt bei Zimmertemperatur stehen lassen, bis der Sauerteig wieder Bläschen bildet.

400 g vom Sauerteig wegnehmen und den Rest in ein  Schraubglas bis zum nächsten Mal in den Kühlschrank stellen.

> Schraubglas nur bis max. zur Hälfte füllen, der Teig geht im Kühlschrank noch auf!


## Brotteig

- 700 g lauwarmes Wasser
- 1 kg Roggenmehl 960
- 30 g Brotgewürz
- 20 g Salz
- 400 g Sauerteig

Die Zutaten zu einem weichen Teig verkneten.

> [Tipp zum Kneten per Hand](https://www.brotbackbuch.de/forumfrage/roggenmischbrot-warum-mit-hand-kneten/): den Teig in einer großen Schüssel (Weitling) kneten, beim Mischen einfach mit der Faust und drehenden Bewegungen die Zutaten nach unten drücken bis sich alles durchmischt hat, dann einmal die Teigreste von der Hand abkratzen (Teigschaber) und dann das Wichtigste: beim Kneten die Hand immer wieder in Wasser tauchen!

> Anscheinend reicht für reine Roggenteige das bloße Vermischen der Zutaten, langes Kneten ist nicht wichtig. Bei [Teigen mit Weizenmehl](https://www.brotstation.de/wissenswertes/g%C3%A4ren-kneten-und-backen-von-roggenhaltigen-teige/) ist es relevanter.

Den Teig Rasten lassen (*Stockgare*).

Zwei Brotlaibe formen und zugedeckt (oder im Gärkörbchen) gehen lassen (*Stückgare*).

> Die Empfehlungen variieren hier stark und sind von der Sauerteigkultur abhängig. Z.B.:
> - [keine Stockgare, 3 Stunden Stückgare](https://www.backenmitchristina.at/rezepte/wuerziges-roggenbrot-mit-sauerteig/)
> - [2 Stunden Stockgare, 1 Stunde Stückgare](https://www.brote-selber-backen.de/teigruhe/).
> - Auch lange Stückgare bis zu 10 Stunden sind möglich.

> Markus: weniger als 15 Minuten Stückgare ergab ein extrem sitzengebliebenes Brot, 14 Stunden Stückgare bei sommerlicher Raumtemperatur ein etwas zu säuerliches Brot.

Den Backofen 30 Minuten lang auf 230 Grad (Heißluft) vorheizen.

> Andere Quellen heizen auf 250°C oder mehr vor, und reduzieren dann nach wenigen Minuten auf 200°C oder weniger

Die Brote aus den Gärkörbchen auf ein Backblech stürzen, sofort in den heißen Ofen geben und 45 Minuten mit viel Dampf backen.


## Brotgewürz

Für klassisch österreichisches Roggenbrot zu gleichen Teilen: Anis, Fenchel, Koriander, Kümmel.

Stark würzig = je 2EL (nicht gemahlen) pro 500g Mehl

> Markus: Experimentieren ist angesagt! Solange man nicht auf Salz vergisst schmeckt das ungewürzte Schwarzbrot jedenfalls auch sehr gut!


https://www.smarticular.net/brotgewuerz-selbst-herstellen-rezept/
