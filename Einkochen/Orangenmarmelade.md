# Orangenmarmelade

> [!CITE] britische *Orange Marmalade*, grob nach [diesem Rezept](https://useyourfood.de/orangenmarmelade-mit-schale/)

**Zutaten**

- ca. 10 große Bio-Orangen (im Idealfall Saftorangen)
- 1/2 kg Gelierzucker 1:2

**Zubereitung**

1. So viele Orangen auspressen, dass man 1 L Saft (mit Fruchtfleisch) hat
2. Zesten (feine Streifen) abschälen, möglichst wenig vom bitteren Weißen abschälen
3. Wie bei Marmeladen üblich, alles zusammen ca. 10 Minuten kochen

>[!NOTE] Erfahrungen Markus
> - mit *Navel* Orangen: sehr gut, perfekt bittere Note mit 75 g Zesten, Orangen gaben aber wenig Saft und viel Fruchtfleisch blieb in der Schale
> - unbekannte Sorte: 100 g Zesten, 9 Orange ließen sich so auspressen, dass das Fruchtfleisch komplett mitgegangen ist - wunderbar

