# Weintraubengelee

1. Trauben waschen und rebeln
2. Mit einem Dampfentsafter entsaften, wenn der Saft fertig ist mit einem Schöpfer lange im Fruchtmus rühren so dass möglichst viel Saft und Fruchtmus rausgequetscht wird
3. Saft mit Gelierzucker (Menge nach Angabe) kochen - fertig

> [!NOTE] Markus: unsere Uhudler-Trauben haben so dermaßen viel Kerne,
> dass entsaften wohl wirklich eine gute Möglichkeit ist.
> (sonst bin ich kein Fan vom Weglassen des guten Fruchtfleischs)

> [!NOTE] Markus' Erfahrung mit 5 kg Uhudler-Trauben
> - 3,8 L Saft (bzw. Mus)
> - mit Gelierzucker für 4 kg Früchte ergab sich ein eher weiches Gelee
