# Bärlauchkapern

> [!CITE] Klassiker von Mama

- 300 g Bärlauchknospen

**Sud**
- 250 g Wasser
- 250 g Essig
- 100 g Zucker
- 30 g Salz
- Pfefferkörner

Knospen waschen und dicht in Gläser füllen.
Sud aufkochen, die Knospen in den Gläsern übergießen und zuschrauben.

> [!TIP] Hinweis von Mama
> Ich mach meist die doppelte Menge Sud, damit wirklich alles bedeckt ist.

Dann die Gläser auf den Kopf stellen und ca. eine Woche ruhen lassen.
Ab und zu Gläser wenden.


