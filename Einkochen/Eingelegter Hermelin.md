# Eingelegter Hermelin – Nakládaný Hermelín

> [!CITE] tschechischer Jausenklassiker, [Quelle](https://chili-und-ciabatta.de/2019/03/eingelegter-hermelin-nakladany-hermelin/)

## Zutaten

pro 100 g Käse (Hermelín, Camembert, oder anderer Weißschimmelkäse)

**Paste**
- 1 Zehe Knoblauch
- 1/3 rote Chilischote gehackt
- 1 TL Paprikapulver
- 1 Prise Salz

**Weitere Gewürze**
- 1/3 rote Zwiebel (in Ringe geschnitten)
- 6 Pfefferkörner
- 6 Wacholderbeeren
- 6 Pimentkörner
- 1 Lorbeerblatt

> [!NOTE] Markus: im Originalrezept stand *einige* als Mengenangabe bei den Körnern, ich habs mit oben genannter Menge ausprobiert, schmeckt so gut würzig

## Zubereitung

Im Idealfall kleine Runde Käselaibe (100 g) verwenden und ein Einmachglas mit ähnlichem Durchmesser bereitstellen. Das macht visuell am meisten her. Man kann aber auch größere Käse zerschneiden und evtl. mit Zahnstochern fixieren.

1. Paste aus gepresstem Knoblauch, Paprika, Chili und Salz zubereiten, in etwa so viel Paprika dazugeben wie der Knoblauch aufnehmen kann. Die Menge Chili kann durchaus großzügig sein, ist am Ende nicht so scharf durchs Durchziehen
2. Käse horizontal aufschneiden (wie einen Burger), Paste draufschmieren, einige Zwiebelringe drauflegen, und wieder zuklappen
3. Hälfte vom restlichen Zwiebel und der Gewürze ins Glas legen, Käse drauflegen, Rest oben drauf. Nachstopfen, damit möglichst wenig Luft drin ist
4. Mit Öl auffüllen, nachstopfen, evtl. klopfen und versuchen die Luftblasen rauszubekommen - wiederholen bis der Käse komplett mit Öl bedeckt ist
5. Mehrere Tage bei Zimmertemperatur ziehen lassen

> [!CITE] Viele Varianten laut [Quelle](https://chili-und-ciabatta.de/2019/03/eingelegter-hermelin-nakladany-hermelin/)..
> Der eingelegte Hermelin ist ein typisch tschechisches Beislessen. Für die Herstellung gibt es unzählige Varianten mit Kräutern, Walnüssen, Frühlingszwiebeln, eingelegtem Gemüse, auch Pesto als Füllung habe ich gesehen. Man kann auch die weiche Käsemasse aus den Hälften herausschaben, mit den jeweiligen Würzzutaten vermischen und dann wieder in die Käsehüllen streichen. Gerne wird Hermelin auch gebraten, gegrillt oder paniert serviert.