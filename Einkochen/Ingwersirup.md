# Ingwersirup

Sirup zum Aufgießen mit heißem Wasser -> Ingwertee

- 500 g frischer, junger Ingwer, geschält
- 3 Tassen Zucker
- 1 Zitrone, entkernt und in Scheiben geschnitten
- 1 Tasse Ahornsirup

**Erster Tag**

Ingwer in dünne Scheiben (wenn Ingwer im Sirup bleiben soll) oder Würfel
(für kandierten Ingwer) schneiden, in Kochtopf geben, Wasser dazugießen
bis der Ingwer gedeckt ist. Langsam aufkochen lassen, Hitze reduzieren,
Deckel drauf, und ca. 20 Minuten köcheln lassen, bis der Ingwer gar ist
(Messerprobe). Bei älterem oder verholztem Ingwer kann es auch länger
dauern. 1 Tasse Zucker dazugeben, rühren und aufkochen. Vom Feuer
wegnehmen.

Zudecken und bei Zimmertemperatur über Nacht stehen lassen.

**Zweiter Tag**

Deckel abnehmen, Ingwer langsam aufkochen, 15 Minuten köcheln lassen.
Zitrone und 1 Tasse Ahornsirup beifügen. Unter gelegentlichem Rühren
weitere 15 Minuten köcheln lassen. Vom Feuer nehmen.

Wieder zudecken und bei Zimmertemperatur über Nacht stehen lassen.

**Dritter Tag**

Deckel abnehmen, Ingwer aufkochen, häufiger rühren. 1 Tasse Zucker
beigeben, 30 Minuten unter häufigem Rühren köcheln lassen. 1 weitere
Tasse Zucker beigeben, aufkochen. Vom Feuer nehmen. Deckel drauf und bei
Zimmertemperatur über Nacht stehen lassen.

**Vierter Tag**

Ohne Deckel langsam aufkochen, Hitze reduzieren. Vorsichtig und langsam
köcheln lassen, bis Ingwer durchscheinend wird und der Sirup dick
eingekocht ist (ca. 45 Minuten). Sirup in Glas abgießen. Zitronen und
Ingwerstücke können entfernt oder im Sirup gelassen werden. Wenn man sie
entfernt: auf Backreinpapier legen, nicht zugedeckt über Nacht trocknen
lassen, in grobem Kristallzucker wälzen. Lagern in gut schließendem
Glasgefäß. Einen Teelöffel Sirup pro Teehäferl mit heißem Wasser
aufgießen für Ingwertee.

