# Zucchinimarmelade

- 1 kg Zucchini (entkernt)
- 1 kg Gelierzucker (1:1)
- Saft einer halben Zitrone
- kleines Stück Ingwer (1 - 2 cm)
- Zimt

Zucchini waschen und entkernen, es sollte 1 kg Fruchtfleisch übrig
bleiben. Fein reiben, mit allen Zutaten vermischen und aufkochen. Nach
Gelierprobe (eher flüssig) in Marmeladegläser füllen.

Varianten aus den Weiten des Internets geben noch Vanille, Nelken,
Koriander, Chili dazu:

## Variante 1
- 1 kg Zucchini
- 1 Vanilleschote
- 3 Stangen Zimt
- 1 EL Nelken
- 1 Stück Ingwer (etwa 3 cm)
- 500 g Gelierzucker 2:1
- 1 Zitrone

## Variante 2
- 1 kg Zucchini
- Gelierzucker 3 zu 1
- Saft von 3 Zitronen (etwa 100 ml)
- 1 daumengroßes Stück Ingwer, kleingehackt; ersatzweise 1
    gestrichener EL gemahlenen
- 1/2 TL gemahlener Koriander
- 1/2 TL Zimt
- 1/2 TL gemahlene Nelken
- eine Prise Chilipulver

