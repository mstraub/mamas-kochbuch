# Zwetschgenmus

- 3 kg Zwetschgen (möglichst reif)
- 25 dag Zucker
- 25 dag braunen Zucker
- 1 TL Zimt
- 5 geriebene Nelken

Die Zwetschgen putzen, halbieren und entsteinen. Mit dem Zucker und den
Gewürzen mischen und anschließend im Topf kurz aufkochen. Die Masse
anschließend in einer Pfanne im Backofen bei 180° ca. 2 Stunden dick
einkochen lassen. In der ersten halben bis dreiviertel Stunde einen
Kochlöffel oder ähnliches in die Backofentür einklemmen, damit
Feuchtigkeit entweichen kann. Die Masse anschließend in Gläser
einfüllen. Wenn man mag, kann die Masse zuvor auch püriert werden.

Zwetschgenmus kann mit einer Scheibe Brot, einem Germknödel oder anderen
Dingen genossen werden.

Kann auch nur mit weißem Zucker gekocht werden, der braune Zucker
verleiht dem ganzen aber ein gutes Aroma!
