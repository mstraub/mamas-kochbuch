# Dirndl-Obst

- 2 kg Dirndln
- 1 kg Gelierzucker (1:1)
- 400 g Zucker
- 3 Äpfel (Friedrich von Preußen)
- 2 Nektarinen
- Zimt

1. Dirndln waschen und mit 400 g Zucker (200 g / kg) entsaften (ca. 1,3 L Saft)
2. Den Rest nach Abkühlung entsteinen
3. Dirndlfruchtfleisch mit anderem Obst, Zimt und Gelierzucker aufkochen und nach Gelierprobe in Marmeladegläser füllen
