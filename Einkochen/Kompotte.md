# Kompotte

> [!CITE] Das große Sacher Kochbuch Seite 576ff

Grobe Anleitung für alle Kompotte:

1. Zuckerlösung (Läuterzucker) vorbereiten (kochen)
2. Früchte dazugeben, kurz aufkochen, danach ziehen / wallen lassen bis die Früchte weich sind
3. Heiß in Gläser abfüllen, verschrauben, auf den Deckel stellen

**Mengen pro 1 kg Frucht:**

| Frucht     | Wasser | Zucker | Kochdauer |
| ---------- | ------ | ------ | --------- |
| Marille    | 1/4 L  | 250 g  |           |
| Kirsche    | 1/4 L  | 125 g  | 10 min    |
| Rhabarber  | 1/4 L  | 500 g  | 1 min     |
| Weichsel   | 1/4 L  | 250 g  | 10 min    |
| Weintraube | 1/4 L  | 400 g  |           |
| Zwetschke  | 1/8 L  | 125 g  | 10-15 min |

> [!WARNING] Kochzeiten sind zusätzlich recherchiert
> .. und nur ein Richtwert, unbedingt während dem Kochen prüfen damit das Obst nicht zerfällt

> [!NOTE] Die Zuckermenge kann bei sehr süßen reifen Früchten (Marille!) sicher noch reduziert werden

## Markus' Versuche

### Uhudler 2022 (Charge α: wenig Zucker)

- 2 kg Uhudlertrauben
- 0,9 L Wasser (= Trauben im Topf fast ganz bedeckt)
- 150 g Zucker

1. Trauben mit Wasser und Zucker aufkochen (mit Deckel)
2. Gläser heiß waschen (frisches Handtuch, frischer Fetzen)
3. Nach dem Aufkochen in die Gläser füllen, Gläser auf den Deckel stellen
4. Mit Geschirrtuch abdecken und auskühlen lassen

-> ergibt 4,5 1 kg-Honiggläser und gefühlt zu viel Flüssigkeit, zerkochte Trauben

### Uhudler 2022 (Charge β: pur)

- 2,4 kg Uhudlertrauben
- 1/4 L Wasser (damit der Boden bedeckt ist)

Zubereitung wie α, mit dem Unterschied, dass die Hitze langsam erhöht wurde damit die Trauben Saft lassen können und nicht die unteren Trauben zerkocht sind bevor die oberen heiß werden. 

-> ergibt 3,5 1 kg-Honiggläser, noch immer sehr flüssig

### Kirsche 2022

nach Hess, Wiener Küche, also mit kompliziertem (und viel) Läuterzucker.
Sehr aufwändig (und der Zucker ist viel zu hart geworden), aber das Ergebnis war unglaublich aromatisch (keine Ahnung mehr woher die Kirschen waren - sprich ob es an ihnen selbst lag)