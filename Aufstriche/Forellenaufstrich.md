# Forellenaufstrich

> [!CITE] Gregor "Gregsi" Strahammers Spieleabendbeitrag

- 100 g geräucherte Forelle
- 200 g Frischkäse
- 1 kleine Zwiebel
- Pfeffer, Salz,..

1. Fisch mit Gabel zerdrücken und mit Käse vermischen
2. Zwiebel sehr fein schneiden und untermischen
3. Nach Belieben mit Pfeffer und Salz würzen
4. Mit Grissini, Sesamsticks,.. oder auf Brot genießen