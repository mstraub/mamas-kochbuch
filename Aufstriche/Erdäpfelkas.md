# Erdäpfelkas

- 600 g mehlige Erdäpfel
- 250 g Rahm
- 1 kleine Zwiebel
- Knoblauch
- Salz, Pfeffer, Kümmel, Muskat
- Schnittlauch (zum Garnieren)

1. Erdäpfel kochen und schälen
2. Erdäpfel mit Gabel zerdrücken so dass noch Stückchen drin sind (alternativ: Erdäpfelpresse)
3. Zwiebel und Knoblauch fein hacken
4. Erdäpfel noch warm mit Zwiebel, Rahm, und den Gewürzen vermischen
    - Siggi meint: nur grob, nicht allzu homogen
    - Mama meint: die Konsistenz sollte cremig sein (mit Rahmmenge anpassen)
6. Einige Stunden kühl rasten / ziehen lassen
7. Mit Schnittlauch garnieren

> [!TIP] Alternativen zum Rahm
> Siggis Mama verwendet Milch und Schlagobers und nur wenig Rahm.
> Im Netz gab es eine Rezeptvariante mit zusätzlich Butter.