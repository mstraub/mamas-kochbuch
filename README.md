# Mamas Kochbuch

erweitert um die mittlerweile jahrelange Kocherfahrung ihrer Sprösslinge

## Überblick

- [Aufstriche](./Aufstriche)
- [Brot](./Brot)
- [Einkochen (Eingelegtes, Marmeladen,..)](./Einkochen)
- [Eis](./Eis)
- [Hauptspeisen](./Hauptspeisen)
- [Kekse](./Kekse)
- [Kuchen und Torten](./Kuchen%20und%20Torten)
- [Nachspeisen](./Nachspeisen)
- [Suppen](./Suppen)
- [Und sonst..](./Und%20sonst)

## Technik

Die Rezepte sind in möglichst "purem" Markdown geschrieben,
sollten also mit vielen Editoren kompatibel sein.

Ich verwende [Obsidian](https://www.obsidian.md) (Desktop Editor, siehe [unterstützte Markdown-Features](https://help.obsidian.md/How+to/Format+your+notes) - super: image resizing, callouts!)

Damit Obsidian möglichst bei Standard-Markdown bleibt:

- `New link format` -> `relative path to file`
- `Use Wiklinks` deaktivieren
- `Show inline title` deaktivieren