# Erdnusssuppe (Westafrika)

> [!cite] Adaptiert von [West African Peanut Soup / Granat Soup](https://feelgoodfoodie.net/recipe/peanut-soup/)

**Zutaten**
- 1 TL Rapsöl
- 2 große Zwiebeln (fein gehackt)
- 1 TL Ingwer (gerieben / fein gehackt - optional)
- 85 g Tomatenmark
- 1 Süßkartoffel (gewürfelt)
- 1,5 L Gemüse- oder Hühnersuppe (mit nur 1 L wirds ein Eintopf)
- 1 TL Salz
- Schwarzer Pfeffer
- 1 TL Cayennepfeffer (optional)
- 250 g Erdnussmus (im Idealfall 100% Erdnuss, kein Zucker)
- Handvoll Spinatblätter (optional)
- 225 g Hühnerfleisch (optional)

**Zubereitung**
1. Zwiebel und Ingwer mit Öl dünsten (glasig)
2. Tomatenmark, Cayennepfeffer, Salz dazu und gut vermischen
3. Süßkartoffel und Huhn dazu
4. Suppe dazu, dann 20 Minuten sanft köcheln lassen (bis Süßkartoffeln durch aber noch nicht zerfallen sind)
5. Erdnussbutter einrühren, Spinat dazu, kurz köcheln bis der Spinat durch ist

Entweder als Suppe essen oder auf weißem Reis servieren und nach Belieben mit Erdnüssen bestreuen.
