# Nussecken

**Mürbteig**
- 20 dag Mehl
- 2 KL Backpulver
- 10 dag Zucker
- 1 Pkg Vanillezucker
- 1 Ei
- 1 Dotter
- 10 dag Butter

**Belag**
- 15 dag Butter
- 15 dag Zucker
- 1 Pkg Vanillezucker
- 3 EL Wasser, Rum
- 30 dag geriebene Nüsse

- Marmelade (zwischen Teig und Belag)
- Schokoladenglasur (oben drauf)

Mürbteig kneten und kalt stellen. Für Belag Butter mit Zucker,
Vanillezucker und Wasser aufkochen, dann Nüsse und Rum dazu, dann kalt
stellen

Auf Blech Teig - Marmelade - Belag aufbringen, bei 150°C ~25min backen.
Warm in Vierecke schneiden, Ecken in Tortenglasur tunken.

