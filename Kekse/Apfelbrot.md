# Apfelbrot

- 50 dag Feigen
- 40 dag Rosinen
- 40 dag Nüsse
- 1,5 kg Äpfel
- 2 EL Kakao
- 2 EL Zimt
- 1 EL Neugewürz (Piment)
- ½ kg Zucker
- etwas Salz
- ⅛ L Rum
- 1 kg Mehl
- 4 TL Backpulver

Die Feigen stifteln, Äpfel grob raspeln und die Nüsse halbieren oder
vierteln. Alle Zutaten gut mischen und einige Stunden ziehen lassen.
Dann eine Form streichen und bei 180° ca. 1 ½ Stunden backen.

