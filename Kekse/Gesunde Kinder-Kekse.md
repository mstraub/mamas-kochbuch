# Gesunde Kinder-Kekse

* 1,5 große Äpfel (geschält und geraspelt)
* 1 EL Zitronensaft
* 120 g Aprikosen (getrocknet)
* 90 ml Hafermilch (Mandel- oder Reismilch)
* 90 ml Kokosöl, Rapsöl oder gemischt
* 270 g Weizenvollkornmehl oder Dinkelvollkornmehl
* 180 g 5-Korn-Mischung oder nur Haferflocken
* 1 Prise Salz

1. Geraspelte Äpfel mit Zitronensaft beträufeln. Die Aprikosen sehr fein hacken und dazugeben.
2. Ofen auf 180° Ober- Unterhitze vorheizen
3. Milch und Öl vermischen. Mehl, Flocken, Salz und evtl. Gewürze in eine Schüssel geben. Die Früchte sowie die Milch-Öl-Mischung zügig unterrühren (geht auch mit einem einfachen Teigschaber). Der Teig soll eher feucht sein. Sollte er extrem nass sein, kann man noch Mehl zugeben.
4. Mit den Händen kleine Kugeln formen und mit Abstand auf ein mit Backpapier belegtes Blech geben. Die Kugeln zu Keksen plattdrücken (je dünner, desto knuspriger). 
5. Kekse ca. 20 Minuten backen. Sie bleiben eher weich.