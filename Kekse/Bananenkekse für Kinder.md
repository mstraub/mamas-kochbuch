# Bananenkekse für Kinder

* 120 g Banane (sehr reif)
* 250 g Dinkelmehl (Type 630)
* 100 g weiche Butter

1. Banane mit Gabel zerdrücken und mit Dinkelmehl und Butter in eine Schüssel geben.
2. Mit dem Knethaken des Rührgeräts zu einem gleichmäßigen Teich kneten (alternativ händisch kneten)
3. Teig in Frischhaltefolie gewickelt ca. 30 Minuten kühl stellen.
4. Ofen auf 180° (Umluft 160°) vorheizen und Backblech mit Backpapier auslegen.
5. Teig auf leicht bemehlter Fläche ausrollen, Kekse ausstechen und auf Backblech verteilen.
6. Im Ofen ca. 12 Minuten leicht goldbraun backen.