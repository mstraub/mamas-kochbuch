# Weihnachtszwieback

- 14 dag Zucker
- 17 dag Mehl
- 8 dag Butter
- 4 dag geriebene Nüsse
- 5 Eiklar

Schnee mit 1 EL Wasser steif schlagen, zerlassene Butter, Zucker, Mehl,
Nüsse vorsichtig mit dem Schneebesen unterheben.

In einer befetteten rechteckigen Auflaufform (max 36x26cm!!) backen
(Heißluft 170°). Nach dem Auskühlen in ca 1 cm dicke Stücke schneiden,
im Rohr auf einem Blech bähen (bei ca 100° trocknen) und noch warm in
Staub-Vanillezuckergemisch drehen.

