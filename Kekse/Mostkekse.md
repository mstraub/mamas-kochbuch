# Mostkekse

laut Siggi: 1:1 Butter und Mehl, Weißwein zugeben bis die Konsistenz
passt, z.B.:

- 25 dag Butter
- 25 dag Mehl
- 3 EL Weißwein oder 4 cl eingedickter Most (aus ¼ L Birnenmost) oder Essig

Varianten im Internet: Teig mit Ei und Zucker

Zu Mürbteig verkneten, eine halbe Stunde im Kühlschrank (eingewickelt in
Frischhaltefolie) rasten lassen, dann auswalken, runde Formen
ausstechen, Klecks säuerlicher Marmelade (z.B. Ribisel) in die Mitte,
zuklappen, Enden zudrücken, backen, mit Staubzucker-Vanille-Gemisch
bestauben

