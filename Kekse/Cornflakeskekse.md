# Cornflakeskekse

- 30 dag Schokolade
- 10 dag Ceres (Kokosfett)
- 28 dag Cornflakes
- 1 ½ EL geriebene Nüsse
- 7,5 dag Staubzucker
- 2 EL Rum (80 %) für leichten Rumgeschmack

Das Kokosfett in einem Topf zergehen lassen und anschließend die
Schokolade hinzugeben. Darauf achten, dass die Masse nicht zu heiß wird!
Zum Schluss Staubzucker, Haselnüsse und, wenn die Masse etwas abgekühlt
ist, den Rum beigeben. Anschließend die Cornflakes unterrühren und etwas
zerstampfen.

Die Masse anschließend in Pralinenförmchen einfüllen und erstarren
lassen (ca. 1 ½ Kuchenblech voll).

