# Busserl

## Mit Mehl (z.B. Kokosbusserl)

- 15 dag Kokosraspeln (Kokosett)
- 15 dag Zucker
- 3 dag Mehl
- 4 Eiklar

Aus den Eiklar Schnee schlagen und dann langsam den Zucker unterrühren.
Danach mit einem Schneebesen das Mehl und das Kokosraspeln unterheben.
Auf einem Backblech Kekse formen, optional auf Oblaten. Im Backrohr bei
160° backen. Eventuell Temperatur zurückdrehen, muss langsam gebacken
werden, ansonsten werden die Kekse "patzig".

**Tipp von Mama:** zum Eiklar 1 EL kaltes Wasser hinzufügen, dann wird
der Schnee fester.

**[Variante](https://www.sonnentor.com/de-at/rezepte-tipps/rezepte/kurkuma-kokosbusserl):**
mit ein paar TL Kurkuma würzen

## Ohne Mehl (z.B. Nussbusserl)

- 3 Eiklar
- 20 dag Zucker
- 20 dag geriebene Nüsse

Aus den Eiklar Schnee schlagen und dann langsam den Zucker einrühren
(noch mit Mixer). Danach mit einem Schneebesen Nüsse unterheben. Auf
Backpapier Busserl formen (optional auf Oblaten).

Im Backrohr bei 160° ca 5 Minuten backen, dann Temperatur auf ~100°
reduzieren und weitere 15 Minuten trocknen. Wenn sich die Busserl vom
Backpapier lösen: fertig.

