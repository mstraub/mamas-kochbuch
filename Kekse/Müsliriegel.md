# Müsliriegel

## Michi

- 300g Trockenobst (Backpflaumen, Feigen, Datteln,...)
- 300g Nüsse/Kerne (Kürbiskerne, Sonnenblumenkerne, Haselnüsse,
    Walnüsse, Mandeln,...)
- 200g Rosinen
- 4 Äpfel
- 300g Haferflocken
- 300g Vollkornmehl (Weizen oder Dinkel)
- ½ L Wasser
- 9 EL Sonnenblumenöl
- 5 EL Honig
- 4 TL Zimt
- 1 TL Salz

Alle Zutaten nach belieben klein hacken/schneiden und miteinander
vermengen. Auf mit Backpapier ausgelegtes Backblech gleichmäßig
verteilen (ruhig 1cm dick, dann ist der Müsliriegel saftiger) und
anschließend im vorgeheizten Backofen bei Ober-/Unterhitze 200° ca. 40
Minuten backen.

Optional: Oblatten auslegen, mit Wasser anfeuchten und anschließend den
Teig darauf verteilen. Oben mit **beidseitig** angefeuchteten Oblatten
zudecken und dann backen.

Noch warm in Stücke schneiden, auskühlen lassen und genießen.

Kann auch portionsweise eingefroren und z.B. auf dem Toaster aufgetaut
werden.

## Strasser-Riegel

- 1/2 kg Polenta in 1 L Wasser mit etwas Salz kochen und abkühlen lassen
- 1/4 kg Haferflocken
- 1/4 kg andere Flocken (Dinkel, Roggen, Weizen) oder nochmals Haferflocken
- 1/2 kg Trockenfrüchte fein geschnitten (Rosinen, Pflaumen, Preiselbeeren,..)
- 2 Äpfel raspeln und kurz in einer Pfanne rösten
- Saft von 1 Orange und 1 Zitrone, etwas Olivenöl
- 4 EL Honig (damit es klebrig wird)
- nach Belieben: Kokosraspeln, Sesam, Sonnenblumenkerne, Walnüsse,..

Das Ganze in kleine Kugeln oder Streifen formen und auf ein Blech mit Backpapier legen.
Damit die Kugeln nicht mehr so klebring sind, bei 100°C für 20 Minuten ins Backrohr.
Danach die getrockneten Kugeln entnehmen und ab aufs Rad!

