# Zimtsterne

nomnom, ohne Mehl, siehe [Zimtsterne nach klassischem Rezept ohne
Mehl](https://www.chefkoch.de/magazin/artikel/2240,0/Chefkoch/Zimtsterne-wuerzige-Weihnachtsplaetzchen-ohne-Mehl.html)

**Teig**
- 500 g gemahlene Mandeln
- 300 g Puderzucker
- 2 TL Zimt
- 2 Eiweiß
- 2 EL Likör (Mandellikör) - optional

**Glasur**
- 1 Eiweiß
- 125 g Puderzucker

1. Mandeln, Puderzucker und Zimt mischen
2. 2 Eiweiß und Mandellikör dazugeben und gut zu einem glatten Teig mischen / kneten
3. Ca 0,5cm dick auswalken
4. 1 Eiweiß sehr steif schlagen. Puderzucker nach und nach zugeben, dabei weiterschlagen. Sterne damit bepinseln.
5. Im vorgeheizten Backofen auf der untersten Schiene bei 150 Grad 10 - 15 Minuten backen.
