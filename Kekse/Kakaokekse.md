# Kakaokekse

- 35 dag Mehl
- 1 Backpulver
- 1 Messerspitze Zimt
- 1 EL Kakao
- 10 dag Zucker
- 1 Pkg Vanillezucker
- Zitronenschale
- 5 dag geriebene Nüsse
- 20 dag Honig
- 2 EL Marmelade
- 5 EL Öl

Honig, Marmelade, Öl wärmen, rühren und mit übrigen Zutaten zu Teig
verkneten, rasten lassen (1 Stunde genügt bei diesem Teig) ca ½ cm dick
ausrollen, ausstechen, backen

Tipp: mit Eidotter bestreichen für hübscheres Aussehen

