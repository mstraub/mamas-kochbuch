# Vanillekipferl

> [!CITE] von Mama, werden fein mürb

- 12 (14) dag Mehl
- 9 (10) dag Butter
- 5 dag geriebene Mandeln
- 3 dag Staubzucker
- 1 (1/2) Pkg Vanillezucker
- etwas Staub- und Vanillezucker zum Wälzen

Mehl und Butter abbröseln, mit Zucker und Mandeln zu Teig kneten,
Kipferl formen, auf Blech bei 160°C goldgelb backen. Noch warm in
Zuckergemisch wälzen.

**Tipp:** eher weniger Temperatur, beim zweiten Blech möglicherweise
zurückschalten

