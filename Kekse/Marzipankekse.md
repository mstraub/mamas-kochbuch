# Marzipankekse

- 15 dag Butter/Margarine
- 8 dag Staubzucker
- 10 dag Marzipan
- 25 dag Mehl
- 2 EL Vanillezucker
- 1 Dotter
- 1 Pkg Zitronenschale
- Hagelzucker

Auf einem mit Mehl bestaubtem Brett Butter (klein schneiden, sollte
schon weich sein), Staubzucker (sieben!) und Marzipanmasse gut
verarbeiten. Zitronenschale und Vanillezucker und zuletzt das gesiebte
Mehl darunterkneten. Aus dem Teig Stangen mit ca. 3 cm Durchmesser
formen, in Alufolie rollen und im Kühlschrank erstarren lassen. Danach
mit Eigelb bestreichen und im Hagelzucker rollen. Zum Schluss die
Stangen in ca. ½ cm dicke Scheiben schneiden und auf einem Backblech
(Abstand!) bei 180° hellbraun backen.

