# Lebkuchen

> [!TIP] gesammelte Tipps
> - Honig / Zucker vorher evtl. sanft erwärmen um Kristalle aufzulösen (als max. Temperatur findet man alles zwischen 40°C und 80°C)
> - ca. 4-5mm dick ausrollen
> - ca. 7 Minuten bei 180°C backen
>   - Drucktest: wenn sich der Teig nach dem sanften Eindrucken mit einem Finger wieder hebt ist er fertig gebacken

## Lebkuchenhaus
> [!CITE] von Mama

**Lebkuchen**
- 56 dag Roggenmehl
- 56 dag Staubzucker
- 42 dag Nüsse gerieben
- 10 dag Honig
- 5 Eier
- 1 Pkg Lebkuchengewürz
- 1 ½ dag Pottasche (Kaliumcarbonat)

1. Alle Zutaten verkneten und Teig mindestens über Nacht ruhen lassen.
2. Teile für das Haus, Boden,.. vor dem Backen ausschneiden bzw. formen (damit sie einen schönen Rand haben)
3. Lebkuchenteile für das Haus nach dem Backen beschweren bis sie kalt sind,
damit sie gerade bleiben.

**Schnee/Kleber**:
- 3 Eiklar
- 60 dag Staubzucker

Schnee schlagen, dann Zucker nach und nach dazugeben.
Der Kleber sollte etwa die Konsistenz von angerührtem Gips haben.

Beim Kleben hilft:
- Zusammenstecken der Teile mit (halben) Zahnstochern
- Zusammenbinden und kreatives Abstützen mit Büchern, Töpfen,.. 

Zum Verzieren Butterpapier fest zusammenrollen (mit einer Spitze),
mit Tixo verkleben, und dann aus der Spitze rausdrucken.
Evtl. die Spitze vergrößern durch Abschneiden mit der Schere.

## Lebkuchen von Maria

- 1 kg Roggenmehl
- ½ kg Rohzucker (Gelbzucker)
- ½ kg Honig
- 5-6 Eier
- ⅛ L Kaffee
- 1 Pkg Lebkuchengewürz
- 5 Kaffeelöffel Natron
- Saft einer Zitrone

Eidotter mit etwas Milch strecken, Kekse bestreichen und mit Nüssen
verzieren.

> [!WARNING] Markus: wird ziemlich patzig wenn man es genau nach Rezept macht.
> Vor allem im Vergleich zum Aranzini-Lebkuchen, der ja sehr ähnlich ist.
> Ich habe dann einfach noch mehr Mehl dazugegeben bis er nicht mehr geklebt hat.

## Aranzini-Lebkuchen
> [!CITE] von Mama

- 50 dag Roggenmehl
- 1,5 TL Natron
- 25 dag Gelbzucker
- 6 dag gehackte Nüsse
- 6 dag Zitronat
- 6 dag Aranzini
- 8 dag Honig
- 8 dag Butter
- 2 Eier
- eventuell etwas Milch

Mehl, Natron, Zucker, Nüsse, Zitronat, Aranzini vermischen

Butter + Honig zerlassen (leicht erhitzen bis geschmolzen) und mit Eiern
verrühren, zu Teig geben und gut verknete.

24 Stunden rasten lassen, dann auswalken, ausstechen, mit ca ½ cm
Abstand aufs Blech legen, optional mit Ei bestreichen und backen.

> [!TIP] Markus: schmeckt auch gut mit Lebkuchengewürz!

## Lebkuchenhausexperiment mit Lagerlebkuchen (2023)

Recherche Rezepte für Lagerlebkuchen:
- https://schokoladenbutterbrot.de/lebkuchen-mit-lagerteig-fuer-weihnachten-backen/
- https://www.homebaking.at/der-lebkuchenteig-honigteig/
- https://brotschafterin.com/lebkuchenteig/

**Über Teigruhe** (Quelle Wikipedia)

> Es ist üblich, den Teig für braunen Lebkuchen als sogenannten Lagerteig zu führen. Dabei wird der Teig ohne die Triebmittel, Gewürze und Eier – im Wesentlichen also das Gemisch aus Mehl und Honig oder einem anderen Süßungsmittel – über mehrere Tage, Wochen oder sogar Monate kühl und in geschlossenen Behältern gelagert. Während dieser Zeit bauen die natürlicherweise im Teig vorhandenen Milchsäurebakterien in geringem Maße den üppig vorhandenen Zucker ab und wandeln ihn in verschiedene Säuren um (unter anderem Milchsäure). Dies verbessert zum einen das Aroma, erlaubt zum anderen die Verwendung von Pottasche und Natron als Lockerungsmittel ohne weitere Säurezugabe (sie funktionieren nämlich nur in saurer Umgebung).

Es kristallisiert sich heraus:
- Vorteig aus Honig + Mehl im Verhältnis 1:1
- keine klare Linie wie viel Roggenmehl vs Weizenmehl
- Hirschhornsalz und Pottasche sollen den Geschmack beeinflussen

**Vorteig** laut [homebaking.org](https://www.homebaking.at/der-lebkuchenteig-honigteig/):
- 1 kg Honig
- 500 g Weizenmehl (T 700)
- 500 g Roggenmehl (T 969)

Honig auflösen, etwas erkalten lassen und mit dem Mehl zu einem Teig mischen. In einem verschließbaren Kübel 3-4 oder mehrere Monate im Keller aufbewahren.

**Hauptteig** laut [homebaking.org](https://www.homebaking.at/der-lebkuchenteig-honigteig/):

- 1000 g gelagerter Vorteig
- 50 g Vollei (ein mittleres Ei)
- 20 g Zucker
- 40 g Weizenmehl oder Roggenmehl
- 30 g Wasser
- 7 g Ammoniumcarbonat (getrennt auflösen!)
- 3 g Kaliumcarbonat (getrennt auflösen!)
- 12 g Lebkuchengewürz (ca 2 EL)

1. Eier und Zucker schaumig rühren
2. Mehl, Gewürze und Triebmittel daruntermischen. Dieser Gewürzteig sollte je nach Konsistenz des Vorteiges fester oder weicher gehalten werden
3. Vor- und Gewürzteig zusammenarbeiten und schön glatt kneten.
4. Vor dem Aufarbeiten immer erst Probebacken, und den Teig dabei nicht zu dick ausrollen (4-5mm).

> [!HINT] Erfahrungen Markus
> - Lagerteig nur 1,5 Wochen (statt mehrere Monate gelagert) -> es war 100g zusätzliches Mehl und *kein* Wasser nötig um den Hauptteig zu einer ausrollbaren Konsistenz zu kneten
> - Geschmack und Konsistenz war gut (direkter Vergleich Hirschhornsalz vs Backpulver steht aber noch aus)


